/*!
 * BookingCalendar v1.0.0
 * Docs & License: 'bitlasoft.com'
 * (c) 2013 Anil Kapala
 */

/*
 * Use booking-calendar.css for basic styling.
 * 3rd Party Plugins: Drag-n-drop(jquery.event.drop.js), 
 */

// Locals
var suggestions = new Array();
var sugDetails = new Array();

(function($, undefined) {

;;
//defaults
var defaults = {

	// display
	defaultView: 'month',
	aspectRatio: 1.35,
	header: {
		left: 'prev,next today gotoDate chartSelect gotoRoom',
		center: 'title',
		right: 'month,week,year,day,gday'
	},
	gotoRoomFlag: false,
	gotoRoomDetails: {
		roomName: null,
		roomTypeId: null,
		roomNoId: null
	},

	// Enable Drag and Drop & Click events 
	dragAndDrop: false,

	isChannelManager: false,

	// is Dormitory
	isDormitory: false,

	// time formats
	titleFormat: {
		month: 'MMMM yyyy',
		week: "MMM d[ yyyy]{ '&#8212;'[ MMM] d yyyy}",
		day: 'dddd, MMM d, yyyy'
	},
	columnFormat: {
		month: 'ddd',
		week: 'ddd M/d',
		day: 'dddd M/d'
	},

	// Right Click Menu Position
	rightClick: {
		offsetX: 0,
		offsetY: 0,
		direction:'down',
		constrainToScreen:true,
		
		className:'context-menu context-menu-theme-vista',
    	itemClassName:'context-menu-item',
    	itemHoverClassName:'context-menu-item-hover',
    	disabledItemClassName:'context-menu-item-disabled',
    	disabledItemHoverClassName:'context-menu-item-disabled-hover',
    	separatorClassName:'context-menu-separator',
    	innerDivClassName:'context-menu-item-inner',
    	themePrefix:'context-menu-theme-',
    	theme:'default'
	},

	// CallBack Functions
	getInfo: function(){},
	getDormitoryInfo: function(){},// When Dormitory draging this will call

	// Highlight Date
	gotoDateHighlight: false,

	// locale
	firstDay: 0,
	monthNames: ['January','February','March','April','May','June','July','August','September','October','November','December'],
	monthNamesShort: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
	dayNames: ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],
	dayNamesShort: ['Su','Mo','Tu','We','Th','Fr','Sa'],
	buttonText: {
		prev: "<span class='bc-text-arrow'>&lsaquo;</span>",
		next: "<span class='bc-text-arrow'>&rsaquo;</span>",
		prevYear: "<span class='bc-text-arrow'>&laquo;</span>",
		nextYear: "<span class='bc-text-arrow'>&raquo;</span>",
		today: '<strong>Today</strong>',
		month: '<strong>Month</strong>',
		week: '<strong>Week</strong>',
		year: '<strong>Year</strong>',
		day: '<strong>Day</strong>',
		gday: '<strong>Grid</strong>'
	},

	// Weekends
	weekends: {
		SUN: false,
		MON: false,
		TUE: false,
		WED: false,
		THU: false,
		FRI: false,
		SAT: false
	},
	weekendsDays: ['SUN','MON','TUE','WED','THU','FRI','SAT'],


	// jquery-ui theming
	theme: false,
	buttonIcons: {
		prev: 'circle-triangle-w',
		next: 'circle-triangle-e'
	},
	//Unused Color Blendar
	colorBlendar: {
		color10: "#339900",
		color9: "#588600",
		color8: "#6B7D00",
		color7: "#7D7400",
		color6: "#906B00",
		color5: "#A26100",
		color4: "#B55800",
		color3: "#C74F00",
		color2: "#DA4600",
		color1: "#FF3300"
	},
	// colorBlendarArray: ["#FF3300", "#DA4600", "#C74F00", "#B55800", "#A26100", "#906B00", "#7D7400", "#6B7D00", "#588600", "#339900"]
	colorBlendarArray: ["#339900", "#588600", "#6B7D00", "#7D7400", "#906B00", "#A26100", "#B55800", "#C74F00", "#DA4600", "#FF3300"],
	// colorBlendarArray: ["#3BCFED", "#5BC5CD", "#6CC0BC", "#7CBBAC", "#8CB69C", "#9CB08C", "#ACAB7C", "#BCA66C", "#CDA15B", "#ED973B"]

	// Legend Colors
	legendColors: {
		available: "#7DFC96",
		booked: '#FF99FF',
		reserved: '#00CCFF',
		temp_reserve: '#FFA500',
		checkout: '#78FFFF',
		maintenance: '#666666',
		dirty: '#FFFF66',
		occupancy: '#3399FF',
		weekend: '#F5ECCE',
		own_consumption: '#000000',
		void_booking: '#7f007f'
	},

	// Use to store search room numbers
	autoRoomsData: [],
	autoRooms: [],

};


// TOREMOVE Rick Click Options
var menuOptions = {
    open: {name: "Open",disabled: false},
    checkIn: {name: "Check-In",disabled: false},
    cancelReservation: {name: "Cancel Reservation",disabled: false},
    noShow: {name: "Noshow",disabled: false},
    advancePayments: {name: "Make Payments",disabled: false},
    roomService: {name: "Room Service",disabled: false},
    roomSwap: {name: "Room Swap",disabled: false},
    checkOut: {name: "Check-Out",disabled: false},
    stayExtend: {name: "Stay Extend",disabled: false},
    reserve: {name: "Reserve",disabled: false},
    temp_reserve: {name: "Temp Reserve",disabled: false},
    void_booking: {name: "Void Booking",disabled: false},
    updateHKStatus: {name: "Update HK Status",disabled: false},
    blockForMaintenance: {name: "Block for maintenance",disabled: false},
};

var rightClickOptionsStatusWiseForEvents = {
	'RES': {
		"open": menuOptions['open'],
		"checkIn": {name: "Check-In",disabled: true},
		"noShow": menuOptions['noShow'],
		"cancelReservation": menuOptions['cancelReservation'],
		"advancePayments": menuOptions['advancePayments']
	},
	'TEMP': {
		"open": menuOptions['open'],
		"checkIn": {name: "Check-In",disabled: true},
		"noShow": menuOptions['noShow'],
		"cancelReservation": menuOptions['cancelReservation'],
		"advancePayments": menuOptions['advancePayments']
	},

	'BKD': {
		"open": menuOptions['open'],
		"checkOut": menuOptions['checkOut'],
		"stayExtend": menuOptions['stayExtend'],
		"roomSwap": menuOptions['roomSwap'],
		"roomService": menuOptions['roomService'],
		"advancePayments": menuOptions['advancePayments']
	},

	'COT': {
		"open": menuOptions['open'],
		"roomService": menuOptions['roomService'],
		"advancePayments": menuOptions['advancePayments'],
		"sep1": "---------",
		"updateHK": {
			"name": "HK Status",
			"items": {
				"updateHK-status": menuOptions['updateHKStatus'],
				"updateHK-mtn": menuOptions['blockForMaintenance']
			}
		}
	}
};

var rightClickOptionsStatusWise = {
	// Current Day Status
	availableCurrent: {
		"checkIn": menuOptions['checkIn'],
		"reserve": menuOptions['reserve'],
		"void_booking": menuOptions['void_booking'],
		"temp_reserve": menuOptions['temp_reserve'],
		"sep1": "---------",
		"updateHK": {
			"name": "HK Status",
			"items": {
				"updateHK-status": menuOptions['updateHKStatus'],
				"updateHK-mtn": menuOptions['blockForMaintenance']
			}
		}
	},

	reserveCurrent: {
		"open": menuOptions['open'],
		"checkIn": menuOptions['checkIn'],
		"noShow": menuOptions['noShow'],
		"cancelReservation": menuOptions['cancelReservation'],
		"advancePayments": menuOptions['advancePayments']
	},

	dirty: {
		"reserve": menuOptions['reserve'],
		"sep1": "---------",
		"updateHK": {
			"name": "HK Status",
			"items": {
				"updateHK-status": menuOptions['updateHKStatus'],
				"updateHK-mtn": menuOptions['blockForMaintenance']
			}
		}
	},

	// Future Days Status
	available: {
		"reserve": menuOptions['reserve'],
		"sep1": "---------",
		"updateHK": {
			"name": "HK Status",
			"items": {
				// "updateHK-status": menuOptions['updateHKStatus'],
				"updateHK-mtn": menuOptions['blockForMaintenance']
			}
		}
	},

	reserve: {
		"open": menuOptions['open'],
		"noShow": menuOptions['noShow'],
		"cancelReservation": menuOptions['cancelReservation'],
		"advancePayments": menuOptions['advancePayments']
	},

	checkIn: {
		"open": menuOptions['open'],
		"checkOut": menuOptions['checkOut'],
		"stayExtend": menuOptions['stayExtend'],
		"roomSwap": menuOptions['roomSwap'],
		"roomService": menuOptions['roomService'],
		"advancePayments": menuOptions['advancePayments']
	},

	checkOut: {
		"open": menuOptions['open'],
		"roomService": menuOptions['roomService'],
		"advancePayments": menuOptions['advancePayments'],
		"sep1": "---------",
		"updateHK": {
			"name": "HK Status",
			"items": {
				"updateHK-status": menuOptions['updateHKStatus'],
				"updateHK-mtn": menuOptions['blockForMaintenance']
			}
		}
	}

};



;;

// Room Types => 1
// Dormitory => 2
var defaultType = 1
var defaultTypeText = {
	1: "Room Types",
	2: "Dormitory"
}


var bc = $.bookingCalendar = { version: "1.0.0" };
var bcViews = bc.views = {};

$.fn.bookingCalendar = function( options ) {

	// method calling
	if (typeof options == 'string') {
		var args = Array.prototype.slice.call(arguments, 1);
		var res;
		this.each(function() {
			var booking_calendar = $.data(this, 'bookingCalendar');
			if (booking_calendar && $.isFunction(booking_calendar[options])) {
				var r = booking_calendar[options].apply(booking_calendar, args);
				if (res === undefined) {
					res = r;
				}
				if (options == 'destroy') {
					$.removeData(this, 'bookingCalendar');
				}
			}
		});
		if (res !== undefined) {
			return res;
		}
		return this;
	}

	// would like to have this logic in EventManager, but needs to happen before options are recursively extended
	var eventSources = options.eventSources || [];
	delete options.eventSources;
	if (options.bookings) {
		eventSources.push(options.bookings);
		delete options.bookings;
	}

	// Assign 'default" to be used by "options"
	options = $.extend(true, {},
		defaults,
		options
	);


	this.each(function(i, _element) {
		var element = $(_element);
		var booking_calendar = new BookingCalendar(element, options, eventSources); // TODO Booking Events
		element.data('bookingCalendar', booking_calendar); // TODO: look into memory leak implications
		booking_calendar.render();
	});

	return this;
};


// function for adding/overriding defaults
function setDefaults(d) {
	$.extend(true, defaults, d);
}
;;

// Booking Calendar - to be called from View
function BookingCalendar(element, options, eventSources){
	var t = this;

	//exports
	t.options = options;
	t.render = render;
	t.changeView = changeView;
	t.prev = prev;
	t.next = next;
	t.today = today;
	t.gotoDate = gotoDate;
	t.trigger = trigger;
	t.gotoRoom = gotoRoom;

	// imports
	BookingsManager.call(t, options, eventSources);


	// locals
	var _element = element[0];
	var header;
	var headerElement;
	var content;
	var tm; // for making theme classes
	var currentView;
	var elementOuterWidth;
	var suggestedViewHeight;
	var resizeUID = 0;
	var ignoreWindowResize = 0;
	var date = options.business_date;
	var events = [];
	var _dragElement;

	/* Main Rendering
	-----------------------------------------------------------------------------*/

	function render(inc) {
		if (!content) {
			initialRender();
		}
		else if (elementVisible()) {
			// mainly for the public API
			calcSize();
			_renderView(inc);
		}
	}

	function initialRender(){
		tm = options.theme ? 'ui' : 'bc';
		element.addClass('bc'); // TODO Set theme with 'tm'

		content = $("<div class='bcal-content' style='position:relative'/>");
			// .prependTo(element);
		element.html(content);

		header = new Header(t, options);
		headerElement = header.render();
		if (headerElement) {
			element.prepend(headerElement);
		}

		changeView(options.defaultView);

	}

	function elementVisible(){
		return element.is(':visible');
	}

	/* View Rendering
	-----------------------------------------------------------------------------*/
	

	function changeView(newViewName) {
		if (!currentView || newViewName != currentView.name) {
			_changeView(newViewName);
		}
	}

	function _changeView(newViewName) {

		if (currentView) {

			trigger('viewDestroy', currentView, currentView, currentView.element);
			unselect();
			// currentView.triggerEventDestroy(); // trigger 'eventDestroy' for each event
			freezeContentHeight();
			currentView.element.remove();
			header.deactivateButton(currentView.name);
		}

		header.activateButton(newViewName);

		window.localStorage.setItem("defaultView", newViewName);
		
		currentView = new bcViews[newViewName](
			$("<div class='bc-view bc-view-" + newViewName + "' style='position:relative'/>")
				.appendTo(content),
			t // the calendar object
		);

		renderView();
		unfreezeContentHeight();
	}

	function renderView(inc) {
		if (elementVisible()) {
				_renderView(inc);
			}
	}


	function _renderView(inc) { // assumes elementVisible
		ignoreWindowResize++;

		if (currentView.start) { // already been rendered?
			trigger('viewDestroy', currentView, currentView, currentView.element);
			// currentView.element.find('.bc-normal-container').remove();
			currentView.element.empty();
		}

		freezeContentHeight();

		currentView.render(date, inc || 0); // the view's render method ONLY renders the skeleton, nothing else
		setSize();
		unfreezeContentHeight();
		(currentView.afterRender || noop)();

		updateTitle();
		updateTodayButton();

		trigger('viewRender', currentView, currentView, currentView.element);
		currentView.trigger('viewDisplay', _element); // deprecated

		ignoreWindowResize--;

		// getAndRenderEvents();
	}



	/* Actions
	----------------------------------------------------------------------------*/
	function prev(){
		options.gotoDateHighlight = false;
		// addWeeks(date, -7);
		// renderView();
		setValueToGotoInputField(null); // Set the Goto Date Input Value
		setValueForGotoRoom();
		renderView(-1);
	}

	function next(){
		options.gotoDateHighlight = false;
		// addWeeks(date, 7);
		// renderView();
		setValueToGotoInputField(null); // Set the Goto Date Input Value
		setValueForGotoRoom();
		renderView(1);
	}

	function today() {
		options.gotoDateHighlight = false;
		date = new Date();
		setValueToGotoInputField(null); // Set the Goto Date Input Value
		setValueForGotoRoom();
		renderView();
	}

	function gotoDate(year, month, dateOfMonth) {
		options.gotoDateHighlight = true;
		if (year instanceof Date) {
			date = cloneDate(year); // provided 1 argument, a Date
		}else{
			setYMD(date, year, month, dateOfMonth);
		}
		
		// Date Checking
		if(isNaN(date)){
			alert("Invalid Date");
		}else{
			renderView();
		}
	}

	function setValueToGotoInputField(val){
		$(".bc-input-gotodate").val(val);
	}

	function setValueForGotoRoom(){
		if(options.gotoRoomDetails.roomName === null || options.gotoRoomDetails.roomName === "Show All"){
			$("#goto_room").val(null);
		}else{
			$("#goto_room").val(options.gotoRoomDetails.roomName);
		}
	}

	// Goto Room 
	function gotoRoom(roomType,roomNo){
		if(options.gotoRoomFlag){
			options.gotoRoomDetails.roomTypeId = roomType;
			options.gotoRoomDetails.roomNoId = roomNo;
		}
	}

	/* Selection
	-----------------------------------------------------------------------------*/
	

	function select(start, end, allDay) {
		currentView.select(start, end, allDay===undefined ? true : allDay);
	}
	

	function unselect() { // safe to be called before renderView
		if (currentView) {
			// currentView.unselect();
		}
	}


	/* Header Updating
	-----------------------------------------------------------------------------*/


	function updateTitle() {
		header.updateTitle(currentView.title);
	}


	function updateTodayButton() {
		var today = new Date();
		if (today >= currentView.start && today < currentView.end) {
			header.disableButton('today');
		}
		else {
			header.enableButton('today');
		}
	}
	


	/* Height "Freezing"
	-----------------------------------------------------------------------------*/

	function freezeContentHeight() {
		content.css({
			width: '100%',
			height: content.height(),
			overflow: 'hidden'
		});
	}


	function unfreezeContentHeight() {
		content.css({
			width: '',
			height: '',
			overflow: ''
		});
	}


	/* Resizing
	-----------------------------------------------------------------------------*/
	
	
	function updateSize() {
		if (elementVisible()) {
			unselect();
			clearEvents();
			calcSize();
			setSize();
			renderEvents();
		}
	}
	
	
	function calcSize() { // assumes elementVisible
		if (options.contentHeight) {
			suggestedViewHeight = options.contentHeight;
		}
		else if (options.height) {
			suggestedViewHeight = options.height - (headerElement ? headerElement.height() : 0) - vsides(content);
		}
		else {
			suggestedViewHeight = Math.round(content.width() / Math.max(options.aspectRatio, .5));
		}
	}
	
	
	function setSize() { // assumes elementVisible

		if (suggestedViewHeight === undefined) {
			calcSize(); // for first time
				// NOTE: we don't want to recalculate on every renderView because
				// it could result in oscillating heights due to scrollbars.
		}

		ignoreWindowResize++;
		// currentView.setHeight(suggestedViewHeight);
		// currentView.setWidth(content.width());
		ignoreWindowResize--;

		elementOuterWidth = element.outerWidth();
	}
	
	
	function windowResize() {
		if (!ignoreWindowResize) {
			if (currentView.start) { // view has already been rendered
				var uid = ++resizeUID;
				setTimeout(function() { // add a delay
					if (uid == resizeUID && !ignoreWindowResize && elementVisible()) {
						if (elementOuterWidth != (elementOuterWidth = element.outerWidth())) {
							ignoreWindowResize++; // in case the windowResize callback changes the height
							updateSize();
							currentView.trigger('windowResize', _element);
							ignoreWindowResize--;
						}
					}
				}, 200);
			}else{
				// calendar must have been initialized in a 0x0 iframe that has just been resized
				lateRender();
			}
		}
	}
	
	
	
	
	
	/* Misc
	-----------------------------------------------------------------------------*/

	function noop() { }
	
	
	function getView() {
		return currentView;
	}
	
	
	function option(name, value) {
		if (value === undefined) {
			return options[name];
		}
		if (name == 'height' || name == 'contentHeight' || name == 'aspectRatio') {
			options[name] = value;
			updateSize();
		}
	}
	
	
	function trigger(name, thisObj) {
		if (options[name]) {
			return options[name].apply(
				thisObj || _element,
				Array.prototype.slice.call(arguments, 2)
			);
		}
	}

}

;;
// Show header for Calendar
function Header(calendar, options){
	var t = this;

	// exports
	t.render = render;
	t.destroy = destroy;
	t.updateTitle = updateTitle;
	t.activateButton = activateButton;
	t.deactivateButton = deactivateButton;
	t.disableButton = disableButton;
	t.enableButton = enableButton;

	// locals
	var element = $([]);
	var tm;

	function render(){
		tm = options.theme ? 'ui' : 'bc';
		var sections = options.header;
		if (sections) {
			element = $("<table class='bc-header' style='width:100%'/>")
				.append(
					$("<tr/>")
						.append(renderSection('left'))
						.append(renderSection('center'))
						.append(renderSection('right'))
				);
			return element;
		}
	}

	function destroy() {
		element.remove();
	}

	// TOOD - Separate independent methods
	function renderSection(position) {
		var e = $("<td class='bc-header-" + position + "'/>");
		var buttonStr = options.header[position];
		if (buttonStr) {
			$.each(buttonStr.split(' '), function(i) {
				if (i > 0) {
					e.append("<span class='bc-header-space'/>");
				}
				var prevButton;
				$.each(this.split(','), function(j, buttonName) {

					if (buttonName === 'title') {
						e.append("<span class='bc-header-title'><h2>&nbsp;</h2></span>");
						if (prevButton) {
							prevButton.addClass(tm + '-corner-right');
						}
						prevButton = null;
					}else if(buttonName === 'gotoDate'){
						dateField = $("<span />").attr({'id':'gotoDate'}).addClass('input-append date')
										.append('<span class="add-on bc-text-icon-calendar"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span>')
										.append(
												$("<input />",{'type':'text','placeholder':'Goto Date'})
													.addClass('bc-input bc-input-gotodate')
													.attr({'data-format':'dd/MM/yyyy'})
													.css({'height': '23px'})
											)
										// Date Picker
										.datetimepicker({
											language: 'en',
											pickTime: false
										})
										// On chenge
										.on('hide',function(ev){
											// TODO Change the logic here for date formating
											_v = $(this).find('input').val();
											_f = _v.split('/');
											_d = new Date(_f[1]+'/'+_f[0]+'/'+_f[2]);
											// $(this).val('');
											calendar.gotoDate( _d.getFullYear(), _d.getMonth(), _d.getDate() );
										});
						dateField.appendTo(e);
					}else if(buttonName === 'gotoRoom'){
						options.gotoRoomFlag = true;
						roomField = $("<span />")
									.append($('<span />').addClass('bc-text-icon-calendar').append('<i class="icon-tags"></i>'))
									.append(
										$("<input />",{'type':'text','placeholder':'Goto Room','id':'goto_room','onkeypress':'return IsAlphaNumeric(event);'})
											.addClass('bc-input bc-input-gotodate')
											.css({'height':'23px'})
											.attr({'data-toggle':'popover'})
											.attr({'data-trigger':'manual'})
											.attr({'data-placement':'bottom'})
											.attr({'data-html':true})
											.attr({'data-delay':150})
											.on("click", function () {
											   $(this).select();
											})
											.focus(function(){
												$(this).popover('hide');
											})
									)

									.append(
										$('<span />').addClass('bc-button bc-state-default bc-corner-right').hover(function(){$(this).addClass(tm + '-state-hover');},function(){$(this).removeClass(tm + '-state-hover')}).append('<strong>Go</strong>')
											.click(function(){
												var selectedRoom = $("#goto_room").val();

												var arr = (localStorage["recentList"])?localStorage["recentList"]:'';

												if(selectedRoom === ''){
													$("#goto_room").attr({'data-content':'<span class="text-danger">Please enter Room No</span>'})
													$("#goto_room").popover('show');
													return false;
												}else{
													var flag;
													options.gotoRoomDetails.roomName = selectedRoom;
													
													if(selectedRoom.toLowerCase() === "show all"){
														options.gotoRoomDetails.roomTypeId = null
														options.gotoRoomDetails.roomNoId = null
														flag = true;
													}else{
														for(var k=0;k<options.autoRooms.length;k++){
															var _room_name = options.autoRooms[k].split(' ');
															if(_room_name[0].toLowerCase()===selectedRoom.toLowerCase()){

																var rList = arr.split(';');
																var _rList = [];
																
																for( h=0,j=1;h<5 && j<5;h++,j++ ){
																	if(h==0) _rList[h] = selectedRoom;

																	_rList[j] = rList[h];
																}
																												
																localStorage.clear;
																localStorage["recentList"] = unique(_rList).join(';');

																options.gotoRoomDetails.roomTypeId = options.autoRoomsData[k].room_type_id;
																options.gotoRoomDetails.roomNoId = options.autoRoomsData[k].room_id;
																flag = true;
															}else{

															}
														}
													}
													
													if(!flag){
														$("#goto_room").attr({'data-content':'<span class="text-danger">Please enter valid Room No</span>'});
														$("#goto_room").popover('show');
														return false;
													}
													calendar.render();
												}
												
											})
									)
									.append('<input type="hidden" id="goto_room_type_id" value=""/><input type="hidden" id="goto_room_id" value=""/>');
						var _div = $("<div/>").attr({'id':'auto_complete_div'}).html('<div id="auto-output" style="overflow-y: scroll; max-height: 91px;"></div>');
						roomField.appendTo(e);
						_div.appendTo(e);
						_div.find('div').niceScroll();
					}else if(buttonName === 'chartSelect' && false){
						selectField = $("<span />").addClass('form-group')
										.append(
												$('<select />')
													.append('<option value="">Select Options</option>')
													.append('<option value="bar">Bar</option>')
													.append('<option value="pie">Pie</option>')
													.append('<option value="tristate">Tristate</option>')
													.append('<option value="line">Line</option>')
													.append('<option value="discrete">Discrete</option>')
													.append('<option value="bullet">Bullet</option>')
													.append('<option value="box">Box</option>')
											);
							selectField.appendTo(e);			
					}else{
						var buttonClick;
						if (calendar[buttonName]) {
							buttonClick = calendar[buttonName]; // calendar method
						}
						else if (bcViews[buttonName]) {
							buttonClick = function() {
								button.removeClass(tm + '-state-hover'); // forget why
								calendar.changeView(buttonName);
							};
						}
						if (buttonClick) {
							var icon = options.theme ? smartProperty(options.buttonIcons, buttonName) : null; // why are we using smartProperty here?
							var text = smartProperty(options.buttonText, buttonName); // why are we using smartProperty here?
							var button = $(
								"<span class='bc-button bc-button-" + buttonName + " " + tm + "-state-default'>" +
									(icon ?
										"<span class='bc-icon-wrap'>" +
											"<span class='ui-icon ui-icon-" + icon + "'/>" +
										"</span>" :
										text
										) +
								"</span>"
								)
								.click(function() {
									if (!button.hasClass(tm + '-state-disabled')) {
										buttonClick();
									}
								})
								.mousedown(function() {
									button
										.not('.' + tm + '-state-active')
										.not('.' + tm + '-state-disabled')
										.addClass(tm + '-state-down');
								})
								.mouseup(function() {
									button.removeClass(tm + '-state-down');
								})
								.hover(
									function() {
										button
											.not('.' + tm + '-state-active')
											.not('.' + tm + '-state-disabled')
											.addClass(tm + '-state-hover');
									},
									function() {
										button
											.removeClass(tm + '-state-hover')
											.removeClass(tm + '-state-down');
									}
								)
								.appendTo(e);
							disableTextSelection(button);
							if (!prevButton) {
								button.addClass(tm + '-corner-left');
							}
							prevButton = button;
						}
					}
				});
				if (prevButton) {
					prevButton.addClass(tm + '-corner-right');
				}
			});
		}
		return e;
	}

	function unique(array){
	    return array.filter(function(el,index,arr){
	        return index == arr.indexOf(el);
	    });
	}

	function updateTitle(html) {
		element.find('h2')
			.html(html);
	}

	function activateButton(buttonName) {
		element.find('span.bc-button-' + buttonName)
			.addClass(tm + '-state-active');
	}
	
	
	function deactivateButton(buttonName) {
		element.find('span.bc-button-' + buttonName)
			.removeClass(tm + '-state-active');
	}
	
	
	function disableButton(buttonName) {
		element.find('span.bc-button-' + buttonName)
			.addClass(tm + '-state-disabled');
	}
	
	
	function enableButton(buttonName) {
		element.find('span.bc-button-' + buttonName)
			.removeClass(tm + '-state-disabled');
	}

}

;;


function BookingsManager(options, _sources){
	var t = this;

	// console.log(_sources);
}




;;

bc.addDays = addDays;
bc.addWeeks = addWeeks;
bc.cloneDate = cloneDate;
bc.parseDate = parseDate;
bc.parseISO8601 = parseISO8601;
bc.parseTime = parseTime;
// bc.formatDate = formatDate;
// bc.formatDates = formatDates;



/* Date Math
-----------------------------------------------------------------------------*/

var dayIDs = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'],
	DAY_MS = 86400000,
	HOUR_MS = 3600000,
	MINUTE_MS = 60000;
	

function addYears(d, n, keepTime) {
	d.setFullYear(d.getFullYear() + n);
	if (!keepTime) {
		clearTime(d);
	}
	return d;
}



function addMonths(d, n, keepTime) { // prevents day overflow/underflow
	if (+d) { // prevent infinite looping on invalid dates
		var m = d.getMonth() + n,
			check = cloneDate(d);
		check.setDate(1);
		check.setMonth(m);
		d.setMonth(m);
		if (!keepTime) {
			clearTime(d);
		}
		while (d.getMonth() != check.getMonth()) {
			d.setDate(d.getDate() + (d < check ? 1 : -1));
		}
	}
	return d;
}

function addWeeks(d, n, keepTime){
	if(n == 1){
		n = 7;
	}else if(n == -1){
		n = -7;
	}
	if(+d){
		var dd = d.getDate() + n,
			check = cloneDate(d);
		check.setDate(dd);
		d.setDate(dd);
		if (!keepTime) {
			clearTime(d);
		}
		fixDate(d, check);
	}
}


function addDays(d, n, keepTime) { // deals with daylight savings
	if (+d) {
		var dd = d.getDate() + n,
			check = cloneDate(d);
		check.setHours(9); // set to middle of day
		check.setDate(dd);
		d.setDate(dd);
		if (!keepTime) {
			clearTime(d);
		}
		fixDate(d, check);
	}
	return d;
}


function fixDate(d, check) { // force d to be on check's YMD, for daylight savings purposes
	if (+d) { // prevent infinite looping on invalid dates
		while (d.getDate() != check.getDate()) {
			d.setTime(+d + (d < check ? 1 : -1) * HOUR_MS);
		}
	}
}


function addMinutes(d, n) {
	d.setMinutes(d.getMinutes() + n);
	return d;
}


function clearTime(d) {
	d.setHours(0);
	d.setMinutes(0);
	d.setSeconds(0); 
	d.setMilliseconds(0);
	return d;
}


function cloneDate(d, dontKeepTime) {
	if (dontKeepTime) {
		return clearTime(new Date(+d));
	}
	return new Date(+d);
}


function zeroDate() { // returns a Date with time 00:00:00 and dateOfMonth=1
	var i=0, d;
	do {
		d = new Date(1970, i++, 1);
	} while (d.getHours()); // != 0
	return d;
}


function dayDiff(d1, d2) { // d1 - d2
	return Math.round((cloneDate(d1, true) - cloneDate(d2, true)) / DAY_MS);
}

function isCurrentDate(d1){
	d2 = new Date();
	if (d1.getDate() === d2.getDate() && d1.getMonth() === d2.getMonth() && d1.getFullYear() === d2.getFullYear()){
		return true;
	}else{
		return false;
	}
}

function isEqualDates(d1,d2){
	return (
					(d1.getDate() === d2.getDate()) &&
					(d1.getMonth() === d2.getMonth()) &&
					(d1.getFullYear() === d2.getFullYear())
			   );
}

function setYMD(date, y, m, d) {
	if (y !== undefined && y != date.getFullYear()) {
		date.setDate(1);
		date.setMonth(0);
		date.setFullYear(y);
	}
	if (m !== undefined && m != date.getMonth()) {
		date.setDate(1);
		date.setMonth(m);
	}
	if (d !== undefined) {
		date.setDate(d);
	}
}

function appendZero(val){
	return (parseInt(val) < 10)?("0"+val):val;
}



/* Date Parsing
-----------------------------------------------------------------------------*/


function parseDate(s, ignoreTimezone) { // ignoreTimezone defaults to true
	if (typeof s == 'object') { // already a Date object
		return s;
	}
	if (typeof s == 'number') { // a UNIX timestamp
		return new Date(s * 1000);
	}
	if (typeof s == 'string') {
		if (s.match(/^\d+(\.\d+)?$/)) { // a UNIX timestamp
			return new Date(parseFloat(s) * 1000);
		}
		if (ignoreTimezone === undefined) {
			ignoreTimezone = true;
		}
		return parseISO8601(s, ignoreTimezone) || (s ? new Date(s) : null);
	}
	// TODO: never return invalid dates (like from new Date(<string>)), return null instead
	return null;
}


function parseISO8601(s, ignoreTimezone) { // ignoreTimezone defaults to false
	// derived from http://delete.me.uk/2005/03/iso8601.html
	// TODO: for a know glitch/feature, read tests/issue_206_parseDate_dst.html
	var m = s.match(/^([0-9]{4})(-([0-9]{2})(-([0-9]{2})([T ]([0-9]{2}):([0-9]{2})(:([0-9]{2})(\.([0-9]+))?)?(Z|(([-+])([0-9]{2})(:?([0-9]{2}))?))?)?)?)?$/);
	if (!m) {
		return null;
	}
	var date = new Date(m[1], 0, 1);
	if (ignoreTimezone || !m[13]) {
		var check = new Date(m[1], 0, 1, 9, 0);
		if (m[3]) {
			date.setMonth(m[3] - 1);
			check.setMonth(m[3] - 1);
		}
		if (m[5]) {
			date.setDate(m[5]);
			check.setDate(m[5]);
		}
		fixDate(date, check);
		if (m[7]) {
			date.setHours(m[7]);
		}
		if (m[8]) {
			date.setMinutes(m[8]);
		}
		if (m[10]) {
			date.setSeconds(m[10]);
		}
		if (m[12]) {
			date.setMilliseconds(Number("0." + m[12]) * 1000);
		}
		fixDate(date, check);
	}else{
		date.setUTCFullYear(
			m[1],
			m[3] ? m[3] - 1 : 0,
			m[5] || 1
		);
		date.setUTCHours(
			m[7] || 0,
			m[8] || 0,
			m[10] || 0,
			m[12] ? Number("0." + m[12]) * 1000 : 0
		);
		if (m[14]) {
			var offset = Number(m[16]) * 60 + (m[18] ? Number(m[18]) : 0);
			offset *= m[15] == '-' ? 1 : -1;
			date = new Date(+date + (offset * 60 * 1000));
		}
	}
	return date;
}


function parseTime(s) { // returns minutes since start of day
	if (typeof s == 'number') { // an hour
		return s * 60;
	}
	if (typeof s == 'object') { // a Date object
		return s.getHours() * 60 + s.getMinutes();
	}
	var m = s.match(/(\d+)(?::(\d+))?\s*(\w+)?/);
	if (m) {
		var h = parseInt(m[1], 10);
		if (m[3]) {
			h %= 12;
			if (m[3].toLowerCase().charAt(0) == 'p') {
				h += 12;
			}
		}
		return h * 60 + (m[2] ? parseInt(m[2], 10) : 0);
	}
}

// Set Time Line
function setTimeLine(element,_date){
	var timeline = element.children(".timeline");
	if (timeline.length == 0) { //if timeline isn't there, add it
	       timeline = $("<div>").addClass("timeline")
	   						.html("<div class='arrow-down'>");
	       element.prepend(timeline);
	}

	var curTime = new Date();

	if(isCurrentDate(_date)){
		timeline.show();
	}else{
		timeline.hide();
	}


	var curSeconds = (curTime.getHours() * 60 * 60) + (curTime.getMinutes() * 60) + curTime.getSeconds();
	var percentOfDay = curSeconds / 86400; //24 * 60 * 60 = 86400, # of seconds in a day

    _tds = $(".bc-calendar").find('td');
	var _pr=0;
	
	_tds.each(function(key,value){
		if(key != 0){
			_pr += $(value).width()+1;
		}
	});

	var total_wi = element.width() - _pr;

    var topLoc = Math.floor(_pr * percentOfDay);

	timeline.css("left", (topLoc+total_wi) + "px");
}

;;

function findPosY(obj){
	var curtop = 0;
	if (obj.offsetParent){
		curtop += obj.offsetHeight;
		while (obj.offsetParent){
			curtop += obj.offsetTop;
			obj = obj.offsetParent;
		}
	}else if (obj.y){
		curtop += obj.y;
		curtop += obj.height;
	}
	return curtop;
}

;;


// TODO - Change the View for depend upon requirements

bcViews.year = YearView;

function YearView(element, calendar) {
	var t = this;

	// exports
	t.render = render;

	// imports
	BasicView.call(t, element, calendar, 'year');
	// var opt = t.opt;
	var renderBasic = t.renderBasic;
	// var skipHiddenDays = t.skipHiddenDays;
	// var getCellsPerWeek = t.getCellsPerWeek;
	// var formatDate = calendar.formatDate;


	function render(date, delta){

		if (delta) {
			addMonths(date, delta);
			date.setDate(1);
		}

		var start = cloneDate(date, true);
		start.setDate(1);

		var end = addMonths(cloneDate(start), 1);

		datesArray = buildDates(date);


		// TODO CAlculate rowCnt & colCnt
		rowCnt = 6;
		colCnt = datesArray.totalCols;
		t.title = datesArray.title;
		t.start = start;
		t.end = end;


		renderBasic(rowCnt, colCnt, datesArray);
	}

	function buildDates(_date){
		arr = [];
		_monthArray = [];
		dayStart = cloneDate(_date);
		dayEnd = cloneDate(_date);

		monthStart = cloneDate(_date);
		monthEnd = cloneDate(_date);

		monthStart.setMonth(dayStart.getMonth() - 2);
		monthEnd.setMonth(dayEnd.getMonth() + 9);


		if(monthStart.getFullYear() === monthEnd.getFullYear()){
			ti = monthStart.getFullYear();
		}else{
			ti = monthStart.getFullYear()+' - '+monthEnd.getFullYear()
		}


		// Dates
		_d = cloneDate(monthStart);
		
		for(i=0;i<=11;i++){
			if (_d.getMonth() === _date.getMonth()){
				str = '<strong id="current-date">'+calendar.options.monthNames[_d.getMonth()]+'</strong>';
			}else{
				str = calendar.options.monthNames[_d.getMonth()];
			}
			arr.push(str);
			
			// Calculate Dates
			if (_d.getMonth() === _date.getMonth()) {
				_c = 'current';
			}else if(_d > new Date()){
				_c = 'future';
			}else{
				_c = 'past';
			}

			_monthArray.push({month:calendar.options.monthNames[_d.getMonth()],isCurrent:_c})
			
			_d.setMonth(_d.getMonth() + 1);
		}
		
		return {	dateTitles:arr,
					firstDate:(dayStart.getMonth()+1)+'/'+dayStart.getDate()+'/'+dayStart.getFullYear(),
					lastDate:(dayEnd.getMonth()+1)+'/'+dayEnd.getDate()+'/'+dayEnd.getFullYear(),
					totalCols:arr.length,
					title:ti,
					dates:[],
					months:_monthArray,
					firstMonth: monthStart,
					lastMonth: monthEnd
				};
	}

}

;;


bcViews.month = MonthView;

function MonthView(element, calendar) {
	var t = this;

	// exports
	t.render = render;

	// imports
	BasicView.call(t, element, calendar, 'month');
	// var opt = t.opt;
	var renderBasic = t.renderBasic;
	// var skipHiddenDays = t.skipHiddenDays;
	// var getCellsPerWeek = t.getCellsPerWeek;
	// var formatDate = calendar.formatDate;


	function render(date, delta){

		if (delta) {
			addWeeks(date, delta);
			// date.setDate(1);
		}

		datesArray = buildDates(date);


		// TODO CAlculate rowCnt & colCnt
		rowCnt = 6;
		colCnt = datesArray.totalCols;
		t.title = datesArray.title;

		t.start = new Date(datesArray.firstDate);
		t.end = new Date(datesArray.lastDate);


		renderBasic(rowCnt, colCnt, datesArray);

		// Create SparkLines for room Types 
		// Arguments 
		// 		1. type
		//		2. offset
		//		3. barWidth
		//		4. barSpacing
		//		5. zeroAxis
		//		6. negBarColor
		//		7. zeroColor
		// buildSparkLines("bar");
	}

	function buildDates(_date){

		arr = [];
		_dateArray = [];
		dayStart = cloneDate(_date);
		dayEnd = cloneDate(_date);
		
		dayStart.setDate(dayStart.getDate() - 7);
		dayEnd.setDate(dayEnd.getDate() + 23);

		if(dayStart.getMonth() === dayEnd.getMonth()){
			ti = defaults.monthNames[dayStart.getMonth()]+' '+dayStart.getFullYear();
		}else if(dayStart.getFullYear() !== dayEnd.getFullYear()){
			ti = defaults.monthNamesShort[dayStart.getMonth()]+'\''+dayStart.getFullYear().toString().slice(-2)+' - '+defaults.monthNamesShort[dayEnd.getMonth()]+'\''+dayEnd.getFullYear().toString().slice(-2);
		}else{
			ti = defaults.monthNamesShort[dayStart.getMonth()]+'-'+defaults.monthNamesShort[dayEnd.getMonth()]+' '+dayStart.getFullYear();
		}


		// Dates
		_d = cloneDate(dayStart);
		for(i=0;_d <= dayEnd;i++){
			if (isCurrentDate(_d)){
				str = '<strong id="current-date">'+_d.getDate()+'<br>'+defaults.dayNamesShort[_d.getDay()]+'</strong>';
			}else if(calendar.options.gotoDateHighlight && (_d.getDate() === _date.getDate() && _d.getMonth() === _date.getMonth() && _d.getFullYear() === _date.getFullYear() )){
				str = '<strong id="goto-date">'+_d.getDate()+'<br>'+defaults.dayNamesShort[_d.getDay()]+'</strong>';
			}else{
				str = _d.getDate()+'<br>'+defaults.dayNamesShort[_d.getDay()]
			}
			arr.push(str);

			// _w = !!(_d.getDay() === 0 || _d.getDay() === 6); //? true : false;
			_w = !!( calendar.options.weekends[defaults.weekendsDays[_d.getDay()]] );

			// _c = (isCurrentDate(_d))? true : false;
			
			// Calculate Dates
			if( isEqualDates(_d,calendar.options.business_date) ){
				_c = 'business-date'
			}else if (isCurrentDate(_d)) {
				_c = 'current'
			}else if(_d > new Date()){
				_c = 'future'
			}else{
				_c = 'past'
			}

			_dateArray.push({date:(_d.getMonth()+1)+'/'+_d.getDate()+'/'+_d.getFullYear(),isWeekend:_w,isCurrent:_c})
			
			_d.setDate(_d.getDate() + 1);
		}
		
		return {dateTitles:arr,firstDate:(dayStart.getMonth()+1)+'/'+dayStart.getDate()+'/'+dayStart.getFullYear(),lastDate:(dayEnd.getMonth()+1)+'/'+dayEnd.getDate()+'/'+dayEnd.getFullYear(),totalCols:arr.length,title:ti,dates:_dateArray};
	}

}

;;



bcViews.week = WeeklyView;

function WeeklyView(element, calendar) {
	var t = this;

	// exports
	t.render = render;

	// imports
	BasicView.call(t, element, calendar, 'week');
	// var opt = t.opt;
	var renderBasic = t.renderBasic;
	// var skipHiddenDays = t.skipHiddenDays;
	// var getCellsPerWeek = t.getCellsPerWeek;
	// var formatDate = calendar.formatDate;


	function render(date, delta){

		if (delta) {
			addDays(date, delta);
			// date.setDate(1);
		}

		// var start = cloneDate(date, true);
		// var end = addDays(cloneDate(start), 1);

		datesArray = buildDates(date);

		t.start = new Date(datesArray.firstDate);
		t.end = new Date(datesArray.lastDate);


		// TODO CAlculate rowCnt & colCnt
		rowCnt = 6;
		colCnt = datesArray.totalCols;
		t.title = datesArray.title;

		// t.start = start;
		// t.end = end;


		renderBasic(rowCnt, colCnt, datesArray);


		// Create SparkLines for room Types 
		// Arguments 
		// 		1. type
		//		2. offset
		//		3. barWidth
		//		4. barSpacing
		//		5. zeroAxis
		//		6. negBarColor
		//		7. zeroColor
		//buildSparkLines("bar",15,6,2,'#f04040 ','#13c116');
	}

	function buildDates(_date){
		arr = [];
		_dateArray = [];
		dayStart = cloneDate(_date);
		dayEnd = cloneDate(_date);
		
		dayStart.setDate(dayStart.getDate() - 2);
		dayEnd.setDate(dayEnd.getDate() + 4);

		if(dayStart.getMonth() === dayEnd.getMonth()){
			ti = defaults.monthNames[dayStart.getMonth()]+' '+dayStart.getFullYear();
		}else if(dayStart.getFullYear() !== dayEnd.getFullYear()){
			ti = defaults.monthNamesShort[dayStart.getMonth()]+'\''+dayStart.getFullYear().toString().slice(-2)+' - '+defaults.monthNamesShort[dayEnd.getMonth()]+'\''+dayEnd.getFullYear().toString().slice(-2);
		}else{
			ti = defaults.monthNamesShort[dayStart.getMonth()]+'-'+defaults.monthNamesShort[dayEnd.getMonth()]+' '+dayStart.getFullYear();
		}

		// Dates
		_d = cloneDate(dayStart);
		
		for(i=0;_d <= dayEnd;i++){
			if (isCurrentDate(_d)){
				str = '<strong id="current-date">'+_d.getDate()+'<br>'+defaults.dayNames[_d.getDay()]+'</strong>';
			}else if(calendar.options.gotoDateHighlight && (_d.getDate() === _date.getDate() && _d.getMonth() === _date.getMonth() && _d.getFullYear() === _date.getFullYear() )){
				str = '<strong id="goto-date">'+_d.getDate()+'<br>'+defaults.dayNames[_d.getDay()]+'</strong>';
			}else{
				str = _d.getDate()+'<br>'+defaults.dayNames[_d.getDay()]
			}
			arr.push(str);

			// _w = !!(_d.getDay() === 0 || _d.getDay() === 6); // ? true : false;
			// user defind
			_w = !!( calendar.options.weekends[defaults.weekendsDays[_d.getDay()]] );
			// _c = (isCurrentDate(_d))? true : false;
			
			// Calculate Dates
			if (isCurrentDate(_d)) {
				_c = 'current'
			}else if(_d > new Date()){
				_c = 'future'
			}else{
				_c = 'past'
			}

			_dateArray.push({date:(_d.getMonth()+1)+'/'+_d.getDate()+'/'+_d.getFullYear(),isWeekend:_w,isCurrent:_c})
			
			_d.setDate(_d.getDate() + 1);
		}
		
		return {dateTitles:arr,firstDate:(dayStart.getMonth()+1)+'/'+dayStart.getDate()+'/'+dayStart.getFullYear(),lastDate:(dayEnd.getMonth()+1)+'/'+dayEnd.getDate()+'/'+dayEnd.getFullYear(),totalCols:arr.length,title:ti,dates:_dateArray};
	}

};


// Day View
bcViews.day = DayView;
function DayView(element, calendar){
	var t = this;

	// exports
	t.render = render;

	// imports
	BasicView.call(t, element, calendar, 'day');
	var renderBasic = t.renderBasic;


	function render(date, delta){

		if (delta) {
			addDays(date, delta);
		}


		datesArray = buildDates(date);

		t.start = new Date(datesArray.firstDate);
		t.end = new Date(datesArray.lastDate);


		// TODO CAlculate rowCnt & colCnt
		rowCnt = 6;
		colCnt = datesArray.totalCols;
		t.title = datesArray.title;

		renderBasic(rowCnt, colCnt, datesArray);
		// Set Time Line Marker		
		if(isCurrentDate(date)){
			// First Time
			setTimeLine(element,date);

			var recursionMethod = function()
			{
			   this.interval = 30000;
			   var recursionMethod = this;
			   this.play = function() {
			     setTimeLine(element,date);
			   };
			};
			$(function(){
			   var rM = new recursionMethod();
			   setInterval(rM.play, rM.interval);
			});
		}
	}

	function buildDates(_date){
		arr = [];
		_dateArray = {};
		_timeArray = [];
		dayStart = cloneDate(_date);
		dayEnd = cloneDate(_date);

		
		// Title
		ti = defaults.dayNames[dayStart.getDay()]+' '+dayStart.getDate()+'-'+defaults.monthNamesShort[dayStart.getMonth()]+'-'+dayStart.getFullYear();

		// Dates
		_d = cloneDate(dayStart);
		
		if (isCurrentDate(_d)){
			str = '<strong id="current-date">'+_d.getDate()+'<br>'+defaults.dayNames[_d.getDay()]+'</strong>';
		}else if(calendar.options.gotoDateHighlight && (_d.getDate() === _date.getDate() && _d.getMonth() === _date.getMonth() && _d.getFullYear() === _date.getFullYear() )){
			str = '<strong id="goto-date">'+_d.getDate()+'<br>'+defaults.dayNames[_d.getDay()]+'</strong>';
		}else{
			str = _d.getDate()+'<br>'+defaults.dayNames[_d.getDay()]
		}
		// arr.push(str);

		// _w = !!(_d.getDay() === 0 || _d.getDay() === 6); //? true : false;
		_w = !!( calendar.options.weekends[defaults.weekendsDays[_d.getDay()]] );
					
		// Calculate Dates
		if (isCurrentDate(_d)) {
			_c = 'current'
		}else if(_d > new Date()){
			_c = 'future'
		}else{
			_c = 'past'
		}

		// _dateArray.push({date:(_d.getMonth()+1)+'/'+_d.getDate()+'/'+_d.getFullYear(),isWeekend:_w,isCurrent:_c})
		_dateArray.date = (_d.getMonth()+1)+'/'+_d.getDate()+'/'+_d.getFullYear();
		_dateArray.isWeekend = _w;
		_dateArray.isCurrent = _c;
		
		for(i=0;i<24;i++){
			_timeArray.push({hour:i});
		}

		
		_arr = ['00<br/>AM','01<br/>AM','02<br/>AM','03<br/>AM','04<br/>AM','05<br/>AM','06<br/>AM','07<br/>AM','08<br/>AM','09<br/>AM','10<br/>AM','11<br/>AM','00<br/>PM','01<br/>PM','02<br/>PM','03<br/>PM','04<br/>PM','05<br/>PM','06<br/>PM','07<br/>PM','08<br/>PM','09<br/>PM','10<br/>PM','11<br/>PM'];

		return {dateTitles:_arr,firstDate:(dayStart.getMonth()+1)+'/'+dayStart.getDate()+'/'+dayStart.getFullYear(),lastDate:(dayEnd.getMonth()+1)+'/'+dayEnd.getDate()+'/'+dayEnd.getFullYear(),totalCols:arr.length,title:ti,dates:_dateArray,time:_timeArray};
	}

};

// Grid View
bcViews.gday = GridView;
function GridView(element, calendar){
	var t = this;

	// exports
	t.render = render;

	// imports
	BasicView.call(t, element, calendar, 'gday');
	var renderBasic = t.renderBasic;

	function render(date, delta){

		if (delta) {
			addDays(date, delta);
		}


		datesArray = buildDates(date); 

		t.start = new Date(datesArray.firstDate);
		t.end = new Date(datesArray.lastDate);


		// TODO CAlculate rowCnt & colCnt
		rowCnt = 6;
		colCnt = datesArray.totalCols;
		t.title = datesArray.title;

		renderBasic(rowCnt, colCnt, datesArray);
		// Set Time Line Marker		
		if(isCurrentDate(date)){
			// First Time
			setTimeLine(element,date);

			var recursionMethod = function()
			{
			   this.interval = 30000;
			   var recursionMethod = this;
			   this.play = function() {
			     setTimeLine(element,date);
			   };
			};
			$(function(){
			   var rM = new recursionMethod();
			   setInterval(rM.play, rM.interval);
			});
		}
	}

	function buildDates(_date){ // Grid Day one DATE
		arr = [];
		_dateArray = [];
		dayStart = cloneDate(_date);
		dayEnd = cloneDate(_date);
		
		
		// Title
		ti = defaults.dayNames[dayStart.getDay()]+' '+dayStart.getDate()+'-'+defaults.monthNamesShort[dayStart.getMonth()]+'-'+dayStart.getFullYear();

		// Dates
		_d = cloneDate(dayStart);
		
		for(i=0;_d <= dayEnd;i++){
			if (isCurrentDate(_d)){
				str = '<strong id="current-date">'+_d.getDate()+'<br>'+defaults.dayNames[_d.getDay()]+'</strong>';
			}else if(calendar.options.gotoDateHighlight && (_d.getDate() === _date.getDate() && _d.getMonth() === _date.getMonth() && _d.getFullYear() === _date.getFullYear() )){
				str = '<strong id="goto-date">'+_d.getDate()+'<br>'+defaults.dayNames[_d.getDay()]+'</strong>';
			}else{
				str = _d.getDate()+'<br>'+defaults.dayNames[_d.getDay()]
			}
			arr.push(str);

			// _w = !!(_d.getDay() === 0 || _d.getDay() === 6); // ? true : false;
			// user defind
			_w = !!( calendar.options.weekends[defaults.weekendsDays[_d.getDay()]] );
			// _c = (isCurrentDate(_d))? true : false;
			
			// Calculate Dates
			if (isCurrentDate(_d)) {
				_c = 'current'
			}else if(_d > new Date()){
				_c = 'future'
			}else{
				_c = 'past'
			}

			_dateArray.push({date:(_d.getMonth()+1)+'/'+_d.getDate()+'/'+_d.getFullYear(),isWeekend:_w,isCurrent:_c})
			
			_d.setDate(_d.getDate() + 1);
		}
		
		return {dateTitles:arr,firstDate:(dayStart.getMonth()+1)+'/'+dayStart.getDate()+'/'+dayStart.getFullYear(),lastDate:(dayEnd.getMonth()+1)+'/'+dayEnd.getDate()+'/'+dayEnd.getFullYear(),totalCols:arr.length,title:ti,dates:_dateArray};
	}
	
}

var ajaxDefaults = {
	dataType: 'json',
	cache: false
};

setDefaults({
	weekMode: 'fixed'
});


function BasicView(element, calendar, viewName) {
	var t = this;

	//exports
	t.element = element;
	t.calendar = calendar;
	t.name = viewName;
	t.renderBasic = renderBasic;
	t.trigger = trigger;
	t.clearSelection = clearSelection;


	// locals
	
	var table;
	var tbody;
	var td;
	var head;
	var headCells;
	var body;
	var bodyRows;
	var bodyCells;
	var bodyFirstCells;
	var firstRowCellInners;
	var firstRowCellContentInners;
	var daySegmentContainer;
	
	var viewWidth;
	var viewHeight;
	var colWidth;
	var weekNumberWidth;
	
	var rowCnt, colCnt;
	var showNumbers;
	var coordinateGrid;
	var hoverListener;
	var colPositions;
	var colContentPositions;
	
	var tm;
	var colFormat;
	var showWeekNumbers;
	var weekNumberTitle;
	var weekNumberFormat;

	var checkInDate;
	var checkOutDate

	var data;
	var data_events;
	var roomsHash = {};
	var tatalOccupancyCount = [];
	var totalRoomCount;
	var totalDirtyRoomCount;
	
	/* Rendering
	------------------------------------------------------------*/

	disableTextSelection(element.addClass('bc-grid'));

	function renderBasic(_rowCnt, _colCnt, _datesArray) {

		rowCnt = _rowCnt;
		colCnt = _colCnt;
		datesArray = _datesArray;

		updateOptions();

		if (!body) {
			buildEventContainer();
		}

		buildTable();
		buildRooms();
	}

	function updateOptions(){
		// TODO
	}

	function buildTable(){
		content = buildDatesContainer();
		dA = datesArray;
		
		// Calender Dates
		table = $("<table>",{'class':"bc-calendar","width":"100%",'height':"100%",'cellspacing':0,'cellpadding':0,'border':0}).css({'border-top':'1px solid #AAAAAA'});
		table.appendTo(content);
		
		// if(viewName == 'gday') { 
		// 	// For Grid View 
		// 	widget = $("<div>",{'class':"widget-box"});
		// 	widget.appendTo(content);
			
		// 	return;
		// }
		

		// Total Occupancy
		($("<table />")
			.addClass('total-occupancy')
			// .css({width:'100%',height:'101%','border-top':'1px solid rgb(170, 170, 170);'})
			.append(
				$('<tbody />')
					.append(
						$("<tr/>")
							.append("<td align='center' style='padding-left: 5px; width: 13%;'>Total Occupancy Status</td>")
							.append(dateWisetotalOccupancy())
					)
			)
		).appendTo(content);


		var text_help_msg = (!calendar.options.dragAndDrop) ? "Your current subscription plan does not include booking through calendar view" : "Click and Drag to Reserve";

		// Help Text
		($("<div />")
			.addClass('bc-help-msg')
			.text(text_help_msg)
		).appendTo(content);

		tbody = $("<tbody />");
		tbody.appendTo(table);

		// IS dormitory
		var butClasses = (calendar.options.isDormitory)?"bc-button bc-state-default bc-corner-left bc-corner-right":"";

		($("<tr>").append(
							$("<td/>").css({'padding-left':'5px','width':'13%'})
								.append(
											$("<div />")
												.addClass('bc-room-type')
												.css({'height':'34px'})
												// .append('<span style="position: relative; top: 8px;cursor:default;">Room Types</span>')
												.append(
													$("<span/>").css({'position':'relative','top':'8px'}).addClass(butClasses)
														.text(defaultTypeText[defaultType])
														.attr({'data-toggle':'dormitory'})
														.click(function(){
															if(calendar.options.isDormitory){
																var parentDiv = $(this).parent();
																var childLastSpan = parentDiv.find('.dormitory-select');

																childLastSpan.show();
															}else{
																return false;
															}
														})
														.hover(
															function(){
																$(this)
																	.not('.bc-state-active')
																	.not('.bc-state-disabled')
																	.addClass('bc-state-hover');
															},
															function() {
																$(this)
																	.removeClass('bc-state-hover')
																	.removeClass('bc-state-down');
															}
														)
												)
												.append(
													$("<span/>").addClass('dormitory-select').attr({'id':'dormitory-select'})
														.append(
															$("<div/>").attr({'data-value':1}).text('Room Types')
																.click(function(){
																	showRoomTypes($(this).attr('data-value'),this);
																})
														)
														.append(
															$("<div/>").attr({'data-value':2}).text('Dormitory')
																.click(function(){
																	showRoomTypes($(this).attr('data-value'),this);
																})
														)
												)

												
												// TODO Will add Later
												// .append(buildSetUPMenuBar(this))
									)
						)
				  .append(buildCalendar())
		).appendTo(tbody);

		// Background Activity
		$("#goto-date").parent('td').css({'background':'#AAAAAA'});
		$("#current-date").parent('td').css({'background':'#AAAAAA'});

		// Current Date Activity
		$("#current-date").parent('td').addClass('bc-current-date');
	}

	function showRoomTypes(val,ele){
		defaultType = parseInt(val);
		$("#table-scroll").remove();
		buildRooms();

		// Hiding 
		var thisEle = $(ele);
		var mainParentEle = thisEle.parent().parent();

		thisEle.parent().hide();
		mainParentEle.find('span.bc-button').text(defaultTypeText[defaultType]);
	}

	var resp_data_room_type_details;
	var resp_data_calandar_data;
		
	function buildRooms(){

		var sources = {};

		var _div = $("<div/>").css({'max-height':'600px','overflow':'hidden','position':'relative'}).attr({'id':'table-scroll'})
		table2 = $("<table>",{'id':'grid-container-table','class':'bc-border','width':'100%'})
		table2.appendTo(_div);
		_div.appendTo(content);
		
		tbody2 = $("<tbody />");
		tbody2.appendTo(table2);

		// TODO -- This thing we need to implement

		// alert(calendar.source.url);

		// console.log(calendar.source.url);

		var from_date = t.start;
		var to_date = t.end;

		// dd-mm-yyyy
		var _fD = from_date.getDate()+'-'+(from_date.getMonth()+1)+'-'+from_date.getFullYear();
		var _eD = to_date.getDate()+'-'+(to_date.getMonth()+1)+'-'+to_date.getFullYear();

		// calendar.options.data_url.url += '?from_date='+_fD+'&to_date='+_eD;

		isDayView = (viewName==='day') ? true:false;

		sources.url = calendar.options.data_url.url+'?from_date='+_fD+'&to_date='+_eD+'&is_day_view='+isDayView;
		sources.method = calendar.options.data_url.method;

		$.ajax($.extend({}, ajaxDefaults, sources, {
			success: function(events) {
				t1 = new Date().getTime();
				// Empty the total room count
				totalRoomCount = 0;
				totalDirtyRoomCount = 0;
				// data = events;
				data = events.room_type_details;
				data_events = events.calandar_data;
				resp_data_room_type_details = data;
				resp_data_calandar_data = data_events;
				// buildRoomsHtml();
				
				buildBody();
				t1a = new Date().getTime();
				// console.log("buildBody t1a: " + (t1a - t1))
				renderEvents();
				t1b = new Date().getTime();				
				// console.log("renderEvents BasicView t1b: " + (t1b - t1a))
				
				// TODO 
				// Scrolling for calendar
				$("#table-scroll").slimscroll({
    				alwaysVisible: true,
  				})

				// Goto Room Functionality
				if(calendar.options.gotoRoomFlag){

					// Auto  complte method
					suggestions = [];
					sugDetails = [];
					$.each(data,function(roomTypeId,typeDetails){
						$.each(typeDetails.room_numbers, function(roomId,details){
							var _room_name = details.split(' ');
							suggestions.push(_room_name[0]);
							sugDetails.push({room_id:roomId,room_type_id:roomTypeId});
							calendar.options.autoRoomsData.push({room_id:roomId,room_type_id:roomTypeId});
							calendar.options.autoRooms.push(details);

							// $.extend( calendar.options.autoRoomsData, {details:{room_id:roomId,room_type_id:roomTypeId}} );
						})
					})


					autoInit();

					// $("#goto_room").autocomplete({
					// 	source: suggestions
					// });

					// $("#goto_room").autocomplete({
					// 	delay: 0,
					// 	source: _dat
					// });

					// Hide the Elements based on selection
					if(calendar.options.gotoRoomDetails.roomTypeId && calendar.options.gotoRoomDetails.roomNoId){
						showSelectedRooms();
					}
				}
				
				t2 = new Date().getTime();				
				// console.log("AJAX SUCCESS T2: " + (t2 - t1))
				
			},
			error: function() {
				// alert("API Error");
				console.log("API ERROR");
			},
			complete: function() {
			}
		}));

	}

	// Goto Room Functionality
	function showSelectedRooms(){
		var room_type_id = calendar.options.gotoRoomDetails.roomTypeId;
		var room_no_id = calendar.options.gotoRoomDetails.roomNoId;

		var all_trs = $("#grid-container-table").find('tr');

		var room_type_tr = $("#room-type-row-"+room_type_id);
		var room_tr = $("#room-row-"+room_no_id);

		all_trs.each(function(i,tr){
			if(room_type_tr.attr('id') === $(tr).attr('id') || room_tr.attr('id') === $(tr).attr('id')){

				if($(tr).hasClass('parent-row')){
					$(tr).find('span.bc-room-type-icon').hide();
					$(tr).find('div').off('click');
				}

				$(tr).show();
			}else{
				$(tr).hide();
			}
		})

	}

	function buildBody(){

		if ( defaultType == 1 ) { // If it is Room Types
			buildRoomTypesBody(); // Because dormitory is false
		}else{
			buildDormitoryBody(); // Because dormitory is false
		}
	}


	function buildRoomTypesBody(){
		$.each(data, function(i,roomType){
			if (!roomType.is_dormitory) {
				// Total Rooms
				totalRoomCount += roomType.total_room_count;
				// Total Dirty Room Count
				totalDirtyRoomCount += roomType.dirty;

				if(roomType.room_numbers){
					// Room Type
					roomTypeTr = $("<tr />").addClass('parent-row').attr({'data-class':'bc-row-active'})
									.attr({'id':'room-type-row-'+roomType.room_type_id})
									.attr({'data-is-dormitory':roomType.is_dormitory})
									.attr({'data-is-dorm-bulk-booking':roomType.is_dorm_bulk_booking})
									.append('<input type="hidden" id="room-type-'+roomType.room_type_id+'-total-rooms" value="'+roomType.total_room_count+'"/>')
					
					// Limited Room Type name
					var roomTypeName = (roomType.name.length > 16) ? roomType.name.substr(0,13)+'...':roomType.name;
					// var styleForLabel = 'position: relative; top: 4px; cursor: default; font-weight: bold;';
					var styleForLabel = {'position': 'relative', 'top': '4px', 'cursor': 'default', 'font-weight': 'bold','left':'0px'}
					// styleForLabel +=  ( (roomType.name.length > 19 ) ? 'float:left;left:3px' : 'left: 0px;' );
					
					if (viewName == "gday") {
						roomTypeTd = $("<td />")
								.css({'width':'100.0%'})
								.append(
									$("<div/>")
										.css({'width':'100%', 'text-align':'left', 'background-color':'#ccc', 'border':'solid 1px #ccc'})
										.html("<h5>" + roomType.name + "</h5>")
								)
						roomTypeTd.appendTo(roomTypeTr)
					} 
					else {
						roomTypeTr.append
						(
							$("<td />")
							.css({'width':'13.0%'})
							.append(
											$("<div />")
												.addClass('bc-room-type')
												.css({'height':'26px'})
												.append(function(){
													if(!calendar.options.isChannelManager){
														return $("<span />")
														.addClass('bc-room-type-icon').css({'float':'right','left':'-8px'})
														.append('<i class="icon-chevron-right arrow"></i>')
													}
												})
												.append(
													$("<span/>")
													.html(roomType.name)
													.css(styleForLabel)
													.text(function(index, text) {
												    	return text.substr(0, 14)+((text.length > 17) ? '...' : '');
													})
												)

												// .append('<span style="'+styleForLabel+'"> '+roomTypeName+' </span>')
												.click(function(){
													var parentRow = $(this).parent().parent();
													var childrenRows = getChildrenRows(parentRow);

													$.each(childrenRows, function() {
														$(this).toggle();
													})

													$(this).find('span:first').children().toggleClass('icon-chevron-down');
													$(this).find('span:first').children().toggleClass('icon-chevron-right');
												})
											)
						)
					}
					
					if (viewName != 'gday') {
						buildRoomDivs(roomTypeTr,roomType,1); 						
					}
					// Append To Table Body
					roomTypeTr.appendTo(tbody2);

					// Checking is channel manager or not
					if(!calendar.options.isChannelManager){
						if(roomType.room_numbers){ // Check the condition wether here room nos are der not?
							
							$.each(roomType.room_numbers, function(j,roomName){
								// TODO
								
								_statusColor = (setRoomColor(roomName)&&dA.time)?'':'';

								if (viewName == 'gday') {
									// gTr.append('<td width="12.3%" class="'+_statusColor+'">'+roomName+'</td>');
									var roomDetails = {};
									roomDetails.id = j;
									roomDetails.name = roomName;
									buildRoomDivs(roomTypeTd,roomType,0,roomDetails,data);
									// tr.appendTo(tbody2);
								} else {
									tr = $("<tr />")
											.addClass('child-row')
											.attr({'id':'room-row-'+j})
											.attr({'data-room-id':j})
											.css({'display':'none'});
									tr.append('<td width="12.3%" class="'+_statusColor+'">'+roomName+'</td>');
									
									var roomDetails = {};
									roomDetails.id = j;
									roomDetails.name = roomName;
									buildRoomDivs(tr,roomType,0,roomDetails,data);
									tr.appendTo(tbody2);
								}
							});
							
							if (viewName == 'gday') {
								roomTypeTr.appendTo(tbody2);
							}
						}
						// Draging
						if(calendar.options.dragAndDrop) //  Enables Click & Drag Functionality
						tbody2
						.drag("start",function( ev, dd ){
							// for left click on rooms need to hide right click popup
	            $('[data-toggle="context-menu"]').hide();
	            $('[data-toggle="context-menu"]').addClass('hidden');
							return $('<div class="selection" />')
							.css('opacity', .65 )
							.appendTo( document.body );
						})
						.drag(function( ev, dd ){
							$( dd.proxy ).css({
								top: Math.min( ev.pageY, dd.startY ),
								left: Math.min( ev.pageX, dd.startX ),
								height: Math.abs( ev.pageY - dd.startY ),
								width: Math.abs( ev.pageX - dd.startX )
							});
						})
						.drag("end",function( ev, dd ){
							$( dd.proxy ).remove();
						})
					}
				}
			} // Is Dorm checking
		}); // end
	}

	function buildDormitoryBody(){
		$.each(data, function(i,roomType){
			
			if (roomType.is_dormitory) {
				
				// Total Rooms
				totalRoomCount += roomType.total_room_count;
				// Total Dirty Room Count
				totalDirtyRoomCount += roomType.dirty;

				if(roomType.room_numbers){
					// Room Type
					roomTypeTr = $("<tr />").addClass('parent-row').attr({'data-class':'bc-row-active'})
									.attr({'id':'room-type-row-'+roomType.room_type_id})
									.attr({'data-is-dormitory':roomType.is_dormitory})
									.attr({'data-is-dorm-bulk-booking':roomType.is_dorm_bulk_booking})
									.append('<input type="hidden" id="room-type-'+roomType.room_type_id+'-total-rooms" value="'+roomType.total_room_count+'"/>')
					
					// Limited Room Type name
					var roomTypeName = (roomType.name.length > 16) ? roomType.name.substr(0,13)+'...':roomType.name;
					// var styleForLabel = 'position: relative; top: 4px; cursor: default; font-weight: bold;';
					var styleForLabel = {'position': 'relative', 'top': '4px', 'cursor': 'default', 'font-weight': 'bold','left':'0px'}
					// styleForLabel +=  ( (roomType.name.length > 19 ) ? 'float:left;left:3px' : 'left: 0px;' );
					
					roomTypeTr.append(
						$("<td />")
							.css({'width':'13.0%'})
							.append(
								$("<div />")
									.addClass('bc-room-type')
									.css({'height':'26px'})
									.append(function(){
										if(!roomType.is_dorm_bulk_booking)
											if(!calendar.options.isChannelManager){
												return $("<span />")
														.addClass('bc-room-type-icon').css({'float':'right','left':'-8px'})
														.append('<i class="icon-chevron-right arrow"></i>')
											}
										})
									.append(
										$("<span/>")
											.html(roomType.name)
											.css(styleForLabel)
											.text(function(index, text) {
										    return text.substr(0, 14)+((text.length > 17) ? '...' : '');
											})
									)
									.click(function(){
										if(roomType.is_dorm_bulk_booking){
											return false;
										}
										var parentRow = $(this).parent().parent();
										var childrenRows = getChildrenRows(parentRow);

										$.each(childrenRows, function() {
											$(this).toggle();
										})

										$(this).find('span:first').children().toggleClass('icon-chevron-down');
										$(this).find('span:first').children().toggleClass('icon-chevron-right');
									})
							)
					)
					
					if(roomType.is_dorm_bulk_booking){
						buildRoomTypeDivsForBulk(roomTypeTr,roomType,data);
					}else{
						buildRoomDivs(roomTypeTr,roomType,1);
					}

					// Append To Table Body
					roomTypeTr.appendTo(tbody2);

					// Checking is channel manager or not
					if(!roomType.is_dorm_bulk_booking) // Check is bulk dorm or what
						if(!calendar.options.isChannelManager){
							if(roomType.room_numbers){ // Check the condition wether here room nos are der not?
								$.each(roomType.room_numbers, function(j,roomName){
									// TODO
									
									_statusColor = (setRoomColor(roomName)&&dA.time)?'':'';


									tr = $("<tr />")
											.addClass('child-row')
											.attr({'id':'room-row-'+j})
											.attr({'data-room-id':j})
											.css({'display':'none'});
									tr.append('<td width="12.3%" class="'+_statusColor+'">'+roomName+'</td>');

									var roomDetails = {};
									roomDetails.id = j;
									roomDetails.name = roomName;
									buildRoomDivs(tr,roomType,0,roomDetails,data);
									tr.appendTo(tbody2);
								});
							}
						} // channel manager end

					// Draging And Drop
					// if(roomType.is_dorm_bulk_booking) // Check is bulk dorm or what
						if(!calendar.options.isChannelManager){
							// Draging
							if(calendar.options.dragAndDrop) //  Enables Click & Drag Functionality
								tbody2
									.drag("start",function( ev, dd ){
										$('[data-toggle="context-menu"]').hide();
              	                        $('[data-toggle="context-menu"]').addClass('hidden');
										return $('<div class="selection" />')
											.css('opacity', .65 )
											.appendTo( document.body );
									})
									.drag(function( ev, dd ){
										$( dd.proxy ).css({
											top: Math.min( ev.pageY, dd.startY ),
											left: Math.min( ev.pageX, dd.startX ),
											height: Math.abs( ev.pageY - dd.startY ),
											width: Math.abs( ev.pageX - dd.startX )
										});
									})
									.drag("end",function( ev, dd ){
										$( dd.proxy ).remove();
									})
						}
				}
			} // Is Dorm checking
		}); // end
	}


	function buildMenuBar(el){
		return $("<ul>").addClass('bc-room-type-sidebar bc-shadow-black')
						.append($("<li/>")
							.append(
									$("<a/>")
										.attr({'href':calendar.options.edit_room_type_url.url})
										.attr({'data-remote':true})
										.append('<div><i class="icon-edit"></i></div><span class="sidebar-text">Edit</span>')
								)
							);
						// .append( '<li><a href="#modalPopup" data-toggle="modal" title="not found"><div><i class="icon-picture"></i></div><span class="sidebar-text">Images</span></a></li><li><a href="#"><div><i class="icon-youtube-play"></i></div><span class="sidebar-text">Videos</span></a></li>');
	}

	function buildSetUPMenuBar(el){

		var _uL = $("<ul>")
				.addClass('bc-room-type-sidebar bc-shadow-black')
				.append($("<li/>")
							.append(
									$("<a/>")
										.attr({'href':calendar.options.create_block_url.url})
										.attr({'data-remote':true})
										.append('<div><i class="icon-plus-sign-alt"></i></div><span class="sidebar-text">Create New</span>')
								)
					)
		return _uL;
	}

	function buildRoomDivs(tr,roomType,isRoomType,roomDetails,details){
		if(dA.months) {
			$.each(dA.months, function(i,v){
				(
					$("<td />")
					.attr({'data-date':v.month})
					// .text(data.monthly_show_availability[i])
				).appendTo(tr);
			});			
		}else if(dA.time){

			if(isRoomType){
				_buildRoomTypeDivsForDay(tr,roomType);
			}else{
				_buildRoomDivsForDay(tr,roomType,roomDetails,details);
			}
			// setInterval(setTimeLine(element), 300000);
		}else{
			if(isRoomType){
				// _displayRoomTypeDivs(tr,roomType,roomName,dateWiseData,totalRooms);
				_buildRoomTypeDivs(tr,roomType);
			}else{

				if (viewName == "gday") {
					_buildRoomDivsForDayGrid(tr,roomType,roomDetails,details);
				} else {
					_buildRoomDivs(tr,roomType,roomDetails,details);
				}
			}
		}
	}
		
	function _buildRoomTypeDivsForDay(tr,roomType){
		// console.log("_buildRoomTypeDivsForDay");
		$.each(dA.time, function(i,v){
			var _pastTime = '';
			var _innerDiv = '';
			var _sDate = dA.dates.date.split('/');

			_id = 'room-type-time-scale-'+roomType.room_type_id+'-'+_sDate[2]+'-'+appendZero(_sDate[0])+'-'+appendZero(_sDate[1])+'-'+appendZero(v.hour);

			// Past Time
				if(isCurrentDate(new Date(dA.dates.date))){
					var _cDate = new Date();

					if(_cDate.getHours() > v.hour){
						_pastTime = 'diagonal_lines_pattern'

						if(_cDate >= calendar.options.business_date){
							_pastTime = ''
							_innerDiv = $("<div/>").css({
								'height': '100%',
								'width': '100%',
								'background-color': 'rgba(0, 0, 0, 0.0470588)'
							})
						}

					}else if(_cDate.getHours() === v.hour){

						var min = _cDate.getMinutes();

						var _width = (_cDate.getMinutes()/60)*100;

						_innerDiv += '<div style="width:'+parseInt(_width)+'%;height:100%;opacity:0.7;" class="diagonal_lines_pattern"></div>';
					}else{
						_pastTime = '';
					}

				}else if((new Date(dA.dates.date) < new Date && calendar.options.business_date <= new Date(dA.dates.date) || isEqualDates(calendar.options.business_date, new Date(dA.dates.date)) )){
						_pastTime = '';
					_innerDiv = $("<div/>").css({
						'height': '100%',
						'width': '100%',
						'background-color': 'rgba(0, 0, 0, 0.0470588)'
					})
				}else if(dA.dates.isCurrent == 'past'){
					_pastTime = 'diagonal_lines_pattern'
				}else{
					_pastTime = '';
				}

				(
					$("<td />")
					.attr({'id':_id})
					.attr({'data-date':dA.dates.date})
					.attr({'data-hour':v.hour})
					.addClass(_pastTime)
					.append(_innerDiv)
				).appendTo(tr);
		});
	}

	function _buildRoomDivsForDay(tr,roomType,roomDetails,details){
		// console.log("_buildRoomDivsForDay");
		$.each(dA.time, function(i,v){

				var _pastTime = '';
				var _innerDiv = '';
				var _sDate = dA.dates.date.split('/');

					_id = 'room-time-scale-'+roomDetails.id+'-'+_sDate[2]+'-'+appendZero(_sDate[0])+'-'+appendZero(_sDate[1])+'-'+appendZero(v.hour);
					_room_type_class = 'hs-active';

				// Past Time
				if(isCurrentDate(new Date(dA.dates.date))){
					var _cDate = new Date();

					if(_cDate.getHours() > v.hour){
						_pastTime = 'diagonal_lines_pattern'
						rcClass = ''

						if(_cDate >= calendar.options.business_date){
							_pastTime = '';
							_innerDiv = $('<div/>').css({
								'background-color':'rgba(0, 0, 0, 0.23)',
								'height': '100%',
								'border': 'medium none',
								'position': 'relative',
								'width':'100%'

							})
							.click(function(e){
								nightAuditMessage($(this),e);
								return false;
							})
						}

						withOutDragFun(v,tr,roomType,roomDetails,details,_id,_room_type_class,_innerDiv,_pastTime,rcClass);
					}else if(_cDate.getHours() === v.hour){

						var min = _cDate.getMinutes();

						var _width = (_cDate.getMinutes()/60)*100;

						_innerDiv += '<div style="width:'+parseInt(_width)+'%;height:100%;opacity:0.7;" class="diagonal_lines_pattern"></div>';
						rcClass =  'rc-available-current'
						withDragFun(v,tr,roomType,roomDetails,details,_id,_room_type_class,_innerDiv,_pastTime, rcClass);
					}else{
						_pastTime = '';
						rcClass = 'rc-available';
						withDragFun(v,tr,roomType,roomDetails,details,_id,_room_type_class,_innerDiv,_pastTime,rcClass);
					}

				}else if((new Date(dA.dates.date) < new Date && calendar.options.business_date <= new Date(dA.dates.date) || isEqualDates(calendar.options.business_date, new Date(dA.dates.date)) )){
						_pastTime = '';

						_innerDiv = $('<div/>').css({
								'background-color':'rgba(0, 0, 0, 0.23)',
								'height': '100%',
								'border': 'medium none',
								'position': 'relative',
								'width':'100%'

							})
							.click(function(e){
								nightAuditMessage($(this),e);
								return false;
							})

						withOutDragFun(v,tr,roomType,roomDetails,details,_id,_room_type_class,_innerDiv,_pastTime,rcClass);
				}else if(dA.dates.isCurrent == 'past'){
					_pastTime = 'diagonal_lines_pattern';
					rcClass =  ''
					withOutDragFun(v,tr,roomType,roomDetails,details,_id,_room_type_class,_innerDiv,_pastTime,rcClass);
				}else{
					_pastTime = '';
					rcClass = 'rc-available';
					withDragFun(v,tr,roomType,roomDetails,details,_id,_room_type_class,_innerDiv,_pastTime,rcClass);
				}

			});
	}

	function _buildRoomDivsForDayGrid(tr,roomType,roomDetails,details){
		var $currentlyDragged;
		var $innerDiv;
		var $startWidth;
		var	$allEle = [];


		$.each(dA.dates, function(i,v){
			_weekendColor = (v.isWeekend)? 'weekend' : '';
			_weekendColor_style = (v.isWeekend)? {'background':calendar.options.legendColors.weekend}:{};
			var rcC = ''

			if(v.isCurrent == 'future' || v.isCurrent == 'current'){
				_pastColor = '';
				rcClass =  (v.isCurrent == 'current') ? 'rc-available-current rc-b-class' : 'rc-available rc-b-class';
				rcC = (v.isCurrent == 'current') ? 'availableCurrent' : 'available';
			}else if(v.isCurrent == 'business-date'){
				_pastColor = 'businessDateClass';
				rcClass = '';
				rcC = '';
				if( isCurrentDate(calendar.options.business_date) ){
					rcClass = 'rc-available-current rc-b-class';
					rcC = 'availableCurrent ';
				}
			}else{
				_pastColor = 'diagonal_lines_pattern';
				_weekendColor = '';
				rcClass = '';
			}

			_date = new Date(v.date);
			_cD = new Date();

			var menuItems = getMenuItems(v,false,roomType.is_dorm_bulk_booking,roomType);


			var datesRange = {};

			// Rooms

			// TODO -- Change the logic
			var sDate = (v.date).split('/');

			var cellTag = "<td />"

			var gridHtmlStr = ""
			var gridPanelClass = ""
			if (viewName == 'gday') {
				cellTag = "<div />"
				gridHtmlStr = roomDetails.name
				gridPanelClass = "hs-grid-box"
			}
				
			var hb_x_path_id = "";
			hb_x_path_tmp = resp_data_calandar_data[sDate[2]+'-'+appendZero(sDate[0])+'-'+appendZero(sDate[1])]
			if (hb_x_path_tmp && hb_x_path_tmp[roomType.room_type_id] && hb_x_path_tmp[roomType.room_type_id].room_numbers && 
						hb_x_path_tmp[roomType.room_type_id].room_numbers[roomDetails.id] && 
						hb_x_path_tmp[roomType.room_type_id].room_numbers[roomDetails.id][0] &&
						hb_x_path_tmp[roomType.room_type_id].room_numbers[roomDetails.id][0].hotel_booking_information) {
				hb_x_path_id = hb_x_path_tmp[roomType.room_type_id].room_numbers[roomDetails.id][0].hotel_booking_information.hb_x_path_id
			}
			var style = (v.isWeekend)? calendar.options.legendColors.weekend : calendar.options.legendColors.available
				
			if (_date > _cD || isCurrentDate(_date)) {
				(
					$(cellTag,{'class':_weekendColor+' hs-active '+_pastColor+' '+rcClass})
					.css({'background':style})
					.addClass(gridPanelClass)
					.attr({'id':'room-'+roomDetails.id+'-'+sDate[2]+'-'+appendZero(sDate[0])+'-'+appendZero(sDate[1])})
					.attr({'data-date':v.date})												
					.addClass('drop room-td-'+roomDetails.id)
					.attr({'data-color':style})
					
					// ADDED
					.attr({'data-x-room-type-id':roomType.room_type_id})
					.attr({'data-x-room-id':roomDetails.id})
					.attr({'data-room-id':roomDetails.id})
					.attr({'data-hb-x-path-id':hb_x_path_id})
	
					// REMOVED
					.attr({'data-room-type':roomType.name})			
					.attr({'data-right-clicks':JSON.stringify(menuItems)})
					.on('contextmenu', function(e){
						e.preventDefault();
						// Your code.
						$(this).popover('hide');

						rightClickInit($(this),e);			  
					})// .contextMenu(menuItems,{theme:'vista'})
					.html(function(){
						statusColor = setRoomColor(roomDetails.name);
						var menuItemsDRT = getMenuItems(v,true,roomType.is_dorm_bulk_booking,roomType)
							
						if(isCurrentDate(_date)){
							if(statusColor){
								return $("<div/>")
								.css({'width':'100%','height':'100%','background':calendar.options.legendColors.dirty})
								.html(gridHtmlStr)
								.addClass('hs-booking-details hs-dirty-room rc-dirty')
								.attr({'data-right-clicks':JSON.stringify(menuItemsDRT)})
								.on('contextmenu', function(e){
								  e.preventDefault();
								  // Your code.
								  $(this).popover('hide');

								  rightClickInit($(this),e);
								})// .contextMenu(menuItemsDRT,{theme:'vista'})
								}else{
									return $("<div/>").html(gridHtmlStr)
								}
							} else {
								return $("<div/>").html(gridHtmlStr)
							}
						})
						.click(function(){
							if(calendar.options.dragAndDrop){ // Enables Click & Drag Functionality
								$('[data-toggle="context-menu"]').hide();
                            	$('[data-toggle="context-menu"]').addClass('hidden');
								$( this ).toggleClass("hs-active");
								
								if ($(this).attr('style'))
							        $(this).removeAttr('style');
							  else if ($(this).attr('style') === undefined) $(this).attr('style', {'background':style});
							  else $(this).attr('style', {'background':style});
								
								$( this ).toggleClass("dropped");
								datesRange.fromDate = $(this).attr('data-date');
								datesRange.toDate = $(this).attr('data-date');
								calendar.options.getInfo($(this).attr('data-date'),roomDetails,$(this).attr('data-room-type'),details,$(this),datesRange,0,roomType.room_type_id,defaultType);
								// calendar.options.getInfo($(this).attr('data-date'),$(this).attr('data-room-type'),$(this),datesRange,0);
							}else{
								$(".bc-help-msg").effect("highlight", { color: "#ff0000" }, 1000);
								return false;
							}
						})
						.drop("start",function(){
							$( this ).toggleClass("hs-active");
						  $(this).attr('style', {'background':style});
							datesRange.fromDate = $(this).attr('data-date');
						})
						.drop(function( ev, dd ){
							$( this ).toggleClass("dropped");
							$( this ).toggleClass('hs-active');
						  $(this).attr('style', {'background':style});

							calendar.options.getInfo($(this).attr('data-date'),roomDetails,$(this).attr('data-room-type'),details,$(this),datesRange,1,roomType.room_type_id,defaultType);
						})
						.drop("end",function(){
							datesRange.toDate = $(this).attr('data-date');
							$( this ).toggleClass("hs-active");
				    	$(this).attr('style', {'background':style});
						})

				).appendTo(tr);
					
				// Right Options
				// callContextMenuPlugIn(rId,rightClickOpts);
			}else if((_date < _cD && calendar.options.business_date <= _date) || isEqualDates(calendar.options.business_date,_date)){

				(
					$(cellTag,{'class':_weekendColor})
					.css({'background':style})
					.addClass(gridPanelClass)
					.attr({'id':'room-'+roomDetails.id+'-'+sDate[2]+'-'+appendZero(sDate[0])+'-'+appendZero(sDate[1])})
					.attr({'data-date':v.date})
					// ADDED
					.attr({'data-x-room-type-id':roomType.room_type_id})
					.attr({'data-x-room-id':roomDetails.id})
					.attr({'data-room-id':roomDetails.id})
					.attr({'data-hb-x-path-id':hb_x_path_id})
		
					// REMOVED
					.attr({'data-room-type':roomType.name})			
					.append(
						$('<div/>')
							.css({
								'background-color':'rgba(0, 0, 0, 0.23)',
								'height': '100%',
								'border': 'medium none',
								'position': 'relative',
								'width':'100%'
							})
							.html(gridHtmlStr)
							.click(function(e){
								nightAuditMessage($(this),e);
								return false;
							})
					)
				).appendTo(tr);
			}else{
				(
					$(cellTag,{'class':_weekendColor+' '+_pastColor})
					.addClass(gridPanelClass)
					.attr({'id':'room-'+roomDetails.id+'-'+sDate[2]+'-'+appendZero(sDate[0])+'-'+appendZero(sDate[1])})
					.attr({'data-date':v.date})
					// ADDED
					.attr({'data-x-room-type-id':roomType.room_type_id})
					.attr({'data-x-room-id':roomDetails.id})
					.attr({'data-room-id':roomDetails.id})
					.attr({'data-hb-x-path-id':hb_x_path_id})
		
					// REMOVED
					.attr({'data-room-type':roomType.name})			
					
					.append(
						$('<div/>')
							.html(gridHtmlStr)
					)
				).appendTo(tr);
			}
		});	
		// applyRightClickOptions();
	}
	
	function withDragFun(v,tr,roomType,roomDetails,details,_id,_room_type_class,_innerDiv,_pastTime,rcClass){

		var statusColor = setRoomColor(roomDetails.name);
		var menuItems;
		var datesRange = {};
		if(isCurrentDate(new Date(dA.dates.date)) && statusColor){
			// var style = {'background':calendar.options.legendColors.dirty};
			var style = calendar.options.legendColors.dirty;
			rcClass = 'rc-dirty'; // update the status for current day
			menuItems = getItemsForDay(true,isCurrentDate(new Date(dA.dates.date)),roomType);
		}else{

			if(isCurrentDate(new Date(dA.dates.date))) rcClass = 'rc-available-current';

			var style = calendar.options.legendColors.available;
			menuItems = getItemsForDay(false,isCurrentDate(new Date(dA.dates.date)),roomType);
		}
		(
					$("<td />")
					.attr({'id':_id})
					.attr({'data-date':dA.dates.date})
					.attr({'data-hour':v.hour})
					.addClass(_pastTime+' '+rcClass)
					.addClass(_room_type_class)
					.append(_innerDiv)
					.addClass('drop room-td-'+roomDetails.id)
					.css({'background':style}) // changed TODO
					// .attr({'data-color':JSON.stringify(style)})
					.attr({'data-color':style})
					
					// ADDED
					.attr({'data-x-room-type-id':roomType.room_type_id})
					.attr({'data-x-room-id':roomDetails.id})	
			
					// REMOVED
					.attr({'data-room-type':roomType.name})
					// .attr({'data-rc-room-details':JSON.stringify(roomDetails)})
					// .attr({'data-rc-b-details':JSON.stringify(details)})
					// .attr({'data-rc-room-type':JSON.stringify(roomType)})
			
					.attr({'data-right-clicks':JSON.stringify(menuItems)})
					// .contextMenu(menuItems,{theme:'vista'})
					.on('contextmenu', function(e){
						e.preventDefault();
						// Your code.
						$(this).popover('hide');

						rightClickInit($(this),e);			  
					})
						.click(function(){
							if(calendar.options.dragAndDrop){ // Enables Click & Drag Functionality
								$( this ).toggleClass("hs-active");
								$( this ).toggleClass("dropped");

								if ($(this).attr('style'))
							        $(this).removeAttr('style');
							    else if ($(this).attr('style') === undefined) $(this).attr('style', style);
							    else $(this).attr('style', style);

								datesRange.fromDate = $(this).attr('data-date');
								datesRange.toDate = $(this).attr('data-date');
								calendar.options.getInfo($(this).attr('data-date'),roomDetails,$(this).attr('data-room-type'),details,$(this),datesRange,0,roomType.room_type_id,defaultType,$(this).attr('data-hour'));
								// calendar.options.getInfo($(this).attr('data-date'),$(this).attr('data-room-type'),$(this),datesRange,0);
							}else{
								$(".bc-help-msg").effect("highlight", { color: "#ff0000" }, 1000);
								return false;
							}
						})
						.drop("start",function(){
							$( this ).toggleClass("hs-active");
							$( this ).css({'background':style})
							if ($(this).attr('style'))
						        $(this).removeAttr('style');
						    else if ($(this).attr('style') === undefined) $(this).attr('style', style);
						    else $(this).attr('style', style);

							datesRange.fromDate = $(this).attr('data-date');
						})
						.drop(function( ev, dd ){
							$( this ).toggleClass("dropped");
							$( this ).toggleClass('hs-active');

							if ($(this).attr('style'))
						        $(this).removeAttr('style');
						    else if ($(this).attr('style') === undefined) $(this).attr('style', style);
						    else $(this).attr('style', style);

							// datesRange.toDate = $(this).attr('data-date');
							calendar.options.getInfo($(this).attr('data-date'),roomDetails,$(this).attr('data-room-type'),details,$(this),datesRange,1,roomType.room_type_id,defaultType,$(this).attr('data-hour'));
							// calendar.options.getInfo($(this).attr('data-date'),$(this).attr('data-room-type'),$(this),datesRange,1);
						})
						.drop("end",function(){
							datesRange.toDate = $(this).attr('data-date');
							$( this ).toggleClass("hs-active");

							if ($(this).attr('style'))
						        $(this).removeAttr('style');
						    else if ($(this).attr('style') === undefined) $(this).attr('style', style);
						    else $(this).attr('style', style);
						})

				).appendTo(tr);
	}

	function withOutDragFun(v,tr,roomType,roomDetails,details,_id,_room_type_class,_innerDiv,_pastTime,rcClass){
		(
					$("<td />")
					.attr({'id':_id})
					.attr({'data-date':dA.dates.date})
					.attr({'data-hour':v.hour})
					.addClass(_pastTime+' '+rcClass)
					.addClass(_room_type_class)
					.append(_innerDiv)
					.attr({'data-room-type':roomType.name})
		).appendTo(tr);
	}

	function _buildRoomDivs(tr,roomType,roomDetails,details){
		// console.log("_buildRoomDivs");
		var $currentlyDragged;
			var $innerDiv;
			var $startWidth;
			var	$allEle = [];

			// var _roomDetails = JSON.stringify(roomDetails);

			$.each(dA.dates, function(i,v){
				_weekendColor = (v.isWeekend)? 'weekend' : '';
				_weekendColor_style = (v.isWeekend)? {'background':calendar.options.legendColors.weekend}:{};
				var rcC = ''

				if(v.isCurrent == 'future' || v.isCurrent == 'current'){
					_pastColor = '';
					rcClass =  (v.isCurrent == 'current') ? 'rc-available-current rc-b-class' : 'rc-available rc-b-class';
					rcC = (v.isCurrent == 'current') ? 'availableCurrent' : 'available';
				}else if(v.isCurrent == 'business-date'){
					_pastColor = 'businessDateClass';
					rcClass = '';
					rcC = '';
					if( isCurrentDate(calendar.options.business_date) ){
						rcClass = 'rc-available-current rc-b-class';
						rcC = 'availableCurrent ';
					}
				}else{
					_pastColor = 'diagonal_lines_pattern';
					_weekendColor = '';
					rcClass = '';
				}

				_date = new Date(v.date);
				_cD = new Date();

				var menuItems = getMenuItems(v,false,roomType.is_dorm_bulk_booking,roomType);


				var datesRange = {};

				// Rooms

				// TODO -- Change the logic
				var sDate = (v.date).split('/');

				// var style = {'background':calendar.options.legendColors.available};
				var style = (v.isWeekend)? calendar.options.legendColors.weekend : calendar.options.legendColors.available
				
				if (_date > _cD || isCurrentDate(_date)) {

					(
						
						$("<td />",{'class':_weekendColor+' hs-active '+_pastColor+' '+rcClass})
						.css({'background':style})
						// .css(_weekendColor_style) // Week End Colors
						// .css({'background-color':'#CEB'})
						.attr({'id':'room-'+roomDetails.id+'-'+sDate[2]+'-'+appendZero(sDate[0])+'-'+appendZero(sDate[1])})
						.attr({'data-date':v.date})
						// .attr({'data-room-no':roomName})
						// .attr({'data-room-details':_roomDetails})
						.addClass('drop room-td-'+roomDetails.id)
						// .attr({'data-color':JSON.stringify(style)})
						.attr({'data-color':style})
						
						// ADDED
						.attr({'data-x-room-type-id':roomType.room_type_id})
						.attr({'data-x-room-id':roomDetails.id})
						
						// REMOVED
						.attr({'data-room-type':roomType.name})
						// .attr({'data-rc-room-type':JSON.stringify(roomType)})
						// .attr({'data-rc-room-details':JSON.stringify(roomDetails)})
						// .attr({'data-rc-b-details':JSON.stringify(details)})
						.attr({'data-right-clicks':JSON.stringify(menuItems)})
						.on('contextmenu', function(e){
							e.preventDefault();
							// Your code.
							$(this).popover('hide');

							rightClickInit($(this),e);			  
						})
						// .contextMenu(menuItems,{theme:'vista'})

						.html(function(){

							statusColor = setRoomColor(roomDetails.name);
							var menuItemsDRT = getMenuItems(v,true,roomType.is_dorm_bulk_booking,roomType)
							if(isCurrentDate(_date)){
								if(statusColor){
									return $("<div/>").css({'width':'100%','height':'100%','background':calendar.options.legendColors.dirty})//.html('<span style="position: relative; font-size: 12px; top: 16%;cursor: default;">DRT<span>')
												.addClass('hs-booking-details hs-dirty-room rc-dirty')
												.attr({'data-right-clicks':JSON.stringify(menuItemsDRT)})
												.on('contextmenu', function(e){
												  e.preventDefault();
												  // Your code.
												  $(this).popover('hide');

												  rightClickInit($(this),e);			  
												})
												// .contextMenu(menuItemsDRT,{theme:'vista'})
								}else{
									return null
								}
							}
						})

					
						.click(function(){
							if(calendar.options.dragAndDrop){ // Enables Click & Drag Functionality
								$('[data-toggle="context-menu"]').hide();
                            	$('[data-toggle="context-menu"]').addClass('hidden');
								$( this ).toggleClass("hs-active");
								
								if ($(this).attr('style'))
							        $(this).removeAttr('style');
							    else if ($(this).attr('style') === undefined) $(this).attr('style', {'background':style});
							    else $(this).attr('style', {'background':style});
								
								$( this ).toggleClass("dropped");
								datesRange.fromDate = $(this).attr('data-date');
								datesRange.toDate = $(this).attr('data-date');
								calendar.options.getInfo($(this).attr('data-date'),roomDetails,$(this).attr('data-room-type'),details,$(this),datesRange,0,roomType.room_type_id,defaultType);
							// calendar.options.getInfo($(this).attr('data-date'),$(this).attr('data-room-type'),$(this),datesRange,0);
							}else{
								$(".bc-help-msg").effect("highlight", { color: "#ff0000" }, 1000);
								return false;
							}
						})
						.drop("start",function(){
							$( this ).toggleClass("hs-active");

							// if ($(this).attr('style'))
						 //        $(this).removeAttr('style');
						 //    else if ($(this).attr('style') === undefined) $(this).attr('style', {'background':style});
						    // else 
						    	$(this).attr('style', {'background':style});

							datesRange.fromDate = $(this).attr('data-date');
						})
						.drop(function( ev, dd ){
							$( this ).toggleClass("dropped");
							$( this ).toggleClass('hs-active');

							// if ($(this).attr('style'))
						 //        $(this).removeAttr('style');
						 //    else if ($(this).attr('style') === undefined) $(this).attr('style', {'background':style});
						    // else 
						    	$(this).attr('style', {'background':style});

							// datesRange.toDate = $(this).attr('data-date');
							calendar.options.getInfo($(this).attr('data-date'),roomDetails,$(this).attr('data-room-type'),details,$(this),datesRange,1,roomType.room_type_id,defaultType);
							// calendar.options.getInfo($(this).attr('data-date'),$(this).attr('data-room-type'),$(this),datesRange,1);
						})
						.drop("end",function(){
							datesRange.toDate = $(this).attr('data-date');
							$( this ).toggleClass("hs-active");

							// if ($(this).attr('style'))
						 //        $(this).removeAttr('style');
						 //    else if ($(this).attr('style') === undefined) $(this).attr('style', {'background':style});
						    // else 
						    	$(this).attr('style', {'background':style});
						})

					).appendTo(tr);
					
					// Right Options
					// var rId = '#room-'+roomDetails.id+'-'+sDate[2]+'-'+appendZero(sDate[0])+'-'+appendZero(sDate[1])

					// callContextMenuPlugIn(rId,rightClickOpts);
				}else if((_date < _cD && calendar.options.business_date <= _date) || isEqualDates(calendar.options.business_date,_date)){
					($("<td />",{'class':_weekendColor})
					.css({'background':style})
					.attr({'id':'room-'+roomDetails.id+'-'+sDate[2]+'-'+appendZero(sDate[0])+'-'+appendZero(sDate[1])})
					.attr({'data-date':v.date})
					// .css({
					// 	'background-color':'rgba(0, 0, 0, 0.05)'
					// })
					.append(
						$('<div/>')
							.css({
								'background-color':'rgba(0, 0, 0, 0.23)',
								'height': '100%',
								'border': 'medium none',
								'position': 'relative',
								'width':'100%'

							})
							.click(function(e){
								nightAuditMessage($(this),e);
								return false;
							})
					)
					
					).appendTo(tr);
				}else{

					($("<td />",{'class':_weekendColor+' '+_pastColor})
					.attr({'id':'room-'+roomDetails.id+'-'+sDate[2]+'-'+appendZero(sDate[0])+'-'+appendZero(sDate[1])})
					.attr({'data-date':v.date})
					.append()
					).appendTo(tr);

					
				}
			});	
		// applyRightClickOptions();
	}

	function applyRightClickOptions(){
		// // AvailableRightOptions
		// var availableitems = rightClickOptionsStatusWise['available'];
		// callContextMenuPlugIn('.rc-available',availableitems);

		// //AvailableCurrnetRightOptions
		// var currentAvalItems = rightClickOptionsStatusWise['availableCurrent'];
		// callContextMenuPlugIn('.rc-available-current',currentAvalItems);		
		
		// Dirty
		var dirtyItems = rightClickOptionsStatusWise['dirty'];
		callContextMenuPlugIn('.rc-dirty',dirtyItems);

		// rc-m-rc-m-booked
		// var checkIN = rightClickOptionsStatusWise['checkIn'];
		// callContextMenuPlugIn('.rc-m-reserve',checkIN);

		
	} 

	function applyRightClickOptionsAfter(){
		var bookedMul = $('.rc-b-class');
		$.each(bookedMul, function(i,value){
			// var b_details = JSON.parse($(this).attr('data-b-details'));
			var id = $(this).attr('id');
			var checkInDate = new Date($(this).attr('data-booking-checkin'));

			var options = $(value).data('right-options');
			// var opts = getRightOptions(b_details,checkInDate);
			callContextMenuPlugIn('#'+id,options);
		});
	}

	function callContextMenuPlugIn(selector,optionItems){
		// $.contextMenu({
		// 	selector: selector,
  //       	callback: function(key, options) {
  //       		callBackFunctions($(this),key,options);
  //       	},
  //       	items: optionItems
		// });
	}

	function callBackFunctions(ele,key,options){
		// var m = "clicked: " + key;
        // window.console && console.log(m) || alert(m);
		var details = null; // jQuery.parseJSON(ele.attr('data-b-details'));
		if (ele.attr('data-hb-x-path-id')) {
			id_split = ele.attr('data-hb-x-path-id').split(':');
			room_type = id_split[0];
			room_num = id_split[1];
			date = id_split[2];
			details = resp_data_calandar_data[date][room_type].room_numbers[room_num][0];
		} else if (viewName == 'gday') {
			if (ele.parent('div').attr('data-hb-x-path-id')) {
				id_split = ele.parent('div').attr('data-hb-x-path-id').split(':');
				room_type = id_split[0];
				room_num = id_split[1];
				date = id_split[2];
				details = resp_data_calandar_data[date][room_type].room_numbers[room_num][0];
			}
		}
		
		
    var room_id; 
		if (viewName == 'gday') {
			room_id = parseInt(ele.attr('data-x-room-id'));
			if (!room_id)
				room_id = parseInt(ele.parent('div').attr('data-x-room-id'));
		} else {
			room_id = parseInt(ele.closest('tr').attr('data-room-id'));
		}

        // var details = jQuery.parseJSON(ele.attr('data-b-details'));
            	
        if(details != undefined || details != null){
        	var booking_id = details.hotel_booking_information.hotel_booking_id
          var booking_detail_id = details.hotel_booking_information.hotel_booking_detail_id
          var booking_number = details.hotel_booking_information.booking_number
        }

        // TODO need to for check-in and cancel and no show constant should be global
        if(key === 'open'){
        	eval(details.hotel_booking_information.booking_action.split(';')[0]);
        }
        if(key === 'cancelReservation'){
           	rightClickReservationActions(booking_id,booking_detail_id,1,5)
        }

        if(key === 'checkIn'){
            if(ele.hasClass('rc-available-current')){
            	openDialogBox(ele);
            }else{
            	rightClickReservationActions(booking_id,booking_detail_id,1,2)
            }
        }

        if(key === 'reserveCurrent' || key === 'reserve'){
            if(ele.hasClass('rc-dirty')){
            	if(dA.time){
            		openDialogBox(ele);
            	}else{
            		openDialogBox(ele.parent());
            	}
          	}else{
            	openDialogBox(ele);
           	}
        }

        if(key === 'noShow'){
        	rightClickReservationActions(booking_id,booking_detail_id,1,4)
        }
        if(key === 'advancePayments'){
        	advancePaymentForCalendar(booking_id,booking_detail_id,1)
        }     
        if(key === 'roomService'){
        	rightClickRoomServiceCharge(booking_id,1)
        }

        if(key === 'checkOut'){
        	rightClickReservationActions(booking_id,booking_detail_id,1,3)
        }

        if(key === 'stayExtend'){
        	rightClickStayExtenceActions(booking_id,booking_detail_id,1)
        } 

        if(key === 'roomSwap'){
        	rightClickRoomSwapActions(booking_id,booking_detail_id,1)
        }
        if(key === 'updateHK-status'){ 
        	url_path="/housekeeping/assign_status/"+room_id
            data = "show_calendar="+1
			getGeneralAjaxrequest(url_path,data)            		
        }

        if(key === 'updateHK-mtn'){
        	url_path="/availabilities/change_room_for_maintenance_from_FD/"+room_id
            
            if(ele.hasClass('rc-dirty') || ele.hasClass('rc-checkout')){
            	var selected_date = ele.parent().attr('data-date')
            	room_type_details = resp_data_room_type_details[ele.parent().attr('data-x-room-type-id')]; // jQuery.parseJSON(ele.parent().attr('data-rc-room-type')); 
            }else{
            	var selected_date = ele.attr('data-date')
            	room_type_details = resp_data_room_type_details[ele.attr('data-x-room-type-id')]; // jQuery.parseJSON(ele.attr('data-rc-room-type'));
            }
            data = "selected_date="+selected_date+"&room_type_id="+room_type_details.room_type_id
            getGeneralAjaxrequest(url_path,data)
        }

        if(key === "temp_confirm_booking") {
        	confirm_temp_reserve_booking(booking_id,booking_number);
        }
        if(key === "temp_release_booking") {
        	release_temp_reserve_booking(booking_id,booking_number);
        }
        if(key === "temp_extend_booking") {
        	extend_temp_reserve_booking(booking_id,booking_number);
        }

	}

	// TODO on booking chnage clor of cell
	function openDialogBox(ele){
		var datesRange = {}; // for checkIN and Reserve

		if(calendar.options.dragAndDrop){ // Enables Click & Drag Functionality
           	ele.toggleClass("hs-active");
            ele.toggleClass("dropped");
            datesRange.fromDate = ele.attr('data-date');
			datesRange.toDate = ele.attr('data-date');
			
			// var roomType = jQuery.parseJSON(ele.attr('data-rc-room-type'));
			// var details = jQuery.parseJSON(ele.attr('data-rc-b-details'));
			// var roomDetails = jQuery.parseJSON(ele.attr('data-rc-room-details'));
			
			// console.log(resp_data_room_type_details[ele.attr('data-x-room-type-id')])
			// console.log(resp_data_room_type_details[ele.attr('data-x-room-id')])
			
			var roomType = resp_data_room_type_details[ele.attr('data-x-room-type-id')]; // jQuery.parseJSON(ele.attr('data-rc-room-type'));
			var details = resp_data_room_type_details; // jQuery.parseJSON(ele.attr('data-rc-b-details'));			
			// console.log("roomType: " + roomType);
			var roomDetails = {};
			roomDetails.id = ele.attr('data-x-room-id');
			roomDetails.name = roomType.room_numbers[ele.attr('data-x-room-id')];
			
			// var roomDetails = roomType.room_numbers[ele.attr('data-x-room-id')] // jQuery.parseJSON(ele.attr('data-rc-room-details'));
			// console.log("roomDetails: " + roomDetails);

			if(roomType.is_dorm_bulk_booking){
				calendar.options.getDormitoryInfo(ele.attr('data-room-type-name'),ele.attr('data-date'),ele.attr('data-room-type'),ele,datesRange,0,roomType.room_type_id,defaultType);
			}else{
				calendar.options.getInfo(ele.attr('data-date'),roomDetails,ele.attr('data-room-type'),details,ele,datesRange,0,roomType.room_type_id,defaultType);
			}
        }else{
        	$(".bc-help-msg").effect("highlight", { color: "#ff0000" }, 1000);
			return false;
        }
	}

	function buildRoomTypeDivsForBulk(tr,roomType,details){
		$.each(dA.dates, function(i,v){
			
			_weekendColor = (v.isWeekend)? 'weekend' : '';
			weekend_style = (v.isWeekend)? {'background':calendar.options.legendColors.weekend} : {};

			if(v.isCurrent == 'future' || v.isCurrent == 'current'){
				 rcClass =  (v.isCurrent == 'current') ? 'rc-available-current rc-b-class' : 'rc-available rc-b-class';
				_pastColor = '';
			}else if(v.isCurrent == 'business-date'){
				_pastColor = 'businessDateClass';
				rcClass = '';
				// rcC = '';
				if( isCurrentDate(calendar.options.business_date) ){
					rcClass = 'rc-available-current rc-b-class';
					// rcC = 'availableCurrent ';
				}
			}else{
				_pastColor = 'diagonal_lines_pattern';
				_weekendColor = '';
				rcClass = '';
			}

			var menuItems = getMenuItems(v,false,roomType.is_dorm_bulk_booking,roomType);

			_date = new Date(v.date);
			_cD = new Date();

			var datesRange = {};

			// TODO -- Change the logic
			var sDate = (v.date).split('/');

			// var style = {'background':calendar.options.legendColors.available};
			var style = (v.isWeekend)? calendar.options.legendColors.weekend : calendar.options.legendColors.available

			if (_date > _cD || isCurrentDate(_date)) {
				(
					$("<td />",{'class':_weekendColor+' '+_pastColor+' spark_line_good drop '+rcClass})
						.attr({'id':'room-type-td-'+roomType.room_type_id+'-'+sDate[2]+'-'+appendZero(sDate[0])+'-'+appendZero(sDate[1])})
						.attr({'data-date':v.date})
						// .attr({'data-room-no':roomName})
						.attr({'data-room-type-name':roomType.name}) // TODO-DASH - REMOVE this
						.css(weekend_style) // Weekend Colorcoming from Database
						.css({'background':style})
						.attr({'data-color':style})
						// .attr({'data-room-no':roomName})

						// ADDED
						.attr({'data-x-room-type-id':roomType.room_type_id})
						// .attr({'data-x-room-id':roomDetails.id})	
			
						// REMOVED

						.attr({'data-room-type':roomType.room_type_id}) // TODO-DASH - REMOVE this
						// .attr({'data-rc-room-type':JSON.stringify(roomType)})
						// // .attr({'data-rc-room-details':JSON.stringify(roomDetails)})
						// .attr({'data-rc-b-details':JSON.stringify(details)})
						.attr({'data-right-clicks':JSON.stringify(menuItems)})

						.append(
								$("<div/>").css({'height':'100%'})
									// .append(function(){
									// 	return colorBlendarFunctionality(roomType.total_room_count,roomType.total_room_count);
									// })
									// .tooltip({
									// 	placement: 'top',
									// 	trigger: 'hover',
									// 	title: calculateRoomPersentage(roomType.total_room_count,roomType.total_room_count)+'%'
									// })
							)
						// .contextMenu(menuItems,{theme:'vista'})
						.on('contextmenu', function(e){
							e.preventDefault();
							// Your code.
							$(this).popover('hide');

							rightClickInit($(this),e);			  
						})
						.click(function(){
							if(calendar.options.dragAndDrop){ // Enables Click & Drag Functionality
								$('[data-toggle="context-menu"]').hide();
                             	$('[data-toggle="context-menu"]').addClass('hidden');
								$( this ).toggleClass("hs-active");
								
								if ($(this).attr('style'))
							        $(this).removeAttr('style');
							    else if ($(this).attr('style') === undefined) $(this).attr('style', {'background':style});
							    else $(this).attr('style', {'background':style});
								
								$( this ).toggleClass("dropped");
								datesRange.fromDate = $(this).attr('data-date');
								datesRange.toDate = $(this).attr('data-date');
								calendar.options.getDormitoryInfo($(this).attr('data-room-type-name'),$(this).attr('data-date'),$(this).attr('data-room-type'),$(this),datesRange,0,roomType.room_type_id,defaultType);
							// calendar.options.getInfo($(this).attr('data-date'),$(this).attr('data-room-type'),$(this),datesRange,0);
							}else{
								$(".bc-help-msg").effect("highlight", { color: "#ff0000" }, 1000);
								return false;
							}
						})
						.drop("start",function(){
							$( this ).toggleClass("hs-active");
						    $(this).attr('style', {'background':style});

							datesRange.fromDate = $(this).attr('data-date');
						})
						.drop(function( ev, dd ){
							$( this ).toggleClass("dropped");
							$( this ).toggleClass('hs-active');
 
						    $(this).attr('style', {'background':style});

							// datesRange.toDate = $(this).attr('data-date');
							calendar.options.getDormitoryInfo($(this).attr('data-room-type-name'),$(this).attr('data-date'),$(this).attr('data-room-type'),$(this),datesRange,1,roomType.room_type_id,defaultType);
						})
						.drop("end",function(){
							datesRange.toDate = $(this).attr('data-date');
							$( this ).toggleClass("hs-active");

						    $(this).attr('style', {'background':style});
						})
				).appendTo(tr);

			}else if((_date < _cD && calendar.options.business_date <= _date) || isEqualDates(calendar.options.business_date,_date)){
					($("<td />",{'class':_weekendColor})
					.css({'background':style})
					.attr({'id':'room-type-td-'+roomType.room_type_id+'-'+sDate[2]+'-'+appendZero(sDate[0])+'-'+appendZero(sDate[1])})
					.attr({'data-date':v.date})
					// .css({
					// 	'background-color':'rgba(0, 0, 0, 0.05)'
					// })
					.append(
						$('<div/>')
							.css({
								'background-color':'rgba(0, 0, 0, 0.23)',
								'height': '100%',
								'border': 'medium none',
								'position': 'relative',
								'width':'100%'

							})
							.click(function(e){
								nightAuditMessage($(this),e);
								return false;
							})
					)
					
					).appendTo(tr);
				}else{
				($("<td />",{'class':_weekendColor+' '+_pastColor+' spark_line_good'})
					.attr({'id':'room-type-td-'+roomType.room_type_id+'-'+sDate[2]+'-'+appendZero(sDate[0])+'-'+appendZero(sDate[1])})
					.attr({'data-date':v.date})
					.append(
								$("<div/>").css({'height':'100%'})
									.tooltip({
										placement: 'top',
										trigger: 'hover'
									})
						)
				).appendTo(tr);
			}
		});
	}

	function _displayRoomTypeDivs(tr,roomType,roomName,dateWiseDataAvailbleRooms,totalRooms){

		$.each(dA.dates, function(i,v){

			_weekendColor = (v.isWeekend)? 'weekend' : '';

			if(v.isCurrent == 'future' || v.isCurrent == 'current'){
				_pastColor = '';
			}else{
				_pastColor = 'diagonal_lines_pattern';
				_weekendColor = '';
			}

			_date = new Date(v.date);
			_cD = new Date();

			// TODO -- Change the logic
			if (_date > _cD || isCurrentDate(_date)) {
				(
					$("<td />",{'class':_weekendColor+' '+_pastColor+' spark_line_good'})
						.attr({'data-date':v.date})
						.attr({'data-room-no':roomName})
						.attr({'data-room-type':roomType})
						.append(
								$("<div/>").css({'height':'100%'})
									// .attr({'data-placement':'top'})
									// .attr({'data-selector':true})
									// .attr({'data-toggle':'tooltip'})
									// .attr({'data-trigger':'hover'})
									// .attr({'data-container':'Hello'})
									.append(function(){
										return colorBlendarFunctionality(dateWiseDataAvailbleRooms[i],totalRooms[i]);
									})
									.tooltip({
										placement: 'top',
										// container: "Hello",
										trigger: 'hover',
										title: calculateRoomPersentage(dateWiseDataAvailbleRooms[i],totalRooms[i])+'%'
									})
									// .mouseover(function(){
									// 	// showToolTip(this);
									// 	// console.log($(this).find('input').val());
									// 	$(this).tooltip('toggle')
									// })
							)
						// .append(
						// 		$('<div />')
						// 		.append('2,4,9,10,-2,-4,12')
						// 		// .append('1:4,2:3,3:2,4:1')
						// 	)
				).appendTo(tr);

			}else{
				($("<td />",{'class':_weekendColor+' '+_pastColor+' spark_line_good'})
					.attr({'data-date':v.date})
					.append(
								$("<div/>").css({'height':'100%'})
									// .attr({'data-placement':'top'})
									// .attr({'data-selector':true})
									// .attr({'data-toggle':'tooltip'})
									// .attr({'data-trigger':'hover'})
									// .attr({'data-container':'Hello'})
									.append(function(){
										return colorBlendarFunctionality(dateWiseDataAvailbleRooms[i],totalRooms[i]);
									})
									.tooltip({
										placement: 'top',
										// container: "Hello",
										trigger: 'hover',
										title: calculateRoomPersentage(dateWiseDataAvailbleRooms[i],totalRooms[i])+'%'
									})
						)

					// .append(
					// 		$('<div />')
					// 			.append('2,4,9,10,-2,-4,12')
					// 			// .append('1:4,2:3,3:2,4:1')
					// 	)
				).appendTo(tr);
			}
		});
	}

	function _buildRoomTypeDivs(tr,roomType){
		// console.log("_buildRoomTypeDivs");
		$.each(dA.dates, function(i,v){
			
			_weekendColor = (v.isWeekend)? 'weekend' : '';
			weekend_style = (v.isWeekend)? {'background':calendar.options.legendColors.weekend} : {};

			if(v.isCurrent == 'future' || v.isCurrent == 'current'){
				_pastColor = '';
			}else if(v.isCurrent == 'business-date'){
				_pastColor = 'businessDateClass';
			}else{
				_pastColor = 'diagonal_lines_pattern';
				_weekendColor = '';
			}

			_date = new Date(v.date);
			_cD = new Date();

			// TODO -- Change the logic
			var sDate = (v.date).split('/');
			if (_date > _cD || isCurrentDate(_date)) {
				(
					$("<td />",{'class':_weekendColor+' '+_pastColor+' spark_line_good'})
						.attr({'id':'room-type-td-'+roomType.room_type_id+'-'+sDate[2]+'-'+appendZero(sDate[0])+'-'+appendZero(sDate[1])})
						.attr({'data-date':v.date})
						.css(weekend_style) // Weekend Colorcoming from Database
						// .attr({'data-room-no':roomName})
						.attr({'data-room-type':roomType.room_type_id})
						.append(
								$("<div/>").css({'height':'100%'})
									// .attr({'data-placement':'top'})
									// .attr({'data-selector':true})
									// .attr({'data-toggle':'tooltip'})
									// .attr({'data-trigger':'hover'})
									// .attr({'data-container':'Hello'})
									.append(function(){
										return colorBlendarFunctionality(roomType.total_room_count,roomType.total_room_count);
									})
									.tooltip({
										placement: 'top',
										// container: "Hello",
										trigger: 'hover',
										title: calculateRoomPersentage(roomType.total_room_count,roomType.total_room_count)+'%'
									})
									// .mouseover(function(){
									// 	// showToolTip(this);
									// 	// console.log($(this).find('input').val());
									// 	$(this).tooltip('toggle')
									// })
							)
						// .append(
						// 		$('<div />')
						// 		.append('2,4,9,10,-2,-4,12')
						// 		// .append('1:4,2:3,3:2,4:1')
						// 	)
				).appendTo(tr);

			}else if((_date < _cD  && calendar.options.business_date <= _date) || isEqualDates(calendar.options.business_date,_date)){
				(
					$("<td />",{'class':_weekendColor })
						.attr({'id':'room-type-td-'+roomType.room_type_id+'-'+sDate[2]+'-'+appendZero(sDate[0])+'-'+appendZero(sDate[1])})
						.attr({'data-date':v.date})
						.append(
								$("<div/>").css({'height':'100%'})
									.css({
										'background-color': 'rgba(0, 0, 0, 0.05)'
									})
									// .attr({'data-placement':'top'})
									// .attr({'data-selector':true})
									// .attr({'data-toggle':'tooltip'})
									// .attr({'data-trigger':'hover'})
									// .attr({'data-container':'Hello'})
									.append(function(){
										// return colorBlendarFunctionality(dateWiseDataAvailbleRooms[i],totalRooms[i]);
									})
									.tooltip({
										placement: 'top',
										// container: "Hello",
										trigger: 'hover'
										// title: calculateRoomPersentage(dateWiseDataAvailbleRooms[i],totalRooms[i])+'%'
									})
						)
				).appendTo(tr);
			}else{
				($("<td />",{'class':_weekendColor+' '+_pastColor+' spark_line_good'})
					.attr({'id':'room-type-td-'+roomType.room_type_id+'-'+sDate[2]+'-'+appendZero(sDate[0])+'-'+appendZero(sDate[1])})
					.attr({'data-date':v.date})
					.append(
								$("<div/>").css({'height':'100%'})
									// .attr({'data-placement':'top'})
									// .attr({'data-selector':true})
									// .attr({'data-toggle':'tooltip'})
									// .attr({'data-trigger':'hover'})
									// .attr({'data-container':'Hello'})
									.append(function(){
										// return colorBlendarFunctionality(dateWiseDataAvailbleRooms[i],totalRooms[i]);
									})
									.tooltip({
										placement: 'top',
										// container: "Hello",
										trigger: 'hover'
										// title: calculateRoomPersentage(dateWiseDataAvailbleRooms[i],totalRooms[i])+'%'
									})
						)

					// .append(
					// 		$('<div />')
					// 			.append('2,4,9,10,-2,-4,12')
					// 			// .append('1:4,2:3,3:2,4:1')
					// 	)
				).appendTo(tr);
			}
		});
	}

	// Calculate Room Persentage
	function calculateRoomPersentage(availbleRooms,totalRooms){

		var bookedRooms = parseFloat(totalRooms) - parseFloat(availbleRooms);

		var totalPercentage = (parseFloat(bookedRooms)/parseFloat(totalRooms))*100;
		
		// Round of the value.
		{
			var t_a = parseFloat(totalPercentage);
			var t_b = parseInt(totalPercentage);
			var t_c = parseFloat(t_a) - parseFloat(t_b);
			if(parseFloat(t_c) > parseFloat(0.5)) totalPercentage = parseFloat(totalPercentage) + 1;
		}

		return parseInt(totalPercentage);
	}

	function renderEvents(){		
		var checkIn = new Array();
		var checkOut = new Array();
		var room_nos = new Array();
		var presentDate = new Array();
		var sDates = new Array();
		var hotelBookingDetails = new Array();

		var mtnStartDate = new Array();
		var mtnEndDate = new Array();
		var mtnRoomNos = new Array();
		var mtnStatus = new Array();

		var bulkStartDate = new Array();
		var bulkEndDate = new Array();
		var bulkRoomTypes = new Array();
		var bulkRooms = new Array();
		var bulkBookingDetails = new Array();


		$.each(data_events,function(date,booking_details){
			var _t = 0;
			var _dirty = 0;
			var _vacant = 0;
			var _maintenance = 0;
			var _dirty_cot = 0;
			var _dirty_bkd = 0;
			var _dirty_mtn = 0;
			var _ckek_out_count = 0;
			$.each(booking_details,function(i,row){
				if (row.booked_count)
					_t =  parseInt(_t)+parseInt(row.booked_count);
				if (row.maintaincence_count)
					_maintenance =  parseInt(_maintenance)+parseInt(row.maintaincence_count);
				
				// Room Type Occupancy Show
				// TODO
				if(dA.time){
					displayRoomTypeLevelAvailabilityForDay(i,date,row.booked_count);
				}else{
					displayRoomTypeLevelAvailability(i,date,row.booked_count);
				}

				if(row.room_numbers){
					$.each(row.room_numbers,function(room_id,room_status){
						
						$.each(room_status, function(ii,b_details){
							if(b_details.booking_status){
								if ( b_details.booking_status == "BKD" && isCurrentDate(new Date(date)) ) _dirty_bkd += 1;
								if ( b_details.booking_status == "COT" && isCurrentDate(new Date(date)) ) _dirty_cot += 1;
								if ( b_details.booking_status == "COT" && (new Date() > new Date(date)) ) _ckek_out_count += 1;

								if ( b_details.hotel_booking_information.is_dormitory && b_details.hotel_booking_information.is_dorm_bulk_booking ){
									bulkStartDate.push(b_details.hotel_booking_information.checkin_time)
									bulkEndDate.push(b_details.hotel_booking_information.checkout_time)
									var temp_hash = {
										room_type_id: i,
										room_number: b_details.room_number
									};
									bulkRooms.push(temp_hash)
									bulkRoomTypes.push(i);

									bulkBookingDetails.push(b_details)
								}else{
									checkIn.push(b_details.hotel_booking_information.checkin_time)
									checkOut.push(b_details.hotel_booking_information.checkout_time)

									room_nos.push(room_id);
									// presentDate.push(_presentDate);
									sDates.push(date);
									hotelBookingDetails.push(b_details);
								}

							}else if(b_details.blocked_status){
								if ( b_details.blocked_status == "MTN" && b_details.hk_status == "DRT" && isCurrentDate(new Date(date)) ) _dirty_mtn += 1;
								mtnStartDate.push(b_details.from_date);
								mtnEndDate.push(b_details.to_date);
								mtnRoomNos.push(room_id);
								mtnStatus.push(b_details.blocked_status);
							}
						});

					});
				}else{
				}

			});
			
			// Dirty Occupied
			totalDirtyRoomCount = parseInt(totalDirtyRoomCount) - parseInt(_dirty_bkd) - parseInt(_dirty_mtn);

			// Caluculate Vcant rooms
			_vacant = parseInt(totalRoomCount) - parseInt(_t) - parseInt(_maintenance);


			tatalOccupancyCount.push({date:date,total_count:_t,occupied:_t,dirty:_dirty,vacant:_vacant,maintenance:_maintenance,check_outs:_ckek_out_count});
		});
		

		// TODO needs to check this logic
		if(viewName != 'day' && viewName != 'gday')
			setAllDatesForStatus();

		// Display the total Occupancy respect with date
		displayTotalOccupany();


		// var pDate = new Date(_presentDate);
		var tdWidth = $("#grid-container-table").find('tr:first').find('td:first').next().width();

		if(dA.time){
			// renderEventsForDay(dA.dates.date);
			_renderEventsForDay(checkIn,checkOut,room_nos,hotelBookingDetails,tdWidth);
			_renderMTNEventsForDay(mtnStartDate,mtnEndDate,mtnRoomNos,mtnStatus,tdWidth);
		}else{
			_renderMTNEvents(mtnStartDate,mtnEndDate,mtnRoomNos,mtnStatus,tdWidth);
			_renderEventsForNormalView(checkIn,checkOut,room_nos,hotelBookingDetails,tdWidth);
			_renderEventsForBulkBookings(bulkStartDate,bulkEndDate,bulkRoomTypes,bulkRooms,bulkBookingDetails,tdWidth);

		}

		// Display total status
		var allCtds = $('.total-status-popover');
		allCtds.each(function(i,_td){
			if($(_td).attr('data-day') != "true" ){
				$(_td)
					.attr({'data-html':true,'data-trigger':'hover','data-placement':'top'})
					.attr({'data-content':displayPopoverTotalStatus(this)})
					.attr({'data-container':'body'})
					.popover();
			}
		})
	}

	// TODO - needs to check once.
	function displayTotalOccupany(){
		$.each(tatalOccupancyCount, function(i,row){

			var total_count = parseInt(row.total_count);
			if($("#total-occupancy-"+row.date).attr('data-is-current') == "past" ){
				total_count = parseInt(row.occupied) + parseInt(row.check_outs);
			}

			$("#total-occupancy-"+row.date).text(total_count);
			// td += '<td align="center" data-is-current="'+v.isCurrent+'" id="total-occupancy-'+_sDate.getFullYear()+'-'+appendZero(parseInt(_sDate.getMonth())+1)+'-'+appendZero( parseInt(_sDate.getDate()) )+'">0</td>'
			
			if(viewName != 'day' && viewName != 'gday'){

				var _totalData = $.parseJSON( $('#total-status-'+row.date).attr('data-total') );
				if(_totalData){
					_totalData.occupied = row.occupied;
					if($('#total-status-'+row.date).attr('data-is-current') == "past") _totalData.occupied = row.check_outs;
					_totalData.maintenance = row.maintenance;
					_totalData.vacant = parseInt(totalRoomCount) - parseInt(row.occupied) - parseInt(_totalData.dirty) - parseInt(row.maintenance);				
					if($('#total-status-'+row.date).attr('data-is-current') == "past"){
						_totalData.vacant = parseInt(totalRoomCount) - parseInt(row.check_outs) - parseInt(_totalData.dirty) - parseInt(row.maintenance);				
					}
				}
				
				$("#total-status-"+row.date).attr({'data-total':JSON.stringify(_totalData)});
			}
		});
	}

	function setAllDatesForStatus(){
		$.each(dA.dates, function(i,v){
			var _sDate = new Date(v.date);

			var _totalData = $.parseJSON( $('#total-status-'+_sDate.getFullYear()+'-'+appendZero(parseInt(_sDate.getMonth())+1)+'-'+appendZero( parseInt(_sDate.getDate()) )).attr('data-total') )
			// Dirty Room
			if(v.isCurrent == "current")
				_totalData.dirty = totalDirtyRoomCount;
			_totalData.vacant = totalRoomCount - parseInt(_totalData.occupied) - parseInt(_totalData.dirty) - parseInt(_totalData.maintenance);

			// Set the values
			$('#total-status-'+_sDate.getFullYear()+'-'+appendZero(parseInt(_sDate.getMonth())+1)+'-'+appendZero( parseInt(_sDate.getDate()) )).attr({'data-total':JSON.stringify(_totalData)});
		})
	}
	
	function _renderEventsForDay(checkIn,checkOut,room_nos,hotelBookingDetails,tdWidth){
		// console.log("_renderEventsForDay");
		var pDate = new Date(dA.dates.date);

		// Only Booking Details
		for(i=0;i<room_nos.length;i++){
			// Check In & Check Out Date
			var cit = new Date(checkIn[i]);
			var cot = new Date(checkOut[i]);

			if(isCurrentDate(cit) && hotelBookingDetails[i].booking_status === 'RES'){
				var statusClass = 'rc-reserve-current'
			}else{
				var statusClass = getStatusClass(hotelBookingDetails[i].booking_status);
			}

			// var rightClickOpts = getRightOptions(hotelBookingDetails[i],cit); // Get Right Click Options
			var rightClickOpts = JSON.stringify(getOptions(hotelBookingDetails[i],cit));

			var menuItems = getMenuItemsForEvents($('#room-time-scale-'+room_nos[i]+'-'+_cit+'-'+appendZero(cit.getHours())),hotelBookingDetails[i],isCurrentDate(cit)); // Items
			var statusColor = (hotelBookingDetails[i].booking_status === 'RES' )?calendar.options.legendColors.temp_reserve:calendar.options.legendColors.booked
			statusColor = (hotelBookingDetails[i].booking_status === 'COT' ) ? calendar.options.legendColors.checkout : statusColor;
			statusColor = (hotelBookingDetails[i].booking_status === 'VOD' ) ? calendar.options.legendColors.void_booking : statusColor;
			statusColor = (hotelBookingDetails[i].booking_status === 'TEMP' ) ? calendar.options.legendColors.temp_reserve : statusColor;
			

			var bookingDetails = JSON.stringify(hotelBookingDetails[i]);
			
			// _width = 0;
			if(isDatesEqual(pDate,cit)){
				var _cit = getDateFormate(cit);
				var _cot = getDateFormate(cot);
				// Get Width of CELL
				if(isDatesEqual(pDate,cot)){
					if( isTimeEqual(parseInt(cot.getHours()), parseInt(cit.getHours())) ) {
						_width = parseInt(cot.getMinutes()) - parseInt(cit.getMinutes());
						_width = (_width/100);
					}else{
						_width = parseInt(cot.getHours())-parseInt(cit.getHours());
					}
				}else{
					_width = 24-parseInt(cit.getHours());
				}
				width = (tdWidth*_width)+(_width/2); // Calculae Width
				
				if (viewName == 'gday')
					width = '100'				
				
				// console.log("hb 1");
				// alert("CHECK THIS hb 1 - TODO");
				// Width Done
					// console.log("hotelBookingDetails[i].hotel_booking_information.hb_x_path_id 3: " + hotelBookingDetails[i].hotel_booking_information.hb_x_path_id)
				$('#room-time-scale-'+room_nos[i]+'-'+_cit+'-'+appendZero(cit.getHours()))
					.html(
						$("<div/>")
							.css({'width':width+'px','height':'100%','background':statusColor,'position':'relative'})
							.attr({'id':'rc-'+room_nos[i]+'-'+_cit+'-'+appendZero(cit.getHours())})
							// .html('<span style="position: relative; font-size: 12px; top: 16%;cursor: default;">'+hotelBookingDetails[i].booking_status+'<span>')
							.addClass('hs-booking-details hs-book-room rc-b-class  '+statusClass)
							// Popover
							.attr({'data-html':true,'data-trigger':'hover','data-delay':'1500','data-placement':'left'})
							// .attr({'data-content':detailsPopover(hotelBookingDetails[i])})
							// .attr({'data-content':"<b>.</b>"}) // TODO: For some reasom this is needed, else events are not registered! CHECK n FIX
							.attr({'data-container':'body'})
							// .attr({'data-b-details':bookingDetails})
							.attr({'data-hb-x-path-id':hotelBookingDetails[i].hotel_booking_information.hb_x_path_id})						
							.attr({'data-right-clicks':JSON.stringify(menuItems)})
							.attr({'data-index':''+i})
							.mouseover(function() {
								if (!$(this).attr('data-content')) {
									$(this).attr({'data-content':detailsPopover(hotelBookingDetails[$(this).attr("data-index")])});
									$(this).popover({
										delay: { show: 100, hide: 1300 }
									})
									.popover('show');
								}
							})						
							// .popover({
							// 	delay: { show: 100, hide: 1300 }
							// })
							// .popover('show')
							// .on('show.bs.popover', function () {
							// 	console.log("show.bs.popover 1");
							// 	$(this).attr({'data-content':detailsPopover(hotelBookingDetails[i])});
							// })
							
							.on('contextmenu', function(e){
							  // Your code.
							  $(this).popover('hide');
							})
							.on('contextmenu', function(e){
							  e.preventDefault();
							  // Your code.
							  $(this).popover('hide');

							  rightClickInit($(this),e);			  
							})
							
							// .contextMenu(menuItems,{theme:'vista'})
					);
				
				disableDragAndDrop($('#room-time-scale-'+room_nos[i]+'-'+_cit+'-'+appendZero(cit.getHours())),_width);
				// Call contextMenu PlugIn
				// callContextMenuPlugIn('#rc-'+room_nos[i]+'-'+_cit+'-'+appendZero(cit.getHours()),rightClickOpts);
			}else if(cit <= pDate && pDate <= cot){
				
				var statusColor = (hotelBookingDetails[i].booking_status === 'RES' )?calendar.options.legendColors.reserved:calendar.options.legendColors.booked
				statusColor = (hotelBookingDetails[i].booking_status === 'COT' ) ? calendar.options.legendColors.checkout : statusColor;
				statusColor = (hotelBookingDetails[i].booking_status === 'VOD' ) ? calendar.options.legendColors.void_booking : statusColor;

			statusColor = (hotelBookingDetails[i].booking_status === 'TEMP' ) ? calendar.options.legendColors.temp_reserve : statusColor;
				
				var _cit = getDateFormate(cit);
				var _cot = getDateFormate(cot);
				var _pDate = getDateFormate(pDate);

				// Get Width of CELL
				$("#room-row-"+room_nos[i]).show();
				if(isDatesEqual(pDate,cot)){
					_width = parseInt(cot.getHours());
				}else{
					_width = 24
				}
				width = (tdWidth*_width)+(_width); // Calculae Width
				$("#room-row-"+room_nos[i]).hide();
				// Width Done

				$('#room-time-scale-'+room_nos[i]+'-'+_pDate+'-'+appendZero(pDate.getHours()))
					.html(
						$("<div/>")
							.css({'width':width+'px','height':'100%','background':statusColor,'position':'relative'})
							.attr({'id':'rc-'+room_nos[i]+'-'+_pDate+'-'+appendZero(pDate.getHours())})
							// .html('<span style="position: relative; font-size: 12px; top: 16%;cursor: default;">'+hotelBookingDetails[i].booking_status+'<span>')
							.addClass('hs-booking-details rc-b-class '+statusClass)
							// Popover
							.attr({'data-html':true,'data-trigger':'hover','data-delay':'1500','data-placement':'left'})
							// .attr({'data-content':detailsPopover(hotelBookingDetails[i])})
							// .attr({'data-content':"<b>.</b>"}) // TODO: For some reasom this is needed, else events are not registered! CHECK n FIX
							.attr({'data-container':'body'})
							// .attr({'data-b-details':bookingDetails})
							.attr({'data-hb-x-path-id':hotelBookingDetails[i].hotel_booking_information.hb_x_path_id})					
							.attr({'data-right-clicks':JSON.stringify(menuItems)})
							.attr({'data-index':''+i})
							.mouseover(function() {
								if (!$(this).attr('data-content')) {
									$(this).attr({'data-content':detailsPopover(hotelBookingDetails[$(this).attr("data-index")])});
									$(this).popover({
										delay: { show: 100, hide: 1300 }
									})
									.popover('show');
								}
							})												
							// .popover({
							// 	delay: { show: 100, hide: 1300 }
							// })
							// .popover('show')
							.on('contextmenu', function(e){
							  // Your code.
							  $(this).popover('hide');
							})
							.on('contextmenu', function(e){
							  e.preventDefault();
							  // Your code.
							  $(this).popover('hide');

							  rightClickInit($(this),e);			  
							})
							// .on('show.bs.popover', function () {
							// 	console.log("show.bs.popover 2");
							// 	$(this).attr({'data-content':detailsPopover(hotelBookingDetails[i])});
							// })
							
							// .contextMenu(menuItems,{theme:'vista'})
					);
				disableDragAndDrop($('#room-time-scale-'+room_nos[i]+'-'+_pDate+'-'+appendZero(pDate.getHours())),_width)
				// Call contextMenu PlugIn
				// callContextMenuPlugIn('#rc-'+room_nos[i]+'-'+_pDate+'-'+appendZero(pDate.getHours()),rightClickOpts);
			}

		} // End Booking Details Loop

	}

	function _renderMTNEventsForDay(mtnStartDate,mtnEndDate,mtnRoomNos,mtnStatus,tdWidth){
		// console.log("_renderMTNEventsForDay:" + mtnRoomNos)
		var pDate = new Date(dA.dates.date);

		// MTN Status
		if(mtnRoomNos.length){ // Condition Start
			// Show MTN Status Loop
			for(i=0;i<mtnRoomNos.length;i++){
				var st = new Date(mtnStartDate[i]);
				var et = new Date(mtnEndDate[i]);

				if(isDatesEqual(pDate,st)){
					var _st = getDateFormate(st);
					var _et = getDateFormate(et);
					var _pDate = getDateFormate(pDate);

					if(isDatesEqual(pDate,et)){
						_width = parseInt(et.getHours())-parseInt(st.getHours());
					}else{
						_width = 24-parseInt(st.getHours());
					}
					width = (tdWidth*_width)+(_width/2); // Calculae Width

					$('#room-time-scale-'+mtnRoomNos[i]+'-'+_st+'-'+appendZero(st.getHours())).html(
						$("<div/>").css({'width':width+'px','height':'100%','background':calendar.options.legendColors.maintenance,'position':'relative'})//.html('<span style="position: relative; font-size: 12px; top: 16%;cursor: default;">'+mtnStatus[i]+'<span>')
								.addClass('hs-booking-details')
								.contextMenu("#mtnRightOptions")
					);
					disableDragAndDrop($('#room-time-scale-'+mtnRoomNos[i]+'-'+_st+'-'+appendZero(st.getHours())),_width)

				}else if(st <= pDate && pDate <= et){
					var _st = getDateFormate(st);
					var _et = getDateFormate(et);
					var _pDate = getDateFormate(pDate);

					// Width Calculate
					if(isDatesEqual(pDate,et)){
						_width = parseInt(et.getHours());
					}else{
						_width = 24
					}
					width = (tdWidth*_width)+(_width/2); // Calculae Width

					$('#room-time-scale-'+mtnRoomNos[i]+'-'+_pDate+'-'+appendZero(pDate.getHours())).html(
						$("<div/>").css({'width':width+'px','height':'100%','position':'relative','background':calendar.options.legendColors.maintenance})//.html('<span style="position: relative; font-size: 12px; top: 16%;cursor: default;">'+mtnStatus[i]+'<span>')
								.addClass('hs-booking-details')
								.contextMenu("#mtnRightOptions")			
					);
					disableDragAndDrop($('#room-time-scale-'+mtnRoomNos[i]+'-'+_pDate+'-'+appendZero(pDate.getHours())),_width)
				}

			}// End MTN Status Loop
		} // Condition End

	}

	// Events
	function _renderEventsForNormalView(checkIn,checkOut,room_nos,hotelBookingDetails,tdWidth){
		
		var td_obj_for_width = $("#grid-container-table").find('tr:first').find('td:first').next();

		// Only Booking Details
		for(i=0;i<room_nos.length;i++){

			var room_status = hotelBookingDetails[i];
			
			var cit = new Date(checkIn[i]);
			var cot = new Date(checkOut[i]);

			// While Check out time only
			// is_cot_width = ( room_status.booking_status === 'COT' && isCurrentDate(cot) ) ? tdWidth : 0;
			if(!room_status.hotel_booking_information.is_hourly_based){
				is_cot_width = ( room_status.booking_status === 'COT' && isCurrentDate(cot) && !isCurrentDate(cit) ) ? tdWidth : 0;
			}else{
				is_cot_width = 0;
			}

			if( (isDateBetweenTwoDates(cit) && isDateBetweenTwoDates(cot)) || ( isDatesEqual(cit,t.start) && isDatesEqual(cot,t.end) ) || ( isDatesEqual(cit,cot) ) ){
				var oneDay  = 24*60*60*1000;
				// Prev one. if you use no_of_days value it'll effect in calender width of booking while do early checkins
				// var diffDays = hotelBookingDetails[i].no_of_days; 
				
				// TODO -- needs to change the date caluculations
				var _arr_t = new Date((cit.getMonth()+1)+'/'+cit.getDate()+'/'+cit.getFullYear());
				var _dep_t = new Date((cot.getMonth()+1)+'/'+cot.getDate()+'/'+cot.getFullYear());
				
				// var diffDays = Math.abs((cot.getTime() - cit.getTime()) / oneDay);
				var diffDays = _arr_t.DaysBetween(_dep_t)
				
				if(parseInt(diffDays) === 0){
					diffDays = 1;
				}
				

				var _cit = getDateFormate(cit);
				var _cot = getDateFormate(cot);

			}else if( cit <= t.start && isDateBetweenTwoDates(cot) ) {
				

				// TODO -- needs to change the date caluculations
				var _arr_t = new Date((t.start.getMonth()+1)+'/'+t.start.getDate()+'/'+t.start.getFullYear());
				var _dep_t = new Date((cot.getMonth()+1)+'/'+cot.getDate()+'/'+cot.getFullYear());
				
				var oneDay  = 24*60*60*1000;
				// var diffDays = Math.abs((cot.getTime() - cit.getTime()) / oneDay);
				// var diffDays = _arr_t.DaysBetween(_dep_t)
				
				var diffDays = Math.abs((cot.getTime() - t.start.getTime()) / oneDay);

				if(parseInt(diffDays) === 0){
					diffDays = 1;
				}
				// diffDays = parseInt(diffDays) - parseInt(is_cot);
				// width = (tdWidth*parseInt(diffDays))+parseInt(parseInt(diffDays)/2);

				var _cit = getDateFormate(t.start);
				var _cot = getDateFormate(cot);
			}else if( isDateBetweenTwoDates(cit) && cot >= t.end ){
				// TODO -- needs to change the date caluculations
				var _arr_t = new Date((cit.getMonth()+1)+'/'+cit.getDate()+'/'+cit.getFullYear());
				var _dep_t = new Date((t.end.getMonth()+1)+'/'+t.end.getDate()+'/'+t.end.getFullYear());
				
				// var diffDays = Math.abs((cot.getTime() - cit.getTime()) / oneDay);
				// var diffDays = _arr_t.DaysBetween(_dep_t)
				
				// if(parseInt(diffDays) === 0){
				// 	diffDays = 1;
				// }

				var oneDay  = 24*60*60*1000;
				// TODO -- Last Day is fixed. for that purpose only wrote this condition
				// var diffDays = ( isDatesEqual(cot,t.end) ) ? 1 : Math.abs((t.end.getTime() - cit.getTime()) / oneDay);
				
				var diffDays = Math.abs((t.end.getTime() - cit.getTime()) / oneDay);

				if( !isDatesEqual(cot,t.end) && (cot > t.end) ){
					var diffDays = Math.abs((cot.getTime() - cit.getTime()) / oneDay);
				}

				if(parseInt(diffDays) === 0){
					diffDays = 1;
				}
				// diffDays = parseInt(diffDays) - parseInt(is_cot);
				// width = (tdWidth*parseInt(diffDays))+parseInt(parseInt(diffDays)/2);

				var _cit = getDateFormate(cit);
				var _cot = getDateFormate(t.end);
			}else if( cit <= t.start && cot >= t.end ){
					var oneDay  = 24*60*60*1000;
					
					var diffDays = Math.abs((t.end.getTime() - t.start.getTime()) / oneDay);
					// width = (tdWidth*parseInt(diffDays))+parseInt(parseInt(diffDays))+tdWidth;

					var _cit = getDateFormate(t.start);
					var _cot = getDateFormate(t.end);

			}else if( isDatesEqual(cit,t.end) || isDatesEqual(cot,t.start) ){
				
				// TODO -- needs to change the date caluculations
				// if(isDatesEqual(cit,t.end)){
				// 	var _arr_t = new Date((cit.getMonth()+1)+'/'+cit.getDate()+'/'+cit.getFullYear());
				// 	var _dep_t = new Date((t.end.getMonth()+1)+'/'+t.end.getDate()+'/'+t.end.getFullYear());
				// }else if(isDatesEqual(cot,t.start)){
				// 	var _arr_t = new Date((t.start.getMonth()+1)+'/'+t.start.getDate()+'/'+t.start.getFullYear());
				// 	var _dep_t = new Date((cot.getMonth()+1)+'/'+cot.getDate()+'/'+cot.getFullYear());
				// }

				var oneDay  = 24*60*60*1000;
				
				// var diffDays = Math.abs((cot.getTime() - cit.getTime()) / oneDay);
				// var diffDays = _arr_t.DaysBetween(_dep_t)
				var diffDays = 1;
				
				// if(parseInt(diffDays) === 0){
				// 	diffDays = 1;
				// }

				if(isDatesEqual(cit,t.end)){
					var _cit = getDateFormate(cit);
					var _cot = getDateFormate(t.end);
				}else if(isDatesEqual(cot,t.start)){
					var _cit = getDateFormate(t.start);
					var _cot = getDateFormate(cot);
				}
			}
			// Width of Booking Details div
			width = ((tdWidth*parseInt(diffDays))+parseInt(parseInt(diffDays)/2))// - (parseInt(is_cot_width));

			if( cit <= t.start && cot >= t.end ){
				width = parseInt(width) + parseInt(tdWidth) + parseInt(tdWidth/2);
			}
			if (viewName == 'gday')
				width = '100'

			var statusColor = (hotelBookingDetails[i].booking_status === 'RES' )?calendar.options.legendColors.reserved : calendar.options.legendColors.booked;
			statusColor = (hotelBookingDetails[i].booking_status === 'COT' ) ? calendar.options.legendColors.checkout : statusColor;
			statusColor = (hotelBookingDetails[i].booking_status === 'VOD' ) ? calendar.options.legendColors.void_booking : statusColor;
			statusColor = (hotelBookingDetails[i].booking_status === 'TEMP' ) ? calendar.options.legendColors.temp_reserve : statusColor;
			
			if(isCurrentDate(cit) && hotelBookingDetails[i].booking_status === 'RES'){
				var statusClass = 'rc-reserve-current'
			}else{
				var statusClass = getStatusClass(hotelBookingDetails[i].booking_status);
			}

			// var rightClickOpts = getRightOptions(hotelBookingDetails[i],cit);

			var bookingDetails = JSON.stringify(hotelBookingDetails[i]);
			var rightClickOpts = JSON.stringify(getOptions(hotelBookingDetails[i],cit));
			// Right Click Menu
			// var menu1 = [];

			var menuItems = getMenuItemsForEvents($('#room-'+room_nos[i]+'-'+_cit),hotelBookingDetails[i],isCurrentDate(cit));
			menuItems2 = [{'label':'open','labelName':'Open','disabled':false},{'label':'checkIn','labelName':'Check-In','disabled':false},{'label':'checkOut','labelName':'Check-Out','disabled':false}];

			// menu1.push({menuOpts.open:function(){console.log($(this))}});
							// {'Open':function(menuItem,menu) { console.log($(this)); } }, $.contextMenu.separator, {'Option 2':function(menuItem,menu) { alert("You clicked Option 2!"); } }

			// TODO
			// if(hotelBookingDetails[i].room_number == 'E4DLX609')
			// 	console.log(hotelBookingDetails[i].booking_status+":"+ hotelBookingDetails[i].room_number +" ==>"+ rightClickOpts);

			// Highlighting 
			var _isHighBal = (hotelBookingDetails[i].is_high_bal)?'2px solid #FF0000':'none';
			var _isHighBalShadowClass = (hotelBookingDetails[i].is_high_bal)?'bc-shadow-highlight':'';
			
			var booking_action = '<a href="#" onclick=\"'+hotelBookingDetails[i].hotel_booking_information.booking_action+'\" style="font-weight:bold;" >'+hotelBookingDetails[i].hotel_booking_information.booking_number+'</a>';
			var booking_dates = {check_in_date:hotelBookingDetails[i].hotel_booking_information.arrival_date_time,check_out_date:hotelBookingDetails[i].hotel_booking_information.departure_date_time}
			var booking_dates_for_action = {check_in_date:hotelBookingDetails[i].hotel_booking_information.checkin_time,check_out_date:hotelBookingDetails[i].hotel_booking_information.checkin_time}
			// <a href="#" onclick=\"'+details.hotel_booking_information.booking_action+'\" style="font-weight:bold;" >Booking Details</a>
			var is_booked;
			if($('#room-'+room_nos[i]+'-'+_cit).children().hasClass('hs-book-room') ){

				var prev_content = $('#room-'+room_nos[i]+'-'+_cit).children().attr('data-content');
				var prev_actions = $('#room-'+room_nos[i]+'-'+_cit).children().attr('data-booking-action').split("::");
				var prev_dates = $('#room-'+room_nos[i]+'-'+_cit).children().attr('data-booking-dates').split("::");
				is_booked = $('#room-'+room_nos[i]+'-'+_cit).children().attr('data-is-booked').split("::");

				var _cIN = $('#room-'+room_nos[i]+'-'+_cit).children().attr('data-booking-checkin').split(" ");
				var _cOUT = $('#room-'+room_nos[i]+'-'+_cit).children().attr('data-booking-checkout').split(" ");
				var check_in_date = new Date(_cIN[0]);
				var check_out_date = new Date(_cOUT[0]);

				is_booked.push(hotelBookingDetails[i].booking_status);
				prev_dates.push(JSON.stringify(booking_dates));
				prev_actions.push(booking_action);

				var _re = "";
				$.each(prev_actions,function(s,action){
					var b_dates = JSON.parse(prev_dates[s]);
					_re += "<div class='page-header'>"+
								"<div> <span style='float:left;'>Booking ID: </span>&nbsp;<span style='float:center;font-weight:bold;'>"+action+"</span></div>"+
								"<div> <span style='float:left;'>Check-In: </span>&nbsp;<span style='float:center;font-weight:bold;'>"+b_dates.check_in_date+"</span></div>"+
								"<div> <span style='float:left;'>Check-Out: </span>&nbsp;<span style='float:center;font-weight:bold;'>"+b_dates.check_out_date+"</span></div>"+
						   "</div>";
				});
				// callBackFunctions($(this),key);

				updateStatusClass($('#room-'+room_nos[i]+'-'+_cit).children());
				
				if($.inArray( "BKD", $.unique(is_booked) ) != -1){

					var menuItemsBKD = getMenuItemsForEvents($('#room-'+room_nos[i]+'-'+_cit),hotelBookingDetails[i],isCurrentDate(cit));

					$('#room-'+room_nos[i]+'-'+_cit).children().addClass('rc-m-booked').css({'background':calendar.options.legendColors.booked,'position':'relative'});
					// $('#room-'+room_nos[i]+'-'+_cit).children().attr({'data-b-details':bookingDetails});
					$('#room-'+room_nos[i]+'-'+_cit).children().attr({'data-hb-x-path-id':hotelBookingDetails[i].hotel_booking_information.hb_x_path_id});

					$('#room-'+room_nos[i]+'-'+_cit).children()
					.removeAttr('data-right-clicks')
					.attr({'data-right-clicks':JSON.stringify(menuItemsBKD)})
					// .off('contextmenu')
					.on('contextmenu', function(e){
						e.preventDefault();
						e.stopPropagation();
						// Your code.
						$(this).popover('hide');

						rightCLicksForMul($(this),menuItemsBKD,e);			  
					})
					// .contextMenu(menuItemsBKD,{theme:'vista'});

					// $('#room-'+room_nos[i]+'-'+_cit).children().html('<span style="position: relative; font-size: 12px; top: 16%;cursor: default;">BKD</span>')
				}else if( $.inArray( "RES", $.unique(is_booked) ) != -1 ) {

					var menuItemsRES = getMenuItemsForEvents($('#room-'+room_nos[i]+'-'+_cit),hotelBookingDetails[i],isCurrentDate(cit));

					$('#room-'+room_nos[i]+'-'+_cit).children().addClass('rc-m-reserve').css({'background':calendar.options.legendColors.reserved,'position':'relative'});
					// $('#room-'+room_nos[i]+'-'+_cit).children().attr({'data-b-details':bookingDetails});
					$('#room-'+room_nos[i]+'-'+_cit).children().attr({'data-hb-x-path-id':hotelBookingDetails[i].hotel_booking_information.hb_x_path_id});

					$('#room-'+room_nos[i]+'-'+_cit).children()
					.removeAttr('data-right-clicks')
					.attr({'data-right-clicks':JSON.stringify(menuItemsRES)})
					// .off('contextmenu')
					
					$('#room-'+room_nos[i]+'-'+_cit).children()
					.on('contextmenu', function(e){
						e.preventDefault();
						e.stopPropagation();
						// Your code.
						$(this).popover('hide');
						// rightClickInit($(this),e);
						rightCLicksForMul($(this),menuItemsRES,e);
					})
					// .contextMenu(menuItemsRES,{theme:'vista'});

					// $('#room-'+room_nos[i]+'-'+_cit).children().html('<span style="position: relative; font-size: 12px; top: 16%;cursor: default;">RES</span>')

				}else{
					var menuItemsCOT = getMenuItemsForEvents($('#room-'+room_nos[i]+'-'+_cit),hotelBookingDetails[i],isCurrentDate(cit));

					$('#room-'+room_nos[i]+'-'+_cit).children().addClass('rc-m-checkout').css({'background':calendar.options.legendColors.checkout,'position':'relative'});
					// $('#room-'+room_nos[i]+'-'+_cit).children().attr({'data-b-details':bookingDetails});
					$('#room-'+room_nos[i]+'-'+_cit).children().attr({'data-hb-x-path-id':hotelBookingDetails[i].hotel_booking_information.hb_x_path_id});


					$('#room-'+room_nos[i]+'-'+_cit).children()
					.removeAttr('data-right-clicks')
					.attr({'data-right-clicks':JSON.stringify(menuItemsCOT)})
					// .off('contextmenu')
					.on('contextmenu', function(e){
						e.preventDefault();
						e.stopPropagation();
						// Your code.
						$(this).popover('hide');

						rightCLicksForMul($(this),menuItemsCOT,e);			  
					})
					// .contextMenu(menuItemsCOT,{theme:'vista'});

					// $('#room-'+room_nos[i]+'-'+_cit).children().html('<span style="position: relative; font-size: 12px; top: 16%;cursor: default;">COT</span>')
				}


				// TODO -- Just patch we have to check in all cased its working or wat
				$('#room-'+room_nos[i]+'-'+_cit).children().css({'width':width+'px'});

				$('#room-'+room_nos[i]+'-'+_cit).children().attr({'data-content':_re});
				// console.log("_re : " + _re);
				$('#room-'+room_nos[i]+'-'+_cit).children().attr({'data-booking-action':prev_actions.join('::')});
				$('#room-'+room_nos[i]+'-'+_cit).children().attr({'data-booking-dates':prev_dates.join('::')});
				$('#room-'+room_nos[i]+'-'+_cit).children().attr({'data-is-booked':is_booked.join('::')});

				// Check IN dates
				$('#room-'+room_nos[i]+'-'+_cit).children().attr({'data-booking-checkin':hotelBookingDetails[i].hotel_booking_information.checkin_time});
				$('#room-'+room_nos[i]+'-'+_cit).children().attr({'data-booking-checkout':hotelBookingDetails[i].hotel_booking_information.checkout_time});

			}else{
				if( isCurrentDate(cot) && hotelBookingDetails[i].booking_status == "COT" && hotelBookingDetails[i].hk_status != "DRT" ){
					// It means Available 
				}else{
					var gridHtmlStr = ""
					// var gridPanelStyle = ""
					var gridPanelClass = ""
					if (viewName == 'gday') {
						gridHtmlStr = hotelBookingDetails[i].room_number
						gridHtmlStr += "<br/ >" + hotelBookingDetails[i].hotel_booking_information.booking_number
						// gridPanelStyle = {'width':'100px','height':'50px','border':'1px solid #ccc', 'float':'left', 'margin':'5px', 'padding':'5px'}
						gridPanelClass = "hs-grid-box"
					}
						
					$('#room-'+room_nos[i]+'-'+_cit)
						// .css(gridPanelStyle)
						.html(
						$("<div/>").css({'width':width+'px','height':'100%','background':statusColor,'position':'relative'})//.html('<span style="position: relative; font-size: 12px; top: 16%;cursor: default;">'+room_status.booking_status+'<span>')
							.addClass('hs-booking-details hs-book-room rc-b-class '+_isHighBalShadowClass+' '+statusClass)
							.css({'border':_isHighBal})
							.attr({'id':'rc-'+room_nos[i]+'-'+_cit})
							// rel="popover"
							// .attr({'rel':'popover','id':'example-'+room_status.hotel_booking_information.hotel_booking_id})
							// TODO--
							.attr({'data-index':''+i})
							.attr({'data-placement':'left'})
							.attr({'data-html':true,'data-trigger':'hover','data-delay':'1500'})
							// .attr({'data-content':detailsPopover(hotelBookingDetails[i])})
							// .attr({'data-content':"<b>.</b>"}) // TODO: For some reasom this is needed, else events are not registered! CHECK n FIX
							.attr({'data-booking-action':booking_action})
							.attr({'data-booking-dates':JSON.stringify(booking_dates)})
							.attr({'data-booking-checkin':hotelBookingDetails[i].hotel_booking_information.checkin_time}) 
							.attr({'data-booking-checkout':hotelBookingDetails[i].hotel_booking_information.checkout_time}) 
							.attr({'data-is-booked':hotelBookingDetails[i].booking_status})
							// .attr({'data-b-details':bookingDetails})
							.attr({'data-hb-x-path-id':hotelBookingDetails[i].hotel_booking_information.hb_x_path_id})
							.attr({'data-container':'body'})
							.attr({'data-right-clicks':JSON.stringify(menuItems)})
							.mouseover(function() {
								if (!$(this).attr('data-content')) {
									$(this).attr({'data-content':detailsPopover(hotelBookingDetails[$(this).attr("data-index")])});
									$(this).popover({
										delay: { show: 100, hide: 1300 }
									})
									.popover('show');

									// console.log("mouseover 3: " + $(this).attr("data-index") + " : " + hotelBookingDetails[$(this).attr("data-index")]);
								}
							})
							// .popover({
							// 	delay: { show: 100, hide: 1300 }
							// })
							// .popover('show')
							// .on('show.bs.popover', function () {
							// 	console.log("show.bs.popover 3");
							// 	$(this).attr({'data-content':detailsPopover(hotelBookingDetails[i])});
							// })
							.on('contextmenu', function(e){
							  e.preventDefault();
							  // Your code.
							  $(this).popover('hide');

							  rightClickInit($(this),e);			  
							})
							.html(gridHtmlStr)
							
							// .contextMenu(menuItems,{theme:'vista'})
							// .append(bookingDetails(room_status))
					).removeClass('drop')
				}
				// Check wether is current date checkout or not
				is_cot = ( room_status.booking_status === 'COT' && isCurrentDate(cot) )? 1 : 0;
				disableDragAndDrop($('#room-'+room_nos[i]+'-'+_cit),(diffDays-is_cot));

			}
			

			// Call contextMenu PlugIn
			// console.log("ANIL======>"+"#rc-"+room_nos[i]+"-"+_cit +" ================>"+room_status.room_number);
			// console.log(rightClickOpts);
			// console.log("ANIL");
			
		} // End Booking Details Loop
	}

	function rightCLicksForMul(ele,menuItems,e){
		// Locals
		var menuDiv = $("#rightOptions");
		var menuDivBody = $("#rightOptions").find('td');
		var x=e.pageX, y=e.pageY;
		var contextDiv = $('<div class="'+calendar.options.rightClick.className+'" data-toggle="context-menu"></div>');

		if(ele.parent('td').length > 0){
			ele.parent('td').off('contextmenu');
		}

		for (var i=0; i<menuItems.length; i++) {
        	var m = menuItems[i];
        	// if (m==$.contextMenu.separator) {
         //  		$div.append(cmenu.createSeparator());
        	// }
        	// else {
          		for (var opt in menuItems[i]) {
            		contextDiv.append(createMenuItem(opt,menuItems[i][opt],ele)); // Extracted to method for extensibility
          		}
        	// }
      	}

		menuDiv.html(contextDiv);

		// Psition Setting up
		x+=calendar.options.rightClick.offsetX; y+=calendar.options.rightClick.offsetY;
		var pos = getPosition(x,y,ele,e); // Extracted to method for extensibility


		menuDiv.css( {top:pos.y+"px", left:pos.x+"px", position:"absolute",zIndex:100010} );    
		menuDiv.show();
		$('body,.calendar').on('click',null,function(e){
			if ($(e.target).data('toggle') !== 'context-menu'
            	&& $(e.target).parents('table').length === 0) { 
              	$('[data-toggle="context-menu"]').hide();
        	}
		}); // Handle a single click to the document to hide the menu

	}

	function rightClickInit(ele,e){
		// Locals
		// For right click closing already opened dialog form 
		$("#dialog").dialog("close");
        emptyFormValues();
		var menuDiv = $("#rightOptions");
		var menuDivBody = $("#rightOptions").find('td');
		var x=e.pageX, y=e.pageY;
		var contextDiv = $('<div class="'+calendar.options.rightClick.className+'" data-toggle="context-menu"></div>');

		var menuItems = ele.data('right-clicks');

		// console.log(ele);
		if (viewName == 'gday') {
			if(ele.parent('div').length > 0){
				ele.parent('div').off('contextmenu');
			}			
		} else {
			if(ele.parent('td').length > 0){
				ele.parent('td').off('contextmenu');
			}
		}
		

		for (var i=0; i<menuItems.length; i++) {
        	var m = menuItems[i];
        	// if (m==$.contextMenu.separator) {
         //  		$div.append(cmenu.createSeparator());
        	// }
        	// else {
          		for (var opt in menuItems[i]) {
            		contextDiv.append(createMenuItem(opt,menuItems[i][opt],ele)); // Extracted to method for extensibility
          		}
        	// }
      	}

		menuDiv.html(contextDiv);

		// Psition Setting up
		x+=calendar.options.rightClick.offsetX; y+=calendar.options.rightClick.offsetY;
		var pos = getPosition(x,y,ele,e); // Extracted to method for extensibility


		menuDiv.css( {top:pos.y+"px", left:pos.x+"px", position:"absolute",zIndex:100010} );    
		menuDiv.show();
		$('body,.calendar').on('click',null,function(e){
			if ($(e.target).data('toggle') !== 'context-menu'
            	&& $(e.target).parents('table').length === 0) { 
              	$('[data-toggle="context-menu"]').hide();
        	}
		}); // Handle a single click to the document to hide the menu

	}

	function nightAuditMessage(ele,e){

		var menuDiv = $(".calendar-night-audit-msg");

		var x=e.pageX, y=e.pageY;
		var contextDiv = $('<div class="'+calendar.options.rightClick.className+'" style="min-width:360px;padding:10px;" data-toggle="context-menu"></div>');

		(
			$('<div/>')
				.append(
					$('<div/>')
						.addClass('col-lg-10 col-md-10 col-sm-10 col-xs-10')
						.text('Your business date is not matching your current date. Please perform Night Audit if required.')
						.css({
							'border-right': '1px solid #d4d4d4'
						})
				)
				.append(
					$('<div/>')
						.addClass('col-md-2 col-lg-2 center')
						.css({
							'cursor':'pointer',
							'font-size':'28px',
							'color':'#999',
							'font-size': '36px'
						})
						.hover(function(){
								$(this).css({
									'color':'#444'
								})
							},
							function(){
								$(this).css({
									'color':'#999'
								})
						})
						.html('&times')
						.click(function(){
							menuDiv.addClass('hidden');
						})

				)
				.addClass('clearfix')
		).appendTo(contextDiv);
		

		// Psition Setting up
		x+=calendar.options.rightClick.offsetX; y+=calendar.options.rightClick.offsetY;
		var pos = getPosition(x,y,ele,e); // Extracted to method for extensibility

		menuDiv.html(contextDiv);

		menuDiv.css( {top:pos.y+"px", left:pos.x+"px", position:"absolute",zIndex:100010} );
		menuDiv.removeClass('hidden');
		$('body,.calendar').on('click',null,function(e){
			if ($(e.target).data('toggle') !== 'context-menu'
            	&& $(e.target).parents('table').length === 0) { 
              	$('[data-toggle="context-menu"]').hide();
              	$('[data-toggle="context-menu"]').addClass('hidden');
              	menuDiv.addClass('hidden');
        	}
		}); // Handle a single click to the document to hide the menu
	}

	function createMenuItem(label,obj,ele){

	  if (typeof obj=="function") { obj={onclick:obj}; } // If passed a simple function, turn it into a property of an object
      // Default properties, extended in case properties are passed
      var o = $.extend({
        onclick:function() { },
        className:'',
        hoverClassName:calendar.options.rightClick.itemHoverClassName,
        icon:'',
        disabled:false,
        title:'',
        hoverItem: hoverItem,
        hoverItemOut: hoverItemOut
      },obj);

      // console.log(o);

      // If an icon is specified, hard-code the background-image style. Themes that don't show images should take this into account in their CSS
      var iconStyle = (o.icon)?'background-image:url('+o.icon+');':'';
      var $div = $('<div class="'+calendar.options.rightClick.itemClassName+' '+o.className+((o.disabled)?' '+calendar.options.rightClick.disabledItemClassName:'')+'" title="'+o.title+'"></div>')
              // If the item is disabled, don't do anything when it is clicked
              // o.onclick.call(ele,this,calendar,e)
              .click(function(e){if(isItemDisabled(this)){return false;}else{return callRightActions(ele,this,o.key,o.disabled,e) }})
              // Change the class of the item when hovered over
              .hover( function(){ o.hoverItem.call(this,(isItemDisabled(this))?calendar.options.rightClick.disabledItemHoverClassName:o.hoverClassName); }
                  ,function(){ o.hoverItemOut.call(this,(isItemDisabled(this))?calendar.options.rightClick.disabledItemHoverClassName:o.hoverClassName); }
              );
      var $idiv = $('<div class="'+calendar.options.rightClick.innerDivClassName+'" style="'+iconStyle+'">'+label+'</div>');
      $div.append($idiv);
      return $div;
	}

	function getPosition(clickX,clickY,ele,e) {
      var x = clickX+calendar.options.rightClick.offsetX;
      var y = clickY+calendar.options.rightClick.offsetY;
      var h = ele.height();
      var w = ele.width();
      var dir = calendar.options.rightClick.direction;
      if (calendar.options.rightClick.constrainToScreen) {
        var $w = $(window);
        var wh = $w.height();
        var ww = $w.width();
        if (dir=="down" && (y+h-$w.scrollTop() > wh)) { dir = "up"; }
        var maxRight = x+w-$w.scrollLeft();
        if (maxRight > ww) { x -= (maxRight-ww); }
        if((x + w + 100) > ww){
        	x = x - (w + 100);   
        }
      }
      if (dir=="up") { y -= h; }
      return {'x':x,'y':y};
    }

    function isItemDisabled(item){ return $(item).is('.'+this.disabledItemClassName); }
    function hoverItem(c) { $(this).addClass(c); }
    function hoverItemOut(c) { $(this).removeClass(c); }

	function getMenuItemsForEvents(ele,hotelBookingDetails,currentFlag){
		var opts = [];

		// {'Open':function(menuItem,menu) { console.log($(this)); } }, $.contextMenu.separator, {'Option 2':function(menuItem,menu) { alert("You clicked Option 2!"); } }
		switch (hotelBookingDetails.booking_status) {
		    case "RES":
		    	// Check current Day
		    	// opts.push({'Open':function(){ callBackFunctions($(this),'open') } })
		    	opts.push({'Open':{onclick:function(){ callBackFunctions($(this),'open')}, disabled:false, key: 'open' } })

		    	opts.push({'Check-In':{onclick:function(){ callBackFunctions($(this),'checkIn')}, disabled:!(currentFlag && hotelBookingDetails.hotel_booking_information.can_do_check_in_action), key: 'checkIn' } })
		    	
		    	opts.push({'No Show':{onclick:function(){ callBackFunctions($(this),'noShow')}, disabled:!hotelBookingDetails.hotel_booking_information.can_do_no_show, key: 'noShow' } })
		    	opts.push({'Cancel Reservation':{onclick:function(){ callBackFunctions($(this),'cancelReservation')}, disabled:!hotelBookingDetails.hotel_booking_information.can_do_cancel_action, key: 'cancelReservation' } })
		    	opts.push({'Make Payments':{onclick:function(){ callBackFunctions($(this),'advancePayments')}, disabled:!hotelBookingDetails.hotel_booking_information.is_allow_advance_payment, key: 'advancePayments' } })
		        break;
		    case "BKD":
				// Check In
				opts.push({'Open':{onclick:function(){ callBackFunctions($(this),'open')}, disabled:false, key: 'open' } })
		    	opts.push({'Check-Out':{onclick:function(){ callBackFunctions($(this),'checkOut')}, disabled:!hotelBookingDetails.hotel_booking_information.can_check_out_action, key: 'checkOut' } })
		    	opts.push({'Stay Extend':{onclick:function(){ callBackFunctions($(this),'stayExtend')}, disabled:!hotelBookingDetails.hotel_booking_information.can_extend_room, key: 'stayExtend' } })
		    	opts.push({'Room Swap':{onclick:function(){ callBackFunctions($(this),'roomSwap')}, disabled:!hotelBookingDetails.hotel_booking_information.can_do_swap_action, key: 'roomSwap' } })
		    	opts.push({'Room Service':{onclick:function(){ callBackFunctions($(this),'roomService')}, disabled:!hotelBookingDetails.hotel_booking_information.is_allow_room_service, key: 'roomService' } })
		    	opts.push({'Make Payments':{onclick:function(){ callBackFunctions($(this),'advancePayments')}, disabled:!hotelBookingDetails.hotel_booking_information.is_allow_advance_payment, key: 'advancePayments' } })
		        break;
		    case "VOD":
				// Check In
				opts.push({'Open':{onclick:function(){ callBackFunctions($(this),'open')}, disabled:false, key: 'open' } })
		        break;
		    case "DRT":
		        // opts = rightClickOptionsStatusWise['dirty'];
		        break;
		    case "COT":
		    	// Check In
				// opts.push({'Open':function(){ callBackFunctions($(this),'open') } })
				opts.push({'Open':{onclick:function(){ callBackFunctions($(this),'open')}, disabled:false, key: 'open' } })
		    	opts.push({'Room Service':{onclick:function(){ callBackFunctions($(this),'roomService')}, disabled:(!hotelBookingDetails.hotel_booking_information.is_allow_room_service || true), key: 'roomService' } })
		    	opts.push({'Make Payments':{onclick:function(){ callBackFunctions($(this),'advancePayments')}, disabled:!hotelBookingDetails.hotel_booking_information.is_allow_advance_payment, key: 'advancePayments' } })
		    	if(currentFlag){
		    		var checkFlag = true;
		    		if (ele.data('rc-room-type') !== undefined && ele.data('rc-room-type').length !== 0 ) {
		    			checkFlag = !ele.data('rc-room-type').is_dorm_bulk_booking;
		    		}
		    		if(checkFlag)
		    		opts.push({'Update HK Status':{onclick:function(){ callBackFunctions($(this),'updateHK-status')}, disabled:false, key: 'updateHK-status' } });
		    	}
		    	
		    	if(!ele.hasClass('diagonal_lines_pattern') ){
		    		var checkFlag = true;
		    		if (ele.data('rc-room-type') !== undefined && ele.data('rc-room-type').length !== 0 ) {
		    			checkFlag = !ele.data('rc-room-type').is_dorm_bulk_booking;
		    		}
		    		if(checkFlag)
		    		opts.push({'Block for maintenance':{onclick:function(){ var drEle = $(this).parent(); /*var drmFalg = $(this).parent().data('rc-room-type').is_dorm_bulk_booking; */  callBackFunctions(drEle,'updateHK-mtn')}, disabled:false, key: 'updateHK-mtn' } })
		    	}
		        break;
		      case 'TEMP':
  					opts.push({'Open':{onclick:function(){ callBackFunctions($(this),'open')}, disabled:false, key: 'open' } })
  					opts.push({'Confirm Booking':{onclick:function(){ callBackFunctions($(this),'temp_confirm_booking')}, disabled:false, key: 'temp_confirm_booking' } })
  					opts.push({'Release Booking':{onclick:function(){ callBackFunctions($(this),'temp_release_booking')}, disabled:false, key: 'temp_release_booking' } })
  					opts.push({'Extend Booking':{onclick:function(){ callBackFunctions($(this),'temp_extend_booking')}, disabled:false, key: 'temp_extend_booking' } })
  					break;

		    // default:

		}

		return opts;
	}

	function callRightActions(element,obj,keyValue,disabledFlag,e){
		if(!disabledFlag){
			callBackFunctions(element,keyValue);
			$('[data-toggle="context-menu"]').hide();
		}else{
			return false;	
		}
	}

	function getMenuItemsForEvents1(ele,hotelBookingDetails,currentFlag){
		var opts = [];

		// {'Open':function(menuItem,menu) { console.log($(this)); } }, $.contextMenu.separator, {'Option 2':function(menuItem,menu) { alert("You clicked Option 2!"); } }
		switch (hotelBookingDetails.booking_status) {
		    case "RES":
		    	// Check current Day
		    	opts.push({'Open':function(){ callBackFunctions($(this),'open') } })
		    	if(currentFlag && hotelBookingDetails.hotel_booking_information.can_do_check_in_action){
		    		opts.push({'Check-In':{onclick:function(){ callBackFunctions($(this),'checkIn')} } })
				}
		    	if(hotelBookingDetails.hotel_booking_information.can_do_check_in_action) opts.push({'No Show':{onclick:function(){ callBackFunctions($(this),'noShow')} } })
		    	if(hotelBookingDetails.hotel_booking_information.can_do_cancel_action) opts.push({'Cancel Reservation':{onclick:function(){ callBackFunctions($(this),'cancelReservation')} } })
		    	if(hotelBookingDetails.hotel_booking_information.is_allow_advance_payment) opts.push({'Make Payments':{onclick:function(){ callBackFunctions($(this),'advancePayments')} } })
		        break;
		    case "BKD":
				// Check In
				opts.push({'Open':function(){ callBackFunctions($(this),'open') } })
				if(hotelBookingDetails.hotel_booking_information.can_check_out_action) opts.push({'Check-Out':{onclick:function(){ callBackFunctions($(this),'checkOut')} } })
				if(hotelBookingDetails.hotel_booking_information.can_extend_room) opts.push({'Stay Extend':{onclick:function(){ callBackFunctions($(this),'stayExtend')} } })
		    	if(hotelBookingDetails.hotel_booking_information.can_do_swap_action) opts.push({'Room Swap':{onclick:function(){ callBackFunctions($(this),'roomSwap')} } })
		    	if(hotelBookingDetails.hotel_booking_information.is_allow_room_service) opts.push({'Room Service':{onclick:function(){ callBackFunctions($(this),'roomService')} } })
		    	if(hotelBookingDetails.hotel_booking_information.is_allow_advance_payment) opts.push({'Make Payments':{onclick:function(){ callBackFunctions($(this),'advancePayments')} } })
		        break;
		    case "DRT":
		        // opts = rightClickOptionsStatusWise['dirty'];
		        break;
		    case "COT":
		    	// Check In
				opts.push({'Open':function(){ callBackFunctions($(this),'open') } })
		    	if(hotelBookingDetails.hotel_booking_information.is_allow_room_service) opts.push({'Room Service':{onclick:function(){ callBackFunctions($(this),'roomService')} } })
		    	if(hotelBookingDetails.hotel_booking_information.is_allow_advance_payment) opts.push({'Make Payments':{onclick:function(){ callBackFunctions($(this),'advancePayments')} } })
		        break;
		    case "VOD":
		    	// Check In
				opts.push({'Open':function(){ callBackFunctions($(this),'open') } })
		    	if(hotelBookingDetails.hotel_booking_information.is_allow_room_service) opts.push({'Room Service':{onclick:function(){ callBackFunctions($(this),'roomService')} } })
		    	if(hotelBookingDetails.hotel_booking_information.is_allow_advance_payment) opts.push({'Make Payments':{onclick:function(){ callBackFunctions($(this),'advancePayments')} } })
		        break;
		   
		    // default:

		}

		return opts;
	}

	function getMenuItems(v,drtFlag,drmFlag,roomType){
		var opts = [];
		// Current Day Status
		if(v.isCurrent == 'future' || v.isCurrent == 'current' || v.isCurrent == 'business-date'){
			if((v.isCurrent == 'current' && !drtFlag) || ( isCurrentDate(calendar.options.business_date) && !drtFlag && v.isCurrent == 'business-date' ) ){
		    	opts.push({'Check-In':{onclick:function(){ callBackFunctions($(this),'checkIn')}, disabled:!roomType.can_do_booking_actions, key: 'checkIn' } });
			}
		    opts.push({'Reserve':{onclick:function(){ callBackFunctions($(this),'reserve')}, disabled:!roomType.can_do_booking_actions, key: 'reserve' } });
		    if(!drmFlag) if(v.isCurrent == 'current' || (v.isCurrent == 'business-date' && isCurrentDate(calendar.options.business_date) ) ) opts.push({'Update HK Status':{onclick:function(){ callBackFunctions($(this),'updateHK-status')}, disabled:false, key: 'updateHK-status' } });
		    if(!drmFlag) opts.push({'Block for maintenance':{onclick:function(){ var drEle = (drtFlag) ? $(this).parent() : $(this); callBackFunctions(drEle,'updateHK-mtn')}, disabled:false, key: 'updateHK-mtn' } })
		}else{
			opts.push('Past Dates');
		}

		return opts;
	}

	function getItemsForDay(drtFlag,currentFlag,roomType){
		var opts = [];
		// Options
		if(currentFlag && !drtFlag){
		    opts.push({'Check-In':{onclick:function(){ callBackFunctions($(this),'checkIn')}, disabled:!roomType.can_do_booking_actions, key: 'checkIn' } });
		}
		opts.push({'Reserve':{onclick:function(){ callBackFunctions($(this),'reserve')}, disabled:!roomType.can_do_booking_actions, key: 'reserve' } });
		if(currentFlag) opts.push({'Update HK Status':{onclick:function(){ callBackFunctions($(this),'updateHK-status')}, disabled:false, key: 'updateHK-status' } })
		opts.push({'Block for maintenance':{onclick:function(){ callBackFunctions($(this),'updateHK-mtn')}, disabled:false, key: 'updateHK-mtn' } })

		return opts;
	}

	function updateStatusClass(ele){
		if(ele.hasClass('rc-reserve'))
			ele.removeClass('rc-reserve');
		else if(ele.hasClass('rc-booked'))
			ele.removeClass('rc-booked');
		else if(ele.hasClass('rc-dirty'))
			ele.removeClass('rc-dirty');
		else if(ele.hasClass('rc-checkout'))
			ele.removeClass('rc-checkout');
		else if(ele.hasClass('rc-available'))
			ele.removeClass('rc-reserve');
		else if(ele.hasClass('rc-temp'))
			ele.removeClass('rc-temp');
		else if(ele.hasClass('rc-void_booking'))
			ele.removeClass('rc-void_booking');
		else if(ele.hasClass('rc-available-current'))
			ele.removeClass('rc-available-current');
	}

	function getRightOptions(hotelBookingDetails,pDate){
		var opts;

		switch (hotelBookingDetails.booking_status) {
		    case "RES":
		    	// Check current Day
		    	if(isCurrentDate(pDate)){
					opts = rightClickOptionsStatusWise['reserveCurrent'];
				}else{
					opts = rightClickOptionsStatusWise['reserve'];
				}
				// Check Actions
				opts = checkActions(opts,hotelBookingDetails);
		        break;
		    case "BKD":
				opts = rightClickOptionsStatusWise['checkIn'];
		    	// Check Actions
		    	opts = checkActions(opts,hotelBookingDetails);
		        break;
		    case "DRT":
		        opts = rightClickOptionsStatusWise['dirty'];
		        break;
		    case "COT":
		        opts = rightClickOptionsStatusWise['checkOut'];
		    	// Check Actions
		    	opts = checkActions(opts,hotelBookingDetails);
		        break;
		    case "VOD":
		        opts = rightClickOptionsStatusWise['void_booking'];
		    	// Check Actions
		        break;
		    default:
        		opts = rightClickOptionsStatusWise['available'];
		}

		return opts;
	}

	function getOptions(hotelBookingDetails,pDate){
		var opts = rightClickOptionsStatusWiseForEvents[hotelBookingDetails.booking_status];


		if(isCurrentDate(new Date(hotelBookingDetails.hotel_booking_information.checkin_time)) && hotelBookingDetails.booking_status == 'RES' ){
			// console.log(isCurrentDate(new Date(hotelBookingDetails.hotel_booking_information.checkin_time)))
			// console.log(hotelBookingDetails.booking_status+":"+hotelBookingDetails.hotel_booking_information.checkin_time+ "==>"  +JSON.stringify(rightClickOptionsStatusWiseForEvents[hotelBookingDetails.booking_status]));
			if(opts.checkIn != undefined) opts.checkIn.disabled=false;
		}

		// Can perform booking Actions
		if(!hotelBookingDetails.hotel_booking_information.can_perform_booking_action){
			if(opts.checkIn != undefined) opts.checkIn.disabled=true;
			if(opts.checkOut != undefined) opts.checkOut.disabled=true;
			if(opts.noShow != undefined) opts.noShow.disabled=true;
			if(opts.cancelReservation != undefined) opts.cancelReservation.disabled=true;
			if(opts.roomSwap != undefined) opts.roomSwap.disabled=true;
		}

		// Room Extend
		if(!hotelBookingDetails.hotel_booking_information.can_extend_room && opts.stayExtend != undefined)
			opts.stayExtend.disabled=true;
		// Room Swap
		if(!hotelBookingDetails.hotel_booking_information.is_allow_room_service && opts.roomService != undefined)
			opts.roomService.disabled=true;

		// Advance Payments
		if(!hotelBookingDetails.hotel_booking_information.is_allow_advance_payment && opts.advancePayments != undefined)
			opts.advancePayments.disabled=true;

		return opts;
	}

	function checkActions(opts,hotelBookingDetails){
		var thisopts = opts;
		// Can perform booking Actions
		if(!hotelBookingDetails.hotel_booking_information.can_perform_booking_action){
		   	delete thisopts["checkIn"];
			delete thisopts["checkOut"];
		   	delete thisopts["noShow"];
			delete thisopts["cancelReservation"];
		   	delete thisopts["roomSwap"];
		}

		// Room Extend
		if(!hotelBookingDetails.hotel_booking_information.can_extend_room)
			delete thisopts["stayExtend"];
		// Room Swap
		if(!hotelBookingDetails.hotel_booking_information.is_allow_room_service)
		    delete thisopts["roomService"];

		// Advance Payments
		if(!hotelBookingDetails.hotel_booking_information.is_allow_advance_payment)
			delete thisopts["advancePayments"];

		return opts;
	}

	function getStatusClass(status){
		var _class = ""
		switch (status) {
		    case "RES":
		        _class = "rc-reserve";
		        break;
		    case "BKD":
		        _class = "rc-booked";
		        break;
		    case "DRT":
		        _class = "rc-dirty";
		        break;
		    case "COT":
		        _class = "rc-checkout";
		        break;
		    case "VOD":
		        _class = "rc-void_booking";
		        break;
		    case "TEMP":
		        _class = "rc-temp";
		        break;
		    default:
        		_class = "rc-available";
		}
		return _class;
	}

	function _renderEventsForBulkBookings(bulkStartDate,bulkEndDate,bulkRoomTypes,bulkRooms,bulkBookingDetails,tdWidth){
		for(i=0;i<bulkRoomTypes.length;i++){

			var room_status = bulkBookingDetails[i];
			
			var cit = new Date(bulkStartDate[i]);
			var cot = new Date(bulkEndDate[i]);

			// While Check out time only
			// is_cot_width = ( room_status.booking_status === 'COT' && isCurrentDate(cot) ) ? tdWidth : 0;
			if(!room_status.hotel_booking_information.is_hourly_based){
				is_cot_width = ( room_status.booking_status === 'COT' && isCurrentDate(cot) && !isCurrentDate(cit) ) ? tdWidth : 0;
			}else{
				is_cot_width = 0;
			}

			if( (isDateBetweenTwoDates(cit) && isDateBetweenTwoDates(cot)) || ( isDatesEqual(cit,t.start) && isDatesEqual(cot,t.end) ) || ( isDatesEqual(cit,cot) ) ){
				var oneDay  = 24*60*60*1000;
				// Prev one. if you use no_of_days value it'll effect in calender width of booking while do early checkins
				// var diffDays = hotelBookingDetails[i].no_of_days; 
				
				// TODO -- needs to change the date caluculations
				var _arr_t = new Date((cit.getMonth()+1)+'/'+cit.getDate()+'/'+cit.getFullYear());
				var _dep_t = new Date((cot.getMonth()+1)+'/'+cot.getDate()+'/'+cot.getFullYear());
				
				// var diffDays = Math.abs((cot.getTime() - cit.getTime()) / oneDay);
				var diffDays = _arr_t.DaysBetween(_dep_t)
				
				if(parseInt(diffDays) === 0){
					diffDays = 1;
				}
				

				var _cit = getDateFormate(cit);
				var _cot = getDateFormate(cot);

			}else if( cit <= t.start && isDateBetweenTwoDates(cot) ) {
				

				// TODO -- needs to change the date caluculations
				var _arr_t = new Date((t.start.getMonth()+1)+'/'+t.start.getDate()+'/'+t.start.getFullYear());
				var _dep_t = new Date((cot.getMonth()+1)+'/'+cot.getDate()+'/'+cot.getFullYear());
				
				var oneDay  = 24*60*60*1000;
				// var diffDays = Math.abs((cot.getTime() - cit.getTime()) / oneDay);
				// var diffDays = _arr_t.DaysBetween(_dep_t)
				
				var diffDays = Math.abs((cot.getTime() - t.start.getTime()) / oneDay);

				if(parseInt(diffDays) === 0){
					diffDays = 1;
				}
				// diffDays = parseInt(diffDays) - parseInt(is_cot);
				// width = (tdWidth*parseInt(diffDays))+parseInt(parseInt(diffDays)/2);

				var _cit = getDateFormate(t.start);
				var _cot = getDateFormate(cot);
			}else if( isDateBetweenTwoDates(cit) && cot >= t.end ){
				// TODO -- needs to change the date caluculations
				var _arr_t = new Date((cit.getMonth()+1)+'/'+cit.getDate()+'/'+cit.getFullYear());
				var _dep_t = new Date((t.end.getMonth()+1)+'/'+t.end.getDate()+'/'+t.end.getFullYear());
				
				// var diffDays = Math.abs((cot.getTime() - cit.getTime()) / oneDay);
				// var diffDays = _arr_t.DaysBetween(_dep_t)
				
				// if(parseInt(diffDays) === 0){
				// 	diffDays = 1;
				// }

				var oneDay  = 24*60*60*1000;
				// TODO -- Last Day is fixed. for that purpose only wrote this condition
				// var diffDays = ( isDatesEqual(cot,t.end) ) ? 1 : Math.abs((t.end.getTime() - cit.getTime()) / oneDay);
				
				var diffDays = Math.abs((t.end.getTime() - cit.getTime()) / oneDay);

				if( !isDatesEqual(cot,t.end) && (cot > t.end) ){
					var diffDays = Math.abs((cot.getTime() - cit.getTime()) / oneDay);
				}

				if(parseInt(diffDays) === 0){
					diffDays = 1;
				}
				// diffDays = parseInt(diffDays) - parseInt(is_cot);
				// width = (tdWidth*parseInt(diffDays))+parseInt(parseInt(diffDays)/2);

				var _cit = getDateFormate(cit);
				var _cot = getDateFormate(t.end);
			}else if( isDatesEqual(cit,t.end) || isDatesEqual(cot,t.start) ){
				
				var oneDay  = 24*60*60*1000;
				
				var diffDays = 1;
				
				if(isDatesEqual(cit,t.end)){
					var _cit = getDateFormate(cit);
					var _cot = getDateFormate(t.end);
				}else if(isDatesEqual(cot,t.start)){
					var _cit = getDateFormate(t.start);
					var _cot = getDateFormate(cot);
				}
			}
			// Width of Booking Details div
			width = ((tdWidth*parseInt(diffDays))+parseInt(parseInt(diffDays)/2))// - (parseInt(is_cot_width));
			if (viewName == 'gday')
				width = '100'

			var statusColor = (bulkBookingDetails[i].booking_status === 'RES' )?calendar.options.legendColors.reserved : calendar.options.legendColors.booked;
			statusColor = (bulkBookingDetails[i].booking_status === 'COT' ) ? calendar.options.legendColors.checkout : statusColor;
			statusColor = (bulkBookingDetails[i].booking_status === 'VOD' ) ? calendar.options.legendColors. void_booking : statusColor;
			statusColor = (bulkBookingDetails[i].booking_status === 'TEMP' ) ? calendar.options.legendColors.temp_reserve : statusColor;
			

			if(isCurrentDate(cit) && bulkBookingDetails[i].booking_status === 'RES'){
				var statusClass = 'rc-reserve-current'
			}else{
				var statusClass = getStatusClass(bulkBookingDetails[i].booking_status);
			}

			// var rightClickOpts = getRightOptions(bulkBookingDetails[i],cit); // Right Click options
			var rightClickOpts = JSON.stringify(getOptions(bulkBookingDetails[i],cit));

			var bookingDetails = JSON.stringify(bulkBookingDetails[i]);

			var menuItems = getMenuItemsForEvents($('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit),bulkBookingDetails[i],isCurrentDate(cit));

			// Highlighting 
			var _isHighBal = (bulkBookingDetails[i].is_high_bal)?'2px solid #FF0000':'none';
			var _isHighBalShadowClass = (bulkBookingDetails[i].is_high_bal)?'bc-shadow-highlight':'';
			
			var booking_action = '<a href="#" onclick=\"'+bulkBookingDetails[i].hotel_booking_information.booking_action+'\" style="font-weight:bold;" >'+bulkBookingDetails[i].hotel_booking_information.booking_number+'</a>';
			var booking_dates = {check_in_date:bulkBookingDetails[i].hotel_booking_information.arrival_date_time,check_out_date:bulkBookingDetails[i].hotel_booking_information.departure_date_time}
			var booking_dates_for_action = {check_in_date:bulkBookingDetails[i].hotel_booking_information.checkin_time,check_out_date:bulkBookingDetails[i].hotel_booking_information.checkin_time}
			// <a href="#" onclick=\"'+details.hotel_booking_information.booking_action+'\" style="font-weight:bold;" >Booking Details</a>

			if ( $("#room-type-td-"+bulkRoomTypes[i]+"-"+_cit).children().hasClass('hs-book-room') ){

				var prev_content = $('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children().attr('data-content');
				var prev_actions = $('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children().attr('data-booking-action').split("::");
				var prev_dates = $('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children().attr('data-booking-dates').split("::");
				var is_booked = $('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children().attr('data-is-booked').split("::");

				var _cIN = $('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children().attr('data-booking-checkin').split(" ");
				var _cOUT = $('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children().attr('data-booking-checkout').split(" ");
				var check_in_date = new Date(_cIN[0]);
				var check_out_date = new Date(_cOUT[0]);

				is_booked.push(bulkBookingDetails[i].booking_status);
				prev_dates.push(JSON.stringify(booking_dates));
				prev_actions.push(booking_action);

				var _re = "";
				$.each(prev_actions,function(s,action){
					var b_dates = JSON.parse(prev_dates[s]);
					_re += "<div class='page-header'>"+
								"<div> <span style='float:left;'>Booking ID: </span>&nbsp;<span style='float:center;font-weight:bold;'>"+action+"</span></div>"+
								"<div> <span style='float:left;'>Check-In: </span>&nbsp;<span style='float:center;font-weight:bold;'>"+b_dates.check_in_date+"</span></div>"+
								"<div> <span style='float:left;'>Check-Out: </span>&nbsp;<span style='float:center;font-weight:bold;'>"+b_dates.check_out_date+"</span></div>"+
						   "</div>";
				});

				if($.inArray( "BKD", $.unique(is_booked) ) != -1){

					var menuItemsBKD = getMenuItemsForEvents($('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit),bulkBookingDetails[i],isCurrentDate(cit));

					$('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children().css({'background':calendar.options.legendColors.booked,'position':'relative'});
					// $('#room-'+room_nos[i]+'-'+_cit).children().html('<span style="position: relative; font-size: 12px; top: 16%;cursor: default;">BKD</span>')
					// $('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children().attr({'data-b-details':bookingDetails});
					$('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children().attr({'data-hb-x-path-id':bulkBookingDetails[i].hotel_booking_information.hb_x_path_id});
					$('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children().attr({'data-right-clicks':JSON.stringify(menuItemsBKD)})
					$('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children()
					.removeAttr('data-right-clicks')
					.attr({'data-right-clicks':JSON.stringify(menuItemsBKD)})
					.on('contextmenu', function(e){
						e.preventDefault();
							  // Your code.
						$(this).popover('hide');

						rightCLicksForMul($(this),menuItemsBKD,e);
					})
					// .contextMenu(menuItemsBKD,{theme:'vista'});

				}else if( $.inArray( "RES", $.unique(is_booked) ) != -1 ) {

					var menuItemsRES = getMenuItemsForEvents($('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit),bulkBookingDetails[i],isCurrentDate(cit));

					$('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children().css({'background':calendar.options.legendColors.reserved,'position':'relative'});
					// $('#room-'+room_nos[i]+'-'+_cit).children().html('<span style="position: relative; font-size: 12px; top: 16%;cursor: default;">RES</span>')
					// $('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children().attr({'data-b-details':bookingDetails});
					$('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children().attr({'data-hb-x-path-id':bulkBookingDetails[i].hotel_booking_information.hb_x_path_id});

					$('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children().attr({'data-right-clicks':JSON.stringify(menuItemsRES)})

					$('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children()
					.removeAttr('data-right-clicks')
					.attr({'data-right-clicks':JSON.stringify(menuItemsRES)})
					.on('contextmenu', function(e){
						e.preventDefault();
						// Your code.
						$(this).popover('hide');

						rightCLicksForMul($(this),menuItemsRES,e);	  
					})
					// .contextMenu(menuItemsRES,{theme:'vista'});
				}else{

					var menuItemsCOT = getMenuItemsForEvents($('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit),bulkBookingDetails[i],isCurrentDate(cit));

					$('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children().css({'background':calendar.options.legendColors.checkout,'position':'relative'});
					// $('#room-'+room_nos[i]+'-'+_cit).children().html('<span style="position: relative; font-size: 12px; top: 16%;cursor: default;">COT</span>')
					// $('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children().attr({'data-b-details':bookingDetails});
					$('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children().attr({'data-hb-x-path-id':bulkBookingDetails[i].hotel_booking_information.hb_x_path_id});

					$('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children().attr({'data-right-clicks':JSON.stringify(menuItemsCOT)})

					$('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children()
					.removeAttr('data-right-clicks')
					.attr({'data-right-clicks':JSON.stringify(menuItemsCOT)})
					.on('contextmenu', function(e){
						e.preventDefault();
						// Your code.
						$(this).popover('hide');

						rightCLicksForMul($(this),menuItemsCOT,e);	  	  
					})
					// .contextMenu(menuItemsCOT,{theme:'vista'});
				}

				// TODO -- Just patch we have to check in all cased its working or wat
				$('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children().css({'width':width+'px'});

				$('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children().attr({'data-content':_re});
				$('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children().attr({'data-booking-action':prev_actions.join('::')});
				$('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children().attr({'data-booking-dates':prev_dates.join('::')});
				$('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children().attr({'data-is-booked':is_booked.join('::')});

				// Check IN dates
				$('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children().attr({'data-booking-checkin':bulkBookingDetails[i].hotel_booking_information.checkin_time});
				$('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).children().attr({'data-booking-checkout':bulkBookingDetails[i].hotel_booking_information.checkout_time});

			}else{
				if( isCurrentDate(cot) && bulkBookingDetails[i].booking_status == "COT" && bulkBookingDetails[i].hk_status != "DRT" ){
					// It means Available 
				}else{
					$('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit).html(
						$("<div/>").css({'width':width+'px','height':'100%','background':statusColor,'position':'relative'})//.html('<span style="position: relative; font-size: 12px; top: 16%;cursor: default;">'+room_status.booking_status+'<span>')
							.addClass('hs-booking-details hs-book-room rc-b-class '+_isHighBalShadowClass+' '+statusClass)
							.css({'border':_isHighBal})
							.attr({'id':'rc-'+bulkRoomTypes[i]+'-'+_cit})
							// rel="popover"
							// .attr({'rel':'popover','id':'example-'+room_status.hotel_booking_information.hotel_booking_id})
							// TODO--
							.attr({'data-index':''+i})
							.attr({'data-placement':'left'})
							.attr({'data-html':true,'data-trigger':'hover','data-delay':'1500'})
							// .attr({'data-content':detailsPopover(bulkBookingDetails[i])})
							// .attr({'data-content':"<b>.</b>"}) // TODO: For some reasom this is needed, else events are not registered! CHECK n FIX
							.attr({'data-booking-action':booking_action})
							.attr({'data-booking-dates':JSON.stringify(booking_dates)})
							.attr({'data-booking-checkin':bulkBookingDetails[i].hotel_booking_information.checkin_time}) 
							.attr({'data-booking-checkout':bulkBookingDetails[i].hotel_booking_information.checkout_time}) 
							.attr({'data-is-booked':bulkBookingDetails[i].booking_status})
							// .attr({'data-b-details':bookingDetails})
							.attr({'data-hb-x-path-id':bulkBookingDetails[i].hotel_booking_information.hb_x_path_id})
							.attr({'data-container':'body'})
							.attr({'data-right-clicks':JSON.stringify(menuItems)})
							.mouseover(function() {
								if (!$(this).attr('data-content')) {
									$(this).attr({'data-content':detailsPopover(bulkBookingDetails[$(this).attr("data-index")])});
									$(this).popover({
										delay: { show: 100, hide: 1300 }
									})
									.popover('show');
								}
							})												
							// .popover({
							// 	delay: { show: 100, hide: 1300 }
							// })
							// .popover('show')
							.on('contextmenu', function(e){
							  e.preventDefault();
							  // Your code.
							  $(this).popover('hide');

							  rightClickInit($(this),e);			  
							})
							// .on('show.bs.popover', function () {
							// 	console.log("show.bs.popover 4");
							// 	$(this).attr({'data-content':detailsPopover(bulkBookingDetails[i])});
							// })
							
							// .contextMenu(menuItems,{theme:'vista'})

							// .append(bookingDetails(room_status))
					).removeClass('drop')
				}
				// Check wether is current date checkout or not
				is_cot = ( room_status.booking_status === 'COT' && isCurrentDate(cot) )? 1 : 0;
				disableDragAndDrop($('#room-type-td-'+bulkRoomTypes[i]+'-'+_cit),(diffDays-is_cot));


			}
			// callContextMenuPlugIn('#rc-'+bulkRoomTypes[i]+'-'+_cit,rightClickOpts);

		}

	}


	function _renderMTNEvents(mtnStartDate,mtnEndDate,mtnRoomNos,mtnStatus,tdWidth){
		if(mtnRoomNos.length){ // Condition Start
			// Show MTN Status Loop
			for(i=0;i<mtnRoomNos.length;i++){
				var st = new Date(mtnStartDate[i]);
				var et = new Date(mtnEndDate[i]);

				if( isDateBetweenTwoDates(st) && isDateBetweenTwoDates(et) || ( isDatesEqual(st,t.start) && isDatesEqual(et,t.end) ) || ( isDatesEqual(st,et) ) ){
					// width = (tdWidth*hotelBookingDetails[i].no_of_days)+parseInt(hotelBookingDetails[i].no_of_days/2);
					// width = tdWidth;
					
					// var oneDay  = 24*60*60*1000;
					// var diffDays = Math.abs((st.getTime() - et.getTime()) / oneDay);
					// width = (tdWidth*parseInt(diffDays))+parseInt(parseInt(diffDays)/2);


					// TODO -- needs to change the date caluculations
					var _arr_t = new Date((st.getMonth()+1)+'/'+st.getDate()+'/'+st.getFullYear());
					var _dep_t = new Date((et.getMonth()+1)+'/'+et.getDate()+'/'+et.getFullYear());
					
					// var diffDays = Math.abs((cot.getTime() - cit.getTime()) / oneDay);
					var diffDays = _arr_t.DaysBetween(_dep_t)
					
					if(parseInt(diffDays) === 0){
						diffDays = 1;
					}

					width = (tdWidth*parseInt(diffDays))+parseInt(parseInt(diffDays));

					var _st = getDateFormate(st);
					var _et = getDateFormate(et);

				}else if( st <= t.start && isDateBetweenTwoDates(et) ) {
					
					var oneDay  = 24*60*60*1000;
					var diffDays = Math.abs((et.getTime() - t.start.getTime()) / oneDay);
					width = (tdWidth*parseInt(diffDays))+parseInt(parseInt(diffDays));

					var _st = getDateFormate(t.start);
					var _et = getDateFormate(et);
				}else if( isDateBetweenTwoDates(st) && et >= t.end ){

					var oneDay  = 24*60*60*1000;
					var diffDays = Math.abs((t.end.getTime() - st.getTime()) / oneDay);
					width = (tdWidth*parseInt(diffDays))+parseInt(parseInt(diffDays))+(tdWidth*2);

					var _st = getDateFormate(st);
					var _et = getDateFormate(t.end);
				}else if( st <= t.start && et >= t.end ){
					var oneDay  = 24*60*60*1000;
					
					var diffDays = Math.abs((t.end.getTime() - t.start.getTime()) / oneDay);
					width = (tdWidth*parseInt(diffDays))+parseInt(parseInt(diffDays))+tdWidth;

					// console.log(et);

					var _st = getDateFormate(t.start);
					var _et = getDateFormate(t.end);

				}else if( isDatesEqual(st,t.end) || isDatesEqual(et,t.start) ){
					var diffDays = 1;

					width = (tdWidth*parseInt(diffDays))+parseInt(parseInt(diffDays));

					if(isDatesEqual(st,t.end)){
						var _st = getDateFormate(st);
						var _et = getDateFormate(t.end);
					}else if(isDatesEqual(et,t.start)){
						var _st = getDateFormate(t.start);
						var _et = getDateFormate(et);
					}
				}
				
				var gridHtmlStr = ""
				if (viewName == 'gday') {
					width = '100'
					gridHtmlStr = $('#room-'+mtnRoomNos[i]+'-'+_st).html();
				}
				
				if(mtnStatus[i] == "OWNC") {
					// menuItems = menu_items_for_own_consumption(_st);
					for (var d = st; d < et; d.setDate(d.getDate() + 1)) {
						_d = getDateFormate(d);
						$('#room-'+mtnRoomNos[i]+'-'+_d).html(
							$("<div/>").css({'width':tdWidth+'px','height':'100%','position':'relative','background':calendar.options.legendColors.own_consumption})
							.html(gridHtmlStr)
							// .attr({'data-right-clicks':JSON.stringify(menuItems)})
							// .on('contextmenu', function(e){
							// 	e.preventDefault();
							// 	// Your code.
							// 	$(this).popover('hide');
							// 	rightClickInit($(this),e);			  
							// })
						)
					}
				} 
				else {
					$('#room-'+mtnRoomNos[i]+'-'+_st).html(
						$("<div/>").css({'width':width+'px','height':'100%','position':'relative','background':calendar.options.legendColors.maintenance})
							.contextMenu('#mtnRightOptions')
							.html(gridHtmlStr)
							//.html('<span style="position: relative; font-size: 12px; top: 16%;cursor: default;color: #FFFFFF;">'+mtnStatus[i]+'</span>')
					)
					.removeClass('drop')

					disableDragAndDrop($('#room-'+mtnRoomNos[i]+'-'+_st),diffDays);
				}
			}// End MTN Status Loop
		} // Condition End

	}

	// function menu_items_for_own_consumption(start_date){
	// 	var opts = [];
	// 	st = new Date(start_date)
	// 	ct = new Date();
	// 	if(st.getDate() >= ct.getDate()){
	// 		if(st.getDate() == ct.getDate()){
	//     	opts.push({'Check-In':{onclick:function(){ callBackFunctions($(this),'checkIn')}, key: 'checkIn' } });
	// 		}
	//     opts.push({'Reserve':{onclick:function(){ callBackFunctions($(this),'reserve')}, key: 'reserve' } });
	// 	}else{
	// 		opts.push('Past Dates');
	// 	}

	// 	return opts;
	// }



	// Check date is between twodates
	function isDateBetweenTwoDates(_d){
		return (_d > t.start && _d < t.end)
	}

	// Get Hours
	function _getHours(_date){
		var _t = _date.split(' ');
		var _returnValue;
		if(_t[2]){
			var _tt = _t[1].split(':');
			_returnValue = (_t[2] === 'PM')?((parseInt(_tt[0])+12)+':'+_tt[1]):(_tt[0]+':'+_tt[1]);
		}else{
			_returnValue = _t[1];
		}

		return _returnValue;
	}
	function _getDate(_date){
		var _t = _date.split(' ');
		var _returnValue;

		_returnValue = _t[0];
		return _returnValue;
	}

	function isDatesEqual(presentDate,_date){
		return (
					(presentDate.getDate() === _date.getDate()) &&
					(presentDate.getMonth() === _date.getMonth()) &&
					(presentDate.getFullYear() === _date.getFullYear())
			   )
	}

	function isTimeEqual(t1,t2){
		return (t1 === t2);
	}

	function displayPopoverTotalStatus(ele){
		var dataTotal = $.parseJSON($(ele).attr('data-total')); // Occupied, Vacant, Dirty, Maintance
		var _re = "";
		_re += "<div style='font-size:11px;'>"+
					'<h5 class="nomargin bold" style="color:#AAAAAA;font-size: 12px;">Total Occupancy</h5>'+
					
					'<div class="row nomargin border-bottom" style="margin-top:5px">'+
						'<div class="col-md-10 nopadding">Occupied</div>'+
						'<div class="col-md-2 nopadding pull-right text-right">'+dataTotal.occupied+'</div>'+
					'</div>'+

					'<div class="row nomargin border-bottom">'+
						'<div class="col-md-10 nopadding">Vacant</div>'+
						'<div class="col-md-2 nopadding pull-right text-right">'+dataTotal.vacant+'</div>'+
					'</div>'+

					'<div class="row nomargin border-bottom">'+
						'<div class="col-md-10 nopadding">Dirty</div>'+
						'<div class="col-md-2 nopadding pull-right text-right">'+dataTotal.dirty+'</div>'+
					'</div>'+

					'<div class="row nomargin border-bottom">'+
						'<div class="col-md-10 nopadding">Maintenance</div>'+
						'<div class="col-md-2 nopadding pull-right text-right">'+dataTotal.maintenance+'</div>'+
					'</div>'+

					'<div class="row nomargin">'+
						'<div class="col-md-10 nopadding">Total Rooms</div>'+
						'<div class="col-md-2 nopadding pull-right text-right">'+totalRoomCount+'</div>'+
					'</div>'+
			   '</div>';
		return _re;
	}

	// // Disable Drag And Drop
	// function disableDragAndDrop(obj,details){
	// 	var cTd = obj;

	// 	var cDate = new Date();
	// 	var _cDate = (cDate.getMonth()+1)+'/'+cDate.getDate()+'/'+cDate.getFullYear();


	// 	for(i=0;i<details.no_of_days;i++){
	// 		var _date = cTd.attr('data-date');
	// 		if(i === (details.no_of_days-1)){
	// 			if(_date === _cDate && details.booking_status === 'COT'){
	// 			}else{
	// 				// Disable
	// 				cTd.off('click');
	// 				cTd.off('drop');
	// 				cTd.addClass('nonselect-item');
	// 			}
	// 		}else{
	// 			// Disable
	// 			cTd.off('click');
	// 			cTd.off('drop');
	// 			cTd.addClass('nonselect-item');
	// 		}
	// 		cTd = cTd.next();
	// 		// cTd.html('');
	// 	}
	// }

	function disableDragAndDrop(obj,_width){
		var s = 0;
		var cTd = obj
		
		while(s<parseInt(_width)){
			// if(isCot){
				cTd.find('div.hs-dirty-room').remove();
			// }
			cTd.off('click');
			cTd.off('drop');
			cTd.addClass('nonselect-item');

			if(s!=0)
				cTd.children().hide();
		
			cTd = cTd.next();
			s++;
		}
	}
	
	function getWidth(obj,details){
		var cTd = obj;
		var _width = 0;

		var cDate = new Date();
		var _cDate = (cDate.getMonth()+1)+'/'+cDate.getDate()+'/'+cDate.getFullYear();

		for(i=0;i<details.no_of_days;i++){
			var _date = cTd.attr('data-date');
			if(i === (details.no_of_days-1)){
				if(_date === _cDate && details.booking_status === 'COT'){
				}else{
					_width += cTd.width();
				}
			}else{
				_width += cTd.width();
			}
			cTd = cTd.next();
		}
		return _width;
	}

	function getWidthForDay(_cit,_cot,_presentDate,_date,_room_id,flag){
		var cit = new Date(_cit);
		var cot = new Date(_cot);
		var pD = new Date(_presentDate);
		var e_date = new Date(_date);
		var width;

		var tdWidth = $("#grid-container-table").find('tr:first').find('td:first').next().width();

		// console.log(_presentDate);

		var _checkinTime = _getHours(_cit);
		var _checkoutTime = _getHours(_cot);

		var checkinTime = _checkinTime.split(':');
		var checkoutTime = _checkoutTime.split(':');


		if( isDatesEqual(pD,cit) && isDatesEqual(pD,cot) && !flag){
			_width = parseInt(checkoutTime[0]) - parseInt(checkinTime[0]);
			width = (tdWidth*_width)+(_width/2);
		}else if( isCurrentDate(e_date) ){
			_width = 24-parseInt(checkoutTime[0]);
			width = (tdWidth*_width)+(_width/2);
		}else if(isDatesEqual(pD,cot) && flag){
			width = (tdWidth*cot.getHours());
		}else if(flag){
			width = (tdWidth*24)+24;
		}else{
			_width = 24-parseInt(checkinTime[0]);
			width = (tdWidth*_width)+(_width/2);
		}
		
		return width;
	}

	function getDateFormate(presentDate){
		return presentDate.getFullYear()+'-'+appendZero(presentDate.getMonth()+1)+'-'+appendZero(presentDate.getDate());
	}

	function displayRoomTypeLevelAvailability(room_type_id,date,availbleRooms){
		
		var crTd = $('#room-type-td-'+room_type_id+'-'+date)
		var totalRooms = parseInt($('#room-type-'+room_type_id+'-total-rooms').val());

		var availble_rooms = totalRooms - availbleRooms;

		crTd.html(
			$("<div/>").css({'height':'100%'})
				.append(function(){
					return colorBlendarFunctionality(availble_rooms,totalRooms);
				})
				.tooltip({
					placement: 'bottom',
					// container: "Hello",
					trigger: 'hover',
					title: calculateRoomPersentage(availble_rooms,totalRooms)+'%'
				})
		);
	}

	function displayRoomTypeLevelAvailabilityForDay(room_type_id,date,availbleRooms){
		// room-type-time-scale-1-2014-02-04-00

		var crTd = $('#room-type-time-scale-'+room_type_id+'-'+date+'-'+'00');
		var totalRooms = parseInt($('#room-type-'+room_type_id+'-total-rooms').val());

		var availble_rooms = totalRooms - availbleRooms;
		var width = (crTd.width()*24)+24;
		crTd.html(
			$("<div/>").css({'height':'100%','width':width})
				.append(function(){
					return colorBlendarFunctionality(availble_rooms,totalRooms);
				})
				.tooltip({
					placement: 'top',
					// container: "Hello",
					trigger: 'hover',
					title: calculateRoomPersentage(availble_rooms,totalRooms)+'%'
				})
				.append('<div style="position: relative; top: -22px;"> Occupancy :'+calculateRoomPersentage(availble_rooms,totalRooms)+'%</div>')
		);

	}

	// Booking Details
	function bookingDetails(details){

		var _d;

		_d = $("<div/>").addClass('row');

		_d.append(
			$("<div/>").addClass('col-md-6')
				.append($("<div/>").append('Booking ID <span>'+details.hotel_booking_information.booking_number+'</span>'))
		)
		.append(
			$("<div/>").addClass('col-md-6')
		)

		return $("<div>").addClass('bc-booking-d bc-shadow-black')
					.append(function(){
						var _re = '<div class="row">'+
							        '<div class="col-md-6">'+
							           '<div> <span style="float:left;">Booking ID: </span>&nbsp;<span style="float:center;font-weight:bold;"><a href="#" onclick=\"'+details.hotel_booking_information.booking_action+'\" >'+details.hotel_booking_information.booking_number+'</a></span> </div>'+
							           // '<div> <span style="float:left;">Booking ID: </span>&nbsp;<span style="float:center;font-weight:bold;">'+details.hotel_booking_information.booking_number+'</span> </div>'+
							           "<div> <span style='float:left;'>Roomnumber: </span>&nbsp;<span style='float:center;font-weight:bold;'>"+details.room_number+"</span> </div>"+
							           "<div> <span style='float:left;'>Check-in: </span>&nbsp;<span style='float:center;font-weight:bold;'>"+details.hotel_booking_information.arrival_date_time+"</span> </div>"+
							           "<div> <span style='float:left;'>Check-out: </span>&nbsp;<span style='float:center;font-weight:bold;'>"+details.hotel_booking_information.departure_date_time+"</span> </div>"+
							        "</div> "+
							       
							        "<div class='col-md-6'>"+
							           "<div> <span style='float:left;'>Guest: </span>&nbsp;<span style='float:center;font-weight:bold;'>"+details.hotel_booking_information.guest_name+"</span> </div>"+
							           "<div> <span style='float:left;'>No. of Adult(s): </span>&nbsp;<span style='float:center;font-weight:bold;'>"+details.hotel_booking_information.no_of_adults+"</span> </div>"+
							           "<div> <span style='float:left;'>Contact: </span>&nbsp;<span style='float:center;font-weight:bold;'>"+details.hotel_booking_information.contact_details+"</span> </div>"+
							        "</div>"+

							    "</div>"+
							    '<div><a href="#" onclick=\"'+details.hotel_booking_information.booking_action+'\" style="font-weight:bold;" >Booking Details</a></div>';
				    	return _re;
				    });
	}

	function detailsPopover(details){
		// var _re = '<div class="row">'+
		// 					        '<div class="col-md-6">'+
		// 					           '<div> <span style="float:left;">Booking ID: </span>&nbsp;<span style="float:center;font-weight:bold;"><a href="#" onclick=\"'+details.hotel_booking_information.booking_action+'\" >'+details.hotel_booking_information.booking_number+'</a></span> </div>'+
		// 					           // '<div> <span style="float:left;">Booking ID: </span>&nbsp;<span style="float:center;font-weight:bold;">'+details.hotel_booking_information.booking_number+'</span> </div>'+
		// 					           "<div> <span style='float:left;'>Roomnumber: </span>&nbsp;<span style='float:center;font-weight:bold;'>"+details.room_number+"</span> </div>"+
		// 					           "<div> <span style='float:left;'>Check-in: </span>&nbsp;<span style='float:center;font-weight:bold;'>"+details.hotel_booking_information.arrival_date_time+"</span> </div>"+
		// 					           "<div> <span style='float:left;'>Check-out: </span>&nbsp;<span style='float:center;font-weight:bold;'>"+details.hotel_booking_information.departure_date_time+"</span> </div>"+
		// 					        "</div> "+
		//
		// 					        "<div class='col-md-6'>"+
		// 					           "<div> <span style='float:left;'>Guest: </span>&nbsp;<span style='float:center;font-weight:bold;'>"+details.hotel_booking_information.guest_name+"</span> </div>"+
		// 					           "<div> <span style='float:left;'>No. of Adult(s): </span>&nbsp;<span style='float:center;font-weight:bold;'>"+details.hotel_booking_information.no_of_adults+"</span> </div>"+
		// 					           "<div> <span style='float:left;'>Contact: </span>&nbsp;<span style='float:center;font-weight:bold;'>"+details.hotel_booking_information.contact_details+"</span> </div>"+
		// 					        "</div>"+
		//
		// 					    "</div>"+
		// 					    '<div class="center"><a href="#" onclick=\"'+details.hotel_booking_information.booking_action+'\" style="font-weight:bold;" >Booking Details</a></div>';
				    	// return _re;
						
		var _re2 =  '<div style="width: 216px;font-size:11px;">'+
						'<div class="row nomargin border-bottom padding-5">'+
							'<div class="col-md-5 nopadding">Booking ID</div>'+
							'<div class="col-md-7 nopadding"><span style="float:center;font-weight:bold;"><a href="#" onclick=\"'+details.hotel_booking_information.booking_action+'\" >'+details.hotel_booking_information.booking_number+'</a>'+details.hotel_booking_information.pay_at_hotel+' </span> </div>'+
						'</div>'+

						'<div class="row nomargin border-bottom padding-5">'+
							'<div class="col-md-5 nopadding">Guest</div>'+
							'<div class="col-md-7 nopadding"><span style="float:center;font-weight:bold;">'+details.hotel_booking_information.guest_name+'</span> </div>'+
						'</div>'+

						'<div class="row nomargin border-bottom padding-5">'+
							'<div class="col-md-5 nopadding">Contact</div>'+
							'<div class="col-md-7 nopadding"><span style="float:center;font-weight:bold;">'+((details.hotel_booking_information.contact_details)?details.hotel_booking_information.contact_details:"NA")+'</span> </div>'+
						'</div>'+

						'<div class="row nomargin border-bottom padding-5">'+
							'<div class="col-md-5 nopadding">Room No.</div>'+
							'<div class="col-md-7 nopadding"><span style="float:center;font-weight:bold;">'+details.room_number+'</span> </div>'+
						'</div>'+

						'<div class="row nomargin border-bottom padding-5">'+
							'<div class="col-md-5 nopadding">Check-in</div>'+
							'<div class="col-md-7 nopadding"><span style="float:center;font-weight:bold;">'+details.hotel_booking_information.arrival_date_time+'</span> </div>'+
						'</div>'+

						'<div class="row nomargin border-bottom padding-5">'+
							'<div class="col-md-5 nopadding">Check-out</div>'+
							'<div class="col-md-7 nopadding"><span style="float:center;font-weight:bold;">'+details.hotel_booking_information.departure_date_time+'</span> </div>'+
						'</div>'+

						'<div class="row nomargin padding-5">'+
							'<div class="col-md-5 nopadding">Adult(s)</div>'+
							'<div class="col-md-7 nopadding"><span style="float:center;font-weight:bold;">'+details.hotel_booking_information.no_of_adults+'</span> </div>'+
						'</div>'+

						'<div class="row nomargin padding-5">'+
							'<div class="col-md-5 nopadding">Consumption</div>'+
							'<div class="col-md-7 nopadding"><span style="float:center;font-weight:bold;">'+details.hotel_booking_information.consumption+'</span> </div>'+
						'</div>'+

						'<div class="row nomargin padding-5">'+
							'<div class="col-md-5 nopadding">Amount payable</div>'+
							'<div class="col-md-7 nopadding"><span style="float:center;font-weight:bold;">'+details.hotel_booking_information.amount_payable+'</span> </div>'+
						'</div>'+
						'<div class="center"><a href="#" onclick=\"'+details.hotel_booking_information.booking_action+'\" style="font-weight:bold;" class="btn btn-xs col-md-12 btn-warning" >More details</a></div>'+
					'</div>';
		return _re2;
	}

	// Color Blendar Graphs
	function colorBlendarFunctionality(availbleRooms,totalRooms){
		var _re = '';

		var bookedRooms = parseInt(totalRooms) - parseInt(availbleRooms);

		var totalPercentage = (parseInt(bookedRooms)/parseInt(totalRooms))*100;

		var occupancyColor = calendar.options.legendColors.occupancy;

		totalPercentage = parseInt(totalPercentage);

		if(totalPercentage >= 0 && totalPercentage <= 30){
			_re += '<div style="height:'+(100-totalPercentage)+'%;position:relative;" class=""></div>';
			
			_re += '<div style="height:'+totalPercentage+'%;position:relative;background:'+occupancyColor+';" class=""></div>';

		}else if(totalPercentage >= 31 && totalPercentage <= 70){
			_re += '<div style="height:'+(100-totalPercentage)+'%;position:relative;" class=""></div>';
			
			_re += '<div style="height:'+totalPercentage+'%;position:relative;background:'+occupancyColor+';" class=""></div>';

		}else if(totalPercentage >= 71 && totalPercentage <= 100){
			_re += '<div style="height:'+(100-totalPercentage)+'%;position:relative;" class=""></div>';
			
			_re += '<div style="height:'+totalPercentage+'%;position:relative;background:'+occupancyColor+';" class=""></div>';

		}


		_re += "<input type='hidden' value='"+parseFloat(totalPercentage).toFixed(2)+"' />";

		return _re;
	}

	function showToolTip(el){
		// $(this).find('input').val())
	}


	function buildResizeDiv(innerDiv, nextTd, startWidth, i){

		nextTd = nextTd.next();

		t.nextTd = nextTd;

		nextTd.on('mouseover',function(){
			i += 1;
			nextTd.unbind('mouseover');
			startWidth += nextTd.width() + 1;

			innerDiv
			.css({
				width: startWidth
			})
			.html('<span class="bc-dragDiv-msg">'+i+' Nights</span>');

			// recursion method
			buildResizeDiv(innerDiv, nextTd, startWidth, i);
		});
		
	}

	function buildDiv(el){
		innerDiv = $("<div />").addClass('roomResizeDiv');
		innerDiv.appendTo($(el));

		return innerDiv;
	}

	function getRoomColor(key){
		if(key === 1){
			return 'bc-legend-reserved';
		}else{
			return 'bc-legend-tempReserve';
		}
	}

	function setRoomColor(roomName){
		var arr;
		arr = roomName.split(' ');
		if($.inArray( "(D)", arr ) == 1){
			return 'bc-dirty';
		}else{
			return null;
		}
	}

	function isRoomDirty(roomName){
		var arr;
		arr = roomName.split(' ');
		if($.inArray( "(D)", arr ) == 1){
			return true;
		}else{
			return false;
		}
	}

	function buildEventContainer(){
		daySegmentContainer =
			$("<div class='bc-event-container' style='position:absolute;z-index:8;top:0;left:0'/>")
				.appendTo(element);
	}

	function buildDatesContainer(){
		container = 
			$("<div class='bc-normal-container'/>")
				.appendTo(element);
		return container;
	}

	function buildCalendar(){
		dA = datesArray;
		td = '';

		// $('table.bc-header span.bc-header-title h2').text(dA.title);
		$.each(dA.dateTitles, function(i,v){
			if(!dA.time){
				var _sDate = new Date(dA.dates[i].date);
				var _id = 'total-status-'+_sDate.getFullYear()+'-'+appendZero(parseInt(_sDate.getMonth())+1)+'-'+appendZero( parseInt(_sDate.getDate()) )
				var dayFlag = false
			}else{
				var dayFlag = true;
			}
			var dataTotal = {occupied:0,vacant:0,dirty:0,maintenance:0};
			if(!dA.time){
				td += '<td class="total-status-popover" id='+_id+' data-day='+dayFlag+' data-is-current='+dA.dates[i].isCurrent+'  data-total='+JSON.stringify(dataTotal)+' >'+v+'</td>';
			}else{
				td += '<td class="total-status-popover" id='+_id+' data-day='+dayFlag+' data-total='+JSON.stringify(dataTotal)+' >'+v+'</td>';
			}
		});

		return td;
	}

	// For Total Occupanct
	function dateWisetotalOccupancy(){
		dA = datesArray;
		td = '';
		// TODO Day view needs to caluculate 
		if(dA.time){
			var _sDate = new Date(dA.dates.date);
			td += '<td align="center" id="total-occupancy-'+_sDate.getFullYear()+'-'+appendZero(parseInt(_sDate.getMonth())+1)+'-'+appendZero( parseInt(_sDate.getDate()) )+'">0</td>';
		}else{
			$.each(dA.dates, function(i,v){
				var _sDate = new Date(v.date);
				td += '<td align="center" data-is-current="'+v.isCurrent+'" id="total-occupancy-'+_sDate.getFullYear()+'-'+appendZero(parseInt(_sDate.getMonth())+1)+'-'+appendZero( parseInt(_sDate.getDate()) )+'">0</td>'
			})
		}

		return td;
	}

	function trigger(name, thisObj) {
		return calendar.trigger.apply(
			calendar,
			[name, thisObj || t].concat(Array.prototype.slice.call(arguments, 2), [t])
		);
	}

	function clearSelection() {
		clearOverlays();
	}

	function getChildrenRows(row){
		var children = [];
        while(row.next().hasClass('child-row')) {
             children.push(row.next());
             row = row.next();
        }            
        return children;
	}

}

;;


/* Misc Utils
-----------------------------------------------------------------------------*/

// TODO 


// Spark Lines Display
	// Create SparkLines for room Types 
		// Arguments 
		// 		1. type
		//		2. offset
		//		3. barWidth
		//		4. barSpacing
		//		5. zeroAxis
		//		6. negBarColor
		//		7. zeroColor

function buildSparkLines(type,offset,barWidth,barSpacing,negBarColor,zeroColor){
	$(".spark_line_good div").sparkline("html", {
		type: type,
//		barColor: "#459D1C",
//		barWidth: "5",
		offset: offset,
		height: "auto",
		width: "auto",
		zeroAxis: false,
		barWidth: barWidth,
		barSpacing: barSpacing,
		negBarColor: negBarColor,
		zeroColor: zeroColor
	});
}



function smartProperty(obj, name) { // get a camel-cased/namespaced property of an object
	if (obj[name] !== undefined) {
		return obj[name];
	}
	var parts = name.split(/(?=[A-Z])/),
		i=parts.length-1, res;
	for (; i>=0; i--) {
		res = obj[parts[i].toLowerCase()];
		if (res !== undefined) {
			return res;
		}
	}
	return obj[''];
}

function disableTextSelection(element) {
	element
		.attr('unselectable', 'on')
		.css('MozUserSelect', 'none')
		.bind('selectstart.ui', function() { return false; });
}

;;

Date.prototype.DaysBetween = function(){  
	var intMilDay = 24 * 60 * 60 * 1000;  
    var intMilDif = arguments[0] - this;  
    var intDays = Math.floor(intMilDif/intMilDay);  
    return intDays;  
}



;;
})(jQuery);

// Show Auto complete for Selecting Rooms -- Total Functionality 
	var outp;
	var oldins;
	var posi = -1;
	var words = new Array();
	var recentWords = new Array();
	var roomIds = new Array();
	var input;
	var key;

	// Set the Visibility
	function setVisible(visi){
		// locals
		var auto_complete_div = $("#auto_complete_div").css({'position':'absolute','visibility':visi});
	}

	// init-- function -- Auto complete method starts here
	function autoInit(){
		outp = document.getElementById("auto-output");
		// outp = $("#auto-output");
		// lookAt();
		window.setInterval("lookAt()", 100);
		setVisible('hidden');
		
		document.onkeydown = keygetter; //needed for Opera...
		document.onkeyup = keyHandler;
	}

	// lookAt method 
	function lookAt(){
		var ins = ($("#goto_room").val())?$("#goto_room").val():0;
		if (oldins == ins) return;
		else if (posi > -1);
		else if (ins.length > 0){
			recentWords = (localStorage['recentList'])?localStorage['recentList'].split(';'):[];
			words = getWord(ins);
			roomIds = getRoomIds(ins);
			if (words.length > 0){
				clearOutput();
				// addWord ('All',0,1);
				addWord ('Show All',0);
				if(recentWords.length){
					for (var i=0;i < recentWords.length; ++i) addWord (recentWords[i].toUpperCase(),null);
				}
				addWord ('Rooms',0,1);
				for (var i=0;i < words.length; ++i) addWord (words[i],roomIds[i]);
				setVisible("visible");
				input = $("#goto_room").val();
			}
			else{
				setVisible("hidden");
				posi = -1;
			}
		}
		else{
			setVisible("hidden");
			posi = -1;
		}
		oldins = ins;
	}

	function addWord(word,roomIdsObj,allFlag){
		var sp = document.createElement("div");
		
		if(!allFlag){
			// TODO
			if(roomIdsObj){
				$(sp).attr({'data-all':'false'});
			}else{
				$(sp).attr({'data-all':'true'});
				if (word == "Show All")$(sp).css({'border-bottom':'1px solid #aaa'})
			}
			if(roomIdsObj) $(sp).attr({'data-room-type-id':roomIdsObj.room_type_id,'data-room-id':roomIdsObj.room_id});
			sp.appendChild(document.createTextNode(word));
			sp.onmouseover = mouseHandler;
			sp.onmouseout = mouseHandlerOut;
			sp.onclick = mouseClick;
		}else{
			sp.appendChild(document.createTextNode(word));
			if(word == 'All'){
				marginTop = 0
			}else{
				marginTop = '10px';
			}
			$(sp).css({'border-bottom':'1px solid #aaa','margin-top':marginTop})
		}		

		outp.appendChild(sp);
	}

	function clearOutput(){
		while (outp.hasChildNodes()){
			noten=outp.firstChild;
			outp.removeChild(noten);
		}
		posi = -1;
	}

	function getWord(beginning){
		var words = new Array();
		for (var i=0;i < suggestions.length; ++i){
			var j = -1;
			var correct = 1;
			var correct2 = 1;
			while (correct == 1 && ++j < beginning.length){
				if (suggestions[i].charAt(j).toLowerCase() != beginning.charAt(j).toLowerCase()) correct = 0;
			}
			
			// For Partial Room Numbers also
			if ( suggestions[i].match(beginning.toString()) == null ) correct2 = 0;

			if (correct == 1) words[words.length] = suggestions[i];
			
			// For Partial Room Numbers also
			if (correct2 == 1) words[words.length] = suggestions[i];
		}

		return words;

	}

	function getRoomIds(beginning){
		var words = new Array();
		for (var i=0;i < suggestions.length; ++i){
			var j = -1;
			var correct = 1;
			while (correct == 1 && ++j < beginning.length){
				if (suggestions[i].charAt(j).toLowerCase() != beginning.charAt(j).toLowerCase()) correct = 0;
			}
			if (correct == 1) words[words.length] = sugDetails[i];
		}
		return words;
	}

	function setColor (_posi, _color, _forg){
		outp.childNodes[_posi].style.background = _color;
		outp.childNodes[_posi].style.color = _forg;
	}

	function keygetter(event){
		if (!event && window.event) event = window.event;
		if (event) key = event.keyCode;
		else key = event.which;
	}

	function keyHandler(event){
		var id = document.getElementById("auto_complete_div");
		if (id && document.getElementById("auto_complete_div").style.visibility == "visible"){
		var textfield = document.getElementById("goto_room");
		if (key == 40){ //Key down
			//alert (words);
			if (words.length > 0 && posi || words.length-1){
				if (posi >=0) setColor(posi, "#fff", "black");
				else input = textfield.value;
				setColor(++posi, "#AAA", "white");
				textfield.value = outp.childNodes[posi].firstChild.nodeValue;
			}
		}
		else if (key == 38){ //Key up
			if (words.length > 0 && posi >= 0){
				if (posi >=1){
					setColor(posi, "#fff", "black");
					setColor(--posi, "#AAA", "white");
					textfield.value = outp.childNodes[posi].firstChild.nodeValue;
				}
				else{
					setColor(posi, "#fff", "black");
					textfield.value = input;
					textfield.focus();
					posi--;
				}
			}
		}
		else if (key == 27){ // Esc
			textfield.value = input;
			setVisible("hidden");
			posi = -1;
			oldins = input;
		}
		else if (key == 8){ // Backspace
			posi = -1;
			oldins=-1;
		}
		}
	}

	var mouseHandler=function(){
		for (var i=0; i < words.length; ++i)
			setColor (i, "white", "black");
	
		this.style.background = "#AAA";
		this.style.color= "white";
	}
	
	var mouseHandlerOut=function(){
		this.style.background = "white";
		this.style.color= "black";
	}
	
	var mouseClick=function(){
		if($(this).attr('data-room-type-id') && $(this).attr('data-room-id')){
			room_type_id = $(this).attr('data-room-type-id');
			room_id = $(this).attr('data-room-id');
		}else{
			room_type_id = null;
			room_id = null;
		}
		// console.log(this.firstChild.nodeValue);

		document.getElementById("goto_room").value = this.firstChild.nodeValue;
		document.getElementById("goto_room_type_id").value = room_type_id;
		document.getElementById("goto_room_id").value = room_id;
		setVisible("hidden");
		posi = -1;
		oldins = this.firstChild.nodeValue;
	}